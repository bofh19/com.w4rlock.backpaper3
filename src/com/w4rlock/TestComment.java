package com.w4rlock;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.SCommentPost;
import com.w4rlock.backpaper4.R;

/**
 * Created by krishna on 17/7/13.
 */
public class TestComment extends Activity {
    private Context context;
    private static final String DEBUG_TAG = "backpaper2";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resource_name_detail_images_v2_comment);
        context = this.getApplicationContext();
        Button button = (Button) findViewById(R.id.test_comment_submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                EditText uname_txt = (EditText)findViewById(R.id.editText_uname);
                EditText email = (EditText)findViewById(R.id.editText_email);
                EditText data = (EditText)findViewById(R.id.editText_comment);
                String comment_name = String.valueOf(uname_txt.getText());
                String comment_data = String.valueOf(data.getText());
                String comment_email = String.valueOf(email.getText());
                Log.d(DEBUG_TAG, comment_name);
                Log.d(DEBUG_TAG,comment_data);
                Log.d(DEBUG_TAG,comment_email);

                String url = "http://backpaper.w4rlock.in/scomments/apost/";
                String domain = "192.168.0.102";
                String csrf_token = "lWNIdbD1OCJjgQBalPe7ruNa9w4WhZN1";
                String comment_uid = "A_TEST_COMMENT";
                String comment_extra_field = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                SCommentPost new_comment = new SCommentPost();
                new_comment.postData(url,domain,csrf_token,comment_uid,comment_data,comment_name,comment_email,comment_extra_field);
            }
        });
    }
}