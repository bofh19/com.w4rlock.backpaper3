package com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils;

import com.w4rlock.backpaper2.models.Categorie;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoriesParser {
	public ArrayList<Categorie> getObjectsArray(Object result2) throws Exception{
		String res = (String)result2;
    	//System.out.println(res);
		try {
			JSONObject main_obj = new JSONObject(res);
			JSONArray main_obj_array = main_obj.getJSONArray("objects");
			
			ArrayList<Categorie> all_cats = new ArrayList<Categorie>(); 
//			Log.d("backpaper2","in parseresult 1");
			for (int i = 0; i < main_obj_array.length(); i++) {
			    JSONObject this_obj = main_obj_array.getJSONObject(i);
			    Categorie this_cat =  new Categorie();
			    this_cat.setCategorie_name(this_obj.getString("categorie_name"));
			    this_cat.setId(this_obj.getInt("id"));
			    this_cat.setCategorieAdult(this_obj.getBoolean("categorie_adult"));
			    all_cats.add(this_cat);
			}
			return all_cats;	
		}catch(Exception e){
			throw e;
		}
	}
}
