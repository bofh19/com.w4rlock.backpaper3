package com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper4.R;

public class GridImageAdapter extends ArrayAdapter<Name>{
	Context context;
	int layoutResourceId;
	Name data[] = null;
	public ImageLoader imageLoader;
	public DisplayImageOptions doptions;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	public GridImageAdapter(Context context,int layoutResourceId,Name[] data){
		super(context,layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
//		imageLoader = new ImageLoader(context.getApplicationContext());
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
		
		doptions = new DisplayImageOptions.Builder()
					.cacheOnDisc()
					.showStubImage(R.drawable.ic_stub)
					.showImageForEmptyUri(R.drawable.ic_empty)
					.showImageOnFail(R.drawable.ic_error)
					.cacheInMemory()
					.cacheOnDisc()
					.build();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		NameHolder nameholder = null;
		if(row == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent,false);
			nameholder = new NameHolder();
			nameholder.txt = (TextView)row.findViewById(R.id.name);
			nameholder.img = (ImageView)row.findViewById(R.id.grid_item_image);
			row.setTag(nameholder);
		}else{
			nameholder = (NameHolder)row.getTag();
		}
		Name this_name = data[position];
		try{
			String this_name_name = this_name.getAdapter_display_name();
			//Log.d("backpaper2",this_name_name);
			nameholder.txt.setText(this_name_name);
		}catch(Exception e){
			Log.d("backpaper2","in go adapter error");
			Log.d("backpaper2",e.toString());
		}
		try{
			//Log.d("backpaper2",this_name.getImg_url());
//			imageLoader.DisplayImage(this_name.getImg_url(),nameholder.img);
			imageLoader.displayImage(this_name.getImg_url(),nameholder.img,doptions,animateFirstListener);
		}catch(Exception e){
			Log.d("backpaper2","error while displaying image@ "+position+e);
		}
//		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
//	    row.startAnimation(animation);
		return row;
	}
	
	static class NameHolder{
		ImageView img;
		TextView txt;
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
}
