package com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.w4rlock.backpaper2.models.Categorie;
import com.w4rlock.backpaper4.R;


/**
 * Created by krishna on 5/20/13.
 */
public class SlideMenuAdapter extends ArrayAdapter<Categorie> {
    Context context;
    int layoutResourceId;
    ArrayList<Categorie> data = null;

    class MenuItem {
        public TextView label;
    }

    public SlideMenuAdapter(Context context,int layoutResourceId,ArrayList<Categorie> data) {
        super(context,layoutResourceId,data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(layoutResourceId, parent,false);
            MenuItem viewHolder = new MenuItem();
            viewHolder.label = (TextView) rowView.findViewById(R.id.drawer_list_item_1);
            rowView.setTag(viewHolder);
        }

        MenuItem holder = (MenuItem) rowView.getTag();
        String s = data.get(position).getCategorie_name();
        holder.label.setText(s);

        return rowView;
    }
}
