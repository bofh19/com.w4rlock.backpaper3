package com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils;

import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Name;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class parseResultNames {
	public static final ServerVariables server_variable = new ServerVariables();
	public ArrayList<Name> getObjectsArray(Object result2) throws Exception{
		String res = (String)result2;
    	//System.out.println(res);
		try {
			JSONObject main_obj = new JSONObject(res);
			JSONArray main_obj_array = main_obj.getJSONArray("objects");
			
			ArrayList<Name> all_names = new ArrayList<Name>(); 
//			Log.d("backpaper2","in parseresult 1");
			for (int i = 0; i < main_obj_array.length(); i++) {
			    JSONObject this_obj = main_obj_array.getJSONObject(i);
			    Name this_name =  new Name();
			    this_name.setCategorie(this_obj.getString("categorie"));
			    this_name.setId(this_obj.getInt("id"));
			    this_name.setName(this_obj.getString("name"));
			    this_name.setResource(this_obj.getString("resource_uri"));
			    JSONObject this_obj_image = this_obj.getJSONArray("name_images").getJSONObject(0);
			    this_name.setImg_url(server_variable.getImgUrl(this_obj_image.getString("imagefile_small")));
                try{
                    this_name.setAvg_of_ranks(this_obj.getInt("avg_of_ranks"));
                    this_name.setSum_votes_count(this_obj.getInt("sum_votes_count"));
                    this_name.setTotal_votes_count(this_obj.getInt("total_votes_count"));
                    this_name.setComments_count(this_obj.getInt("comments_count"));
                    this_name.setLikes_votes_count(this_obj.getInt("likes_votes_count"));
                    this_name.setDislikes_votes_count(this_obj.getInt("dislikes_votes_count"));
                }catch (Exception e){
                    this_name.setAvg_of_ranks(0);
                    this_name.setSum_votes_count(0);
                    this_name.setTotal_votes_count(0);
                    this_name.setComments_count(0);
                    this_name.setLikes_votes_count(0);
                    this_name.setDislikes_votes_count(0);
                }
			    all_names.add(this_name);
			}
			return all_names;	
		}catch(Exception e){
			throw e;
		}
	}
}
