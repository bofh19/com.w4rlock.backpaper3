package com.w4rlock.backpaper2.ActivityUtils.ImageNavUtils;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper4.R;

public class ListViewImageAdapter extends ArrayAdapter<NameImage>{

	Context context;
	int layoutResourceId;
	ArrayList<NameImage> data = null;
	public ImageLoader imageLoader;
	public DisplayImageOptions doptions;
	public ListViewImageAdapter(Context context,int layoutResourceId, ArrayList<NameImage> data){
		super(context,layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
//		imageLoader = new ImageLoader(context.getApplicationContext());
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
		
		doptions = new DisplayImageOptions.Builder()
					.cacheOnDisc()
					.showStubImage(R.drawable.ic_stub)
					.showImageForEmptyUri(R.drawable.ic_empty)
					.showImageOnFail(R.drawable.ic_error)
					.cacheInMemory()
					.cacheOnDisc()
					.displayer(new RoundedBitmapDisplayer(10))
					.build();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ImgHolder imgholder = null;
		if(row == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent,false);
			imgholder = new ImgHolder();
			imgholder.img = (ImageView)row.findViewById(R.id.resource_nav_image_1_image_view);
			row.setTag(imgholder);
		}else{
			imgholder = (ImgHolder)row.getTag();
		}
		try{
			String this_img_url = data.get(position).getImagefile_small();
			if(this_img_url == "loading"){
				LayoutInflater inflater2 = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View row2 = inflater2.inflate(R.layout.loading_status, parent,false);
				return row2;
			}
//			Log.d("backpaper2",this_img_url);
//			imageLoader.DisplayImage(this_img_url,imgholder.img);
			imageLoader.displayImage(this_img_url,imgholder.img,doptions);
		}catch(Exception e){
			Log.d("backpaper2","error while displaying image@ "+position+e);
		}
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
	    row.startAnimation(animation);
		return row;
	}
	
	static class ImgHolder{
		ImageView img;
	}
}

