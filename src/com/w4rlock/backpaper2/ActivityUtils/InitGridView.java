package com.w4rlock.backpaper2.ActivityUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.GridView;

public class InitGridView {
	private GridView gridView;
	private Context context;
	public InitGridView(GridView gridView,Context context){
		this.gridView = gridView;
		this.context = context;
	}
	
	public void executeLayoutInit(){
		InitilizeGridLayout();
	}
	
	// gridview init
    public static final int NUM_OF_COLUMNS = 3;
	public static final int GRID_PADDING = 0; // in dp
	public int columnWidth;
	
    private void InitilizeGridLayout() {
		Resources r = context.getResources();
		float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				GRID_PADDING, r.getDisplayMetrics());

		columnWidth = (int) ((getScreenWidth() - ((NUM_OF_COLUMNS + 1) * padding)) / NUM_OF_COLUMNS);
		gridView.setNumColumns(NUM_OF_COLUMNS);
		gridView.setColumnWidth(columnWidth);
		gridView.setStretchMode(GridView.NO_STRETCH);
		gridView.setPadding((int) padding, (int) padding, (int) padding,
				(int) padding);
		gridView.setHorizontalSpacing((int) padding);
		gridView.setVerticalSpacing((int) padding);
	}
    
    /*
	 * getting screen width
	 */
	@SuppressLint("NewApi")
	public int getScreenWidth() {
		int columnWidth;
		WindowManager wm = (WindowManager)context.getSystemService(
						Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		final Point point = new Point();
		try {
			display.getSize(point);
		} catch (java.lang.NoSuchMethodError ignore) { // Older device
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		columnWidth = point.x;
		return columnWidth;
	}
}
