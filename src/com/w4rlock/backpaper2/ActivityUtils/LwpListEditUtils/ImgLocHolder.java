package com.w4rlock.backpaper2.ActivityUtils.LwpListEditUtils;

public class ImgLocHolder {
	private String imgLoc;
	private Boolean state;
	public String getImgLoc() {
		return imgLoc;
	}
	public void setImgLoc(String imgLoc) {
		this.imgLoc = imgLoc;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
}
