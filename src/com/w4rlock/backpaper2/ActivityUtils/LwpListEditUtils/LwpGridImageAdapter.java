package com.w4rlock.backpaper2.ActivityUtils.LwpListEditUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.w4rlock.backpaper4.R;

public class LwpGridImageAdapter extends ArrayAdapter<ImgLocHolder>{
	Context context;
	int layoutResourceId;
	List<ImgLocHolder> data = null;
	ImageLoader imageLoader;
	public DisplayImageOptions doptions;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	public LwpGridImageAdapter(Context context, int resource, ArrayList<ImgLocHolder> objects) {
		super(context, resource, objects);
		
		this.layoutResourceId = resource;
		this.data = objects;
		this.context = context;
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
		
		doptions = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.showStubImage(R.drawable.ic_stub)
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error)
		.cacheInMemory()
		.cacheOnDisc()
		.displayer(new RoundedBitmapDisplayer(10))
		.build();
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ImgHolder imgholder = null;
		if(row == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent,false);
			imgholder = new ImgHolder();
			imgholder.img = (ImageView)row.findViewById(R.id.lwp_list_grid_item_image);
			row.setTag(imgholder);
		}else{
			imgholder = (ImgHolder)row.getTag();
		}
		ImgLocHolder img_loc = data.get(position);
		try{
//			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//			Bitmap b = BitmapFactory.decodeFile(img_loc.getImgLoc(), options);
//			imgholder.img.setImageBitmap(b);
			String location = "file://"+img_loc.getImgLoc();
//			Log.d("backpaper2",location);
			imageLoader.displayImage(location, imgholder.img,doptions,animateFirstListener);
            if(android.os.Build.VERSION.SDK_INT >= 11){
                if(img_loc.getState()){
                    imgholder.img.setBackgroundColor(Color.BLACK);
                }else{
                    imgholder.img.setBackgroundColor(Color.WHITE);
                }
            }else{
                if(img_loc.getState()){
                    imgholder.img.setBackgroundColor(Color.WHITE);
                }else{
                    imgholder.img.setBackgroundColor(Color.BLACK);
                }
            }

		}catch(Exception e){
			Log.d("backpaper2","error while displaying image@ "+position+e);
		}
//		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
//	    row.startAnimation(animation);
		return row;
	}
	
	static class ImgHolder{
		ImageView img;
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
}
