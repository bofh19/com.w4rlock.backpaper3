package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper4.R;

public class GridImageAdapter2 extends ArrayAdapter<NameImage>{
	
	Context context;
	int layoutResourceId;
	ArrayList<NameImage> data = null;
	public ImageLoader imageLoader;
	public DisplayImageOptions doptions;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	public GridImageAdapter2(Context context,int layoutResourceId, ArrayList<NameImage> data){
		super(context,layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
//		imageLoader = new ImageLoader(context.getApplicationContext());
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
		
		doptions = new DisplayImageOptions.Builder()
					.cacheOnDisc()
					.showStubImage(R.drawable.ic_stub)
					.showImageForEmptyUri(R.drawable.ic_empty)
					.showImageOnFail(R.drawable.ic_error)
					.cacheInMemory()
					.cacheOnDisc()
					.build();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ImgHolder imgholder = null;
		if(row == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent,false);
			imgholder = new ImgHolder();
			imgholder.img = (ImageView)row.findViewById(R.id.name_images_grid_item_image);
            imgholder.ranksUsed = (TextView)row.findViewById(R.id.name_images_grid_item_text);
			row.setTag(imgholder);
		}else{
			imgholder = (ImgHolder)row.getTag();
		}
		try{
			String this_img_url = data.get(position).getImagefile_small();
			if(this_img_url == "loading"){
				LayoutInflater inflater2 = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View row2 = inflater2.inflate(R.layout.loading_status, parent,false);
				return row2;
			}
//			Log.d("backpaper2",this_img_url);
//			imageLoader.DisplayImage(this_img_url,imgholder.img);
			imageLoader.displayImage(this_img_url,imgholder.img,doptions,animateFirstListener);
            try{
                imgholder.ranksUsed.setText("ranks used: "+data.get(position).getRanksUsed());
            }catch(Exception e){

            }
		}catch(Exception e){
			Log.d("backpaper2","error while displaying image@ "+position+e);
		}
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
	    row.startAnimation(animation);
		return row;
	}
	
	static class ImgHolder{
		ImageView img;
        TextView ranksUsed;
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
}

