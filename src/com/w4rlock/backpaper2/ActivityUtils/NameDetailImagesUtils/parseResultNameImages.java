package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper2.models.NameImages;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class parseResultNameImages {
	public static final ServerVariables server_variable = new ServerVariables();
	Context context;

    public parseResultNameImages(Context context){
        this.context = context;
    }

    public NameImages getObjectsArray(Object result2) throws Exception{
		String res = (String)result2;
		NameImages name_image = new NameImages();
		try {
			JSONObject main_obj = new JSONObject(res);
			JSONObject meta = main_obj.getJSONObject("meta");
			name_image.setTotal_count(meta.getInt("total_count"));
			try{
				String next = meta.getString("next");
				if(!next.equals("null"))
					name_image.setNext(server_variable.getNextUrl(next));
				else
					name_image.setNext(null);
			}catch(Exception e){
				name_image.setNext(null);
			}
			JSONArray main_obj_array = main_obj.getJSONArray("objects");
			ArrayList<NameImage> name_img = new ArrayList<NameImage>();
			for (int i = 0; i < main_obj_array.length(); i++) {
				NameImage this_name_img = new NameImage();
			    JSONObject this_obj = main_obj_array.getJSONObject(i);
			    this_name_img.setImagefile(server_variable.getImgUrl(this_obj.getString("imagefile")));
			    this_name_img.setImagefile_small(server_variable.getImgUrl(this_obj.getString("imagefile_small")));
			    this_name_img.setId(this_obj.getInt("id"));
			    this_name_img.setName_uri(this_obj.getString("name"));
			    this_name_img.setSource(this_obj.getString("source"));
			    this_name_img.setDate(this_obj.getString("date"));
			    this_name_img.setImagefileOriginal(server_variable.getImgUrl(this_obj.getString("imagefile_download")));
                this_name_img.setImageAdult(this_obj.getBoolean("image_adult"));
                this_name_img.setImageName(this_obj.getString("name_name"));
                this_name_img.setResourceUri(this_obj.getString("resource_uri"));
                try{
                    this_name_img.setRanksUsed(this_obj.getInt("ranks_used"));
                }catch (Exception e){
                    this_name_img.setRanksUsed(-1);
                }
			    name_img.add(this_name_img);
			}
            name_img = getAdultFiltered(name_img);
			name_image.setImages(name_img);
			return name_image;	
		}catch(Exception e){
            Log.d("backpaper2-parse", e.toString());
			throw e;
		}
	}	
	
	public NameImages getObjectsArrayNext(NameImages name_image,Object result2) throws Exception{
		String res = (String)result2;
		try {
			JSONObject main_obj = new JSONObject(res);
			JSONObject meta = main_obj.getJSONObject("meta");
			name_image.setTotal_count(meta.getInt("total_count"));
			try{
				String next = meta.getString("next");
				if(!next.equals("null"))
					name_image.setNext(server_variable.getNextUrl(next));
				else
					name_image.setNext(null);
			}catch(Exception e){
				name_image.setNext(null);
			}
			JSONArray main_obj_array = main_obj.getJSONArray("objects");
		
			ArrayList<NameImage> name_img = new ArrayList<NameImage>();
			
			for (int i = 0; i < main_obj_array.length(); i++) {
				NameImage this_name_img = new NameImage();
			    JSONObject this_obj = main_obj_array.getJSONObject(i);
			    this_name_img.setImagefile(server_variable.getImgUrl(this_obj.getString("imagefile")));
			    this_name_img.setImagefile_small(server_variable.getImgUrl(this_obj.getString("imagefile_small")));
			    this_name_img.setId(this_obj.getInt("id"));
			    this_name_img.setName_uri(this_obj.getString("name"));
			    this_name_img.setSource(this_obj.getString("source"));
			    this_name_img.setDate(this_obj.getString("date"));
                this_name_img.setImagefileOriginal(server_variable.getImgUrl(this_obj.getString("imagefile_download")));
                this_name_img.setImageAdult(this_obj.getBoolean("image_adult"));
                this_name_img.setImageName(this_obj.getString("name_name"));
                this_name_img.setResourceUri(this_obj.getString("resource_uri"));
                try{
                    this_name_img.setRanksUsed(this_obj.getInt("ranks_used"));
                }catch (Exception e){
                    this_name_img.setRanksUsed(-1);
                }
			    name_img.add(this_name_img);
			}
            name_img = getAdultFiltered(name_img);
			name_image.setImages(name_img);
			return name_image;	
		}catch(Exception e){
			throw e;
		}
	}
    private ArrayList<NameImage> getAdultFiltered(ArrayList<NameImage> images) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        if(!sPref.getBoolean("category_adult",false)){
            ArrayList<NameImage> new_name_images = new ArrayList<NameImage>();
            for(NameImage this_name_images_new : images){
//                Log.d("backpaper2-adultfiltered", this_name_images_new.getId() + " " + this_name_images_new.getImageAdult());
                if(!this_name_images_new.getImageAdult()){
                    new_name_images.add(this_name_images_new);
                }
            }
            return new_name_images;
        }else{
            return images;
        }
    }
}


