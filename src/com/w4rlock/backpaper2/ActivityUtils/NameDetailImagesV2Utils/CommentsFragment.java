package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

/**
 * Created by krishna on 18/7/13.
 */

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Comments;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.backpaper4.ImageNavV2;
import com.w4rlock.backpaper4.NameDetailImagesV2;
import com.w4rlock.backpaper4.R;
import com.w4rlock.cacheManager.getURLdata;


public class CommentsFragment extends Fragment {
    private static final String DEBUG_TAG = "backpaper2-comments-fragment";
    private Context context;
    private ListView mainListVIew = null;
    public MainListViewAdapter mainAdapter = null;
    public ArrayList<Comments> comments = new ArrayList<Comments>();
    private Toast mToast=null;
    private Name given_name=null;
    private NameImage given_name_image = null;
    private ViewGroup rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.resource_name_detail_images_v2_comment, container, false);
        context = this.getActivity().getApplicationContext();
        mainListVIew = (ListView)rootView.findViewById(R.id.main_list_view);
        Intent intent = this.getActivity().getIntent();
        if(this.getActivity().getClass() == NameDetailImagesV2.class){
            given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameDetailIntentMessage());
        }else if(this.getActivity().getClass() == ImageNavV2.class){
            given_name_image = (NameImage) intent.getSerializableExtra(IntentConfigVariables.getNameImgNavMessageImages());
        }
        if(NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            getComments();
        }
//        goMainAdapter();
        return rootView;
    }

    private void getComments() {
        class GetComments extends getURLdata {
            GetComments(){
                setReadTimeOut(15000);
                setConnTimeOut(15000);
            }
            @Override
            public void postresult(Object result2) {
                try{
                   comments = CommentsParser.getCommentsArray(result2);
                    System.out.println("result2 = " + result2);
                   goMainAdapter();
                }catch(Exception e){
                    Log.d("backpaper2-getcomments", "unable to get comments " + e.toString());
                }
            }
        }
        GetComments getcomments = new GetComments();
        String comment_uid = "U";
        if(this.getActivity().getClass() == NameDetailImagesV2.class){
            comment_uid = given_name.getCommentUid();
        }else if(this.getActivity().getClass() == ImageNavV2.class){
            comment_uid = given_name_image.getCommentUid();
        }
        getcomments.getData(ServerVariables.getCommentsList(comment_uid));
    }

    private void goMainAdapter() {
        mainAdapter = new MainListViewAdapter(context,R.layout.resource_name_detail_images_v2_comments_listview,comments);
        mainListVIew.setAdapter(mainAdapter);
    }

    private void displayToast(String msg){
        try{
            mToast.setText(msg);
            mToast.show();
        }catch(Exception e){
            mToast = Toast.makeText(this.getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }


    public void addNewComment(Comments comment) {
        comments.add(0,comment);
        mainAdapter.notifyDataSetChanged();
    }

    public void redrawComments(NameImage current_name_image){
        given_name_image = current_name_image;
        if(NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            getComments();
        }
        PostCommentFragment postCommentFrag = (PostCommentFragment) getFragmentManager().findFragmentById(R.id.frag_series);
        postCommentFrag.setGivenNameImage(current_name_image);
    }

}
