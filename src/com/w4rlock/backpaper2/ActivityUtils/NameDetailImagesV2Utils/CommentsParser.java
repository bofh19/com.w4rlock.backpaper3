package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

import com.w4rlock.backpaper2.models.Comments;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by krishna on 20/7/13.
 */
public class CommentsParser {
    public static ArrayList<Comments> getCommentsArray(Object result) throws Exception{
        String res = (String)result;
        ArrayList<Comments> comments = new ArrayList<Comments>();
        try{
            JSONObject main_obj = new JSONObject(res);
            //System.out.println("main_obj = " + main_obj);
            JSONArray main_objects = main_obj.getJSONArray("objects");
            //System.out.println("main_objects = " + main_objects);
            for(int i=0;i<main_objects.length();i++){
                JSONObject this_obj = main_objects.getJSONObject(i);
                System.out.println("this_obj = " + this_obj);
                Comments new_comment = new Comments();
                try {
                    new_comment.setCommentData(this_obj.getString("comment_data"));
                }catch (Exception e){
                    new_comment.setCommentData(" ");
                }
                try {
                    new_comment.setCommentUname(this_obj.getString("name"));
                }catch (Exception e){
                    new_comment.setCommentUname(" ");
                }
                try {
                    new_comment.setCommentTstamp(this_obj.getString("modified_date"));
                }catch (Exception e){
                    new_comment.setCommentTstamp(" ");
                }
                try {
                    new_comment.setCommentId(String.valueOf(this_obj.getInt("id")));
                    comments.add(new_comment);
                }catch (Exception e){

                }
            }
        }catch(Exception e){
            throw e;
        }
        return comments;
    }
}
