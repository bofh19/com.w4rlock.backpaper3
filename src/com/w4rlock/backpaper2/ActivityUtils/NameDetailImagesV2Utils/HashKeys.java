package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

/**
 * Created by Chitti on 17/7/13.
 */
public class HashKeys {
    public static final String[] DEV_KEYS = {
            "zkLJuCh20A6lQpGY5UXcHZdWy",
            "XwMeBH3GJhDsx15jrIOK7A0ut",
            "KTld4qMiGZPCw1pr6Ucj85oBs",
            "uXVGIxLJkTSoEYFi2BRtgCDs1",
            "5ORfSatexEA9nwNY6Uormu0Pd",
            "SvAcxhMEW3OrwjXmyFbTQLI7k",
            "02picYqyK1fZNIDr6Evmza5Pu",
            "TpOKMd0wQJbPqo67kgusIzXHU",
            "3gURtn2yjDP7W8VCAr1kOmu0L",
            "KP5XGBAZOm2vFTWe1CbzcgMfw"
    };
    public static String[] getKeys(){
        return DEV_KEYS;
    }
}
