package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.parseResultNameImages;
import com.w4rlock.backpaper2.Logger.GTracker;
import com.w4rlock.backpaper2.configs.GetDisplayParams;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper2.models.NameImages;
import com.w4rlock.backpaper2.utils.Connectivity;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.backpaper4.ImageNavV2;
import com.w4rlock.backpaper4.R;
import com.w4rlock.cacheManager.CacheManager;
import com.w4rlock.cacheManager.getURLdata;

/**
 * Created by krishna on 18/7/13.
 */
public class ImagesFragment extends Fragment {
    public static final ServerVariables server_variable = new ServerVariables();
    private static String url;
    private CacheManager cm;
    private com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.GridImageAdapter2 adapter=null;
    private GridView gridView;
    private Boolean refresh = false;
    private Toast mToast=null;
    //	private ArrayList<String> image_urls=null;
    private NameImages name_images = null;
    private Name given_name=null;

    private Context context;
    private ViewGroup rootView=null;
    private Boolean auto_updated=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(
                R.layout.activity_name_detail_images_v2_f1, container, false);
        context = this.getActivity().getApplicationContext();
        cm = new CacheManager(this.getActivity().getApplicationContext());
        Intent intent = this.getActivity().getIntent();
        given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameDetailIntentMessage());
        String server_url;
        if(given_name.getId() == -1){  // if recent images initiated through notifications panel
            server_url = server_variable.getNameImagesUrlWithParams(GetDisplayParams.getFullDisplayAsQueryParams(context));
            url = new String(server_url);
            refresh = true;
            goOnNetwork(server_url);
        }else if(given_name.getId() == -3){
            server_url = server_variable.getNameImagesPopularUrlWithParams(GetDisplayParams.getFullDisplayAsQueryParams(context));
            url = new String(server_url);
            refresh = true;
            goOnNetwork(server_url);
        }
        else{
            server_url = server_variable.getNameAllImageUrlWithParams((given_name.getId()), GetDisplayParams.getFullDisplayAsQueryParams(context));
            url = new String(server_url);
            if(cm.checkFileFromUrl(server_url)){
                goOnFile(server_url);
                displayToast("Displaying Cached Data. Click Refresh in Menu Button To Load Latest Data");
            }else{
                goOnNetwork(server_url);
            }
        }
        Log.d("backpaper2", given_name.getName());
        setLikeButtonActions();
        showFirstTimeOptions(true);
        return rootView;
    }

    private void setLikeButtonActions(){
        View likeButtonLayout = rootView.findViewById(R.id.like_button_layout);
        likeButtonLayout.bringToFront();
        Button plusOneButton = (Button) rootView.findViewById(R.id.name_detail_plusone_button);
        plusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLikeRank(1);
            }
        });
        Button zeroButton = (Button) rootView.findViewById(R.id.name_detail_zero_button);
        zeroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLikeRank(0);
            }
        });

        Button minusOneButton = (Button) rootView.findViewById(R.id.name_detail_minusone_button);
        minusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLikeRank(-1);
            }
        });
        setInitialCurrentRanks();
        setInitialButtonRankView();
    }

    private void postLikeRank(final int rank) {
        if(!NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            displayToast("Not able to Connect to Internet");
            return;
        }
        class LikePost extends SLikePost{
            @Override
            public void failed(String message){
                displayToast(message);
            }
            @Override
            public void postResult(Object result){
                try{
                    displayToast(result.toString()+" On "+rank);
                    SharedPreferences settings_likes =  context.getSharedPreferences(IntentConfigVariables.LikesDetailsPref, 0);
                    SharedPreferences.Editor settings_likes_editor = settings_likes.edit();
                    settings_likes_editor.putLong(IntentConfigVariables.LikesLastCommentEpoch,System.currentTimeMillis());
                    settings_likes_editor.putInt(IntentConfigVariables.LikesCurrentStatusVar(given_name.getCommentUid()),rank);
                    settings_likes_editor.commit();
                    setButtonClickedViews(rank,true);
                    setInitialCurrentRanks();
                }catch(Exception e){
                    Log.d(DEBUG_TAG,"error while post result display toast "+e.toString());
                }
            }
        }
        String url = ServerVariables.getLikePostUrl();
        String rank_given = String.valueOf(rank);
        SharedPreferences settings_likes =  context.getSharedPreferences(IntentConfigVariables.LikesDetailsPref, 0);
        long timeDiff = System.currentTimeMillis() - settings_likes.getLong(IntentConfigVariables.LikesLastCommentEpoch,0);
        if(timeDiff/1000 < 5){
            displayToast("Please Wait "+timeDiff/1000+" before rating again");
            return;
        }
        String dev_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String domain = ServerVariables.getDomain();
        SharedPreferences settings =  context.getSharedPreferences(IntentConfigVariables.CsrfTokenPref, 0);
        String csrf_token = null;
        if(( csrf_token = settings.getString(IntentConfigVariables.CsrfTokenValue,null)) == null){
            displayToast("Some thing has Gone wrong. Sorry About that !!!!. Press back and come back here.");
            return;
        }
        LikePost new_like = new LikePost();
        String rank_uid = given_name.getCommentUid();
        displayToast("Submitting Rating");
        new_like.postData(url, domain, csrf_token, rank_given, rank_uid, dev_id);
    }
    private void setInitialButtonRankView(){
        SharedPreferences settings_likes = context.getSharedPreferences(IntentConfigVariables.LikesDetailsPref,0);
        if(settings_likes.getInt(IntentConfigVariables.LikesCurrentStatusVar(given_name.getCommentUid()),-2)==-2){
            setButtonClickedViews(-2,false);
            return;
        };
        if(!NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            displayToast("Not able to Connect to Internet");
            return;
        }
        class getUserRank extends getURLdata{
            getUserRank(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2){
                try {
                    int currentRank = LikesParser.getMyRank((String)result2);
                    if(currentRank!=-1)
                        setButtonClickedViews(currentRank,true);
                    else if(currentRank == -2)
                        displayToast("Not Rated. To Rate Select on the top right corner");
                    else
                        setButtonClickedViews(currentRank,false);
                } catch (Exception e) {
                    Log.d("backpaper2","unable to write to cache file "+e.toString());
                }
            }
            @Override
            public void onError(Object result){
                displayToast( "Unable to get Users Current Rank");
            }
            @Override
            public void onTimeOut(Object result){
                displayError("Unable to get Users Current Rank");
            }
        }
        getUserRank gettingUserRank = new getUserRank();
        String dev_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String rank_uid = given_name.getCommentUid();
        String current_url = ServerVariables.getLikesListOwnUrl(rank_uid,dev_id);
        Log.d("backpaper2-likes-own-url",current_url);
        gettingUserRank.getData(current_url);
    }

    private  void setInitialCurrentRanks(){
        final TextView ranksText = (TextView)rootView.findViewById(R.id.name_detail_current_rank);
        ranksText.setText("loading..");
        if(!NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            ranksText.setText("XX");
            return;
        }
        class getRanksSum extends getURLdata{
            getRanksSum(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2){
                try {
                    ranksText.setText(LikesParser.getTotalCount((String)result2));
                } catch (Exception e) {
                    Log.d("backpaper2","unable to write to cache file "+e.toString());
                    ranksText.setText("XX");
                }
            }
            @Override
            public void onError(Object result){
                displayToast( "Unable to get Users Current Rank");
            }
            @Override
            public void onTimeOut(Object result){
                displayError("Unable to get Users Current Rank");
            }
        }
        getRanksSum gettingRanksSum = new getRanksSum();
        String rank_uid = given_name.getCommentUid();
        String current_url = ServerVariables.getLikesListUrl(rank_uid);
        Log.d("backpaper2-likes-list-url",current_url);
        gettingRanksSum.getData(current_url);
    }

    private void setButtonClickedViews(int rank, boolean b) {
        Button plusOneButton = (Button) rootView.findViewById(R.id.name_detail_plusone_button);
        Button zeroButton = (Button) rootView.findViewById(R.id.name_detail_zero_button);
        Button minusOneButton = (Button) rootView.findViewById(R.id.name_detail_minusone_button);
       // minusOneButton.setBackgroundResource(R.drawable.arrow_down_icon);
       // plusOneButton.setBackgroundResource(R.drawable.arrow_up_icon);
        minusOneButton.setBackgroundColor(Color.TRANSPARENT);
        plusOneButton.setBackgroundColor(Color.TRANSPARENT);
        if(b){
            Log.d("backpaper2","----------got rank ----"+rank);
            if(rank == 1){
                plusOneButton.setBackgroundColor(getResources().getColor(R.color.black_transparent));
               // plusOneButton.setBackgroundResource(R.drawable.arrow_up_icon_black);
            }else if(rank == -1){
                minusOneButton.setBackgroundColor(getResources().getColor(R.color.black_transparent));
               // minusOneButton.setBackgroundResource(R.drawable.arrow_down_icon_black);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh_data:
                loading_data = false;
                load_data = false;
                refresh = true;
                reloadDataFromNetwork();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reloadDataFromNetwork() {
        View loadingBar = (View)this.getActivity().findViewById(R.id.progress_large);
        if ( loadingBar.getVisibility() == View.VISIBLE){
            displayToast("Please Wait While Data is Loading");
        }else{
            goOnNetwork(url);
        }
    }

    private void goOnFile(String server_url) {
        try{
            String result2 = cm.readFromUrl(server_url);
            parseresult(result2);
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            if(!auto_updated && Connectivity.isConnectedWifi(context) && sharedPrefs.getBoolean("auto_download",true) ){
                reloadDataFromNetwork();
                auto_updated = true;
                refresh = true;
            }
        }catch(Exception e){
            Log.d("backpaper2","error while reading file "+e.toString());
            goOnNetwork(server_url);
        }
    }

    private Boolean goOnNetwork(final String url) {
        onDataLoading();
        if(!NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            displayError("No Network Or Cache");
            return false;
        }
        class GetData extends getURLdata {
            GetData(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2){
                try {
                    cm.addtoCacheFile((String)result2, url);
                } catch (IOException e) {
                    Log.d("backpaper2","unable to write to cache file "+e.toString());
                }
                parseresult(((String)result2));
            }
            @Override
            public void onError(Object result){
                Log.d("backpaper2","Error - goOnNetwork");
                displayToast( "Error while loading data");
                displayError("Error while Loading Data .. Try Reloading Data");
            }
            @Override
            public void onTimeOut(Object result){
                displayToast("Time Out..");
                displayError("Time Out.. Try Reloading data");
            }
        }
        GetData getdata = new GetData();
        getdata.getData(url);
        return true;
    }

    private void displayError(String string) {
//        TextView loading_text = (TextView)this.getActivity().findViewById(R.id.loading);
//        loading_text.setText(string);
//        loading_text.setVisibility(View.VISIBLE);
        displayToast(string);
        hideLoadingBar();
    }


    private void parseresult(String result2) {
        try {
            com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.parseResultNameImages presult = new com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.parseResultNameImages(this.getActivity().getApplicationContext());
            name_images = presult.getObjectsArray(result2);
            goAdapter(name_images.getImages());
        } catch (Exception e) {
            Log.d("backpaper2","1"+e.toString());
            //displayError(e.toString());
        }
    }

    private void goAdapter(ArrayList<NameImage> name_images) {
        //	String[] name_images_array = name_images.toArray(new String[name_images.size()]);
        try{
            adapter = new com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.GridImageAdapter2(this.getActivity().getApplicationContext(), R.layout.resource_name_detail_images_adapter,name_images);
            gridView = (GridView)rootView.findViewById(R.id.name_images_gridview1);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(mMessageClickedHandler);
            gridView.setOnScrollListener(mScrollListener);
        }catch (Exception e){
            Log.d("backpaper2","2"+e.toString());
        }
        displayLoaders(false);
    }

    private void hideLoadingBar(){
        try{
            View loadingbar = (View)this.getActivity().findViewById(R.id.progress_large);
            loadingbar.setVisibility(View.GONE);
        }catch(Exception e){
            Log.d("backpaper","Error while hiding loading bar "+e.toString());
        }
    }
    private void displayLoaders(boolean b) {
        if(b){
            try{
                TextView loading = (TextView)this.getActivity().findViewById(R.id.loading);
                loading.setVisibility(View.VISIBLE);

                View loadingbar = (View)this.getActivity().findViewById(R.id.progress_large);
                loadingbar.setVisibility(View.VISIBLE);

            }catch(Exception e){
                Log.d("backpaper2",e.toString());
            }
        }else{
            try{
                TextView loading = (TextView)this.getActivity().findViewById(R.id.loading);
                loading.setVisibility(View.GONE);

                View loadingbar = (View)this.getActivity().findViewById(R.id.progress_large);
                loadingbar.setVisibility(View.GONE);

            }catch(Exception e){
                Log.d("backpaper2",e.toString());
            }
        }
    }

    private void displayToast(String msg){
        try{
            mToast.setText(msg);
            mToast.show();
        }catch(Exception e){
            try{
            mToast = Toast.makeText(this.getActivity().getApplicationContext(), msg , Toast.LENGTH_SHORT );
            mToast.show();
            }catch (Exception ex){
                Log.d("backpaper2",ex.toString());
            }
        }
    }

    /// loading extra data func

////next result set

    private void goOnFileNext(String server_url,NameImages name_images) {
        Log.d("backpaper2","refresh variable value - "+refresh.toString());
        try{
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            if(refresh || (Connectivity.isConnectedWifi(context) && sharedPrefs.getBoolean("auto_download",true)) ){
                goOnNetworkNext(server_url, name_images);
            }else{
                String result2 = cm.readFromUrl(server_url);
                parseresultNext(result2, name_images);
            }
        }catch(Exception e){
            Log.d("backpaper2","error while reading file "+e.toString());
            goOnNetworkNext(server_url, name_images);
        }
    }

    private Boolean goOnNetworkNext(final String url,final NameImages name_images) {
        onDataLoading();
        if(!NetworkChecker.getNetStatus(context)){
            displayError("No Network Or Cache");
            onDataLoaded();
            return false;
        }
        class GetData extends getURLdata{
            GetData(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2){
                try {
                    cm.addtoCacheFile((String)result2, url);
                } catch (IOException e) {
                    Log.d("backpaper2","unable to write to cache file "+e.toString());
                }
                try{
                parseresultNext(((String)result2),name_images);
                }catch (Exception e){
                    Log.d("backpaper2","trying to catch that wierd error "+e.toString());
                    displayToast("Some Wierd Error Has Occurd finishing this activity . please reload ");
                    closeThisActivity();
                }
            }
            @Override
            public void onError(Object result){
                displayToast("Error while loading data");
                displayError("Error while Loading Data");
            }
            @Override
            public void onTimeOut(Object result){
                displayToast("Time Out..");
                displayError("Network Time Out...Try Reloading ");
            }
        }
        GetData getdata = new GetData();
        getdata.getData(url);
        return true;
    }
    public void parseresultNext(String string,NameImages name_images) {
        com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.parseResultNameImages presult = new parseResultNameImages(this.getActivity().getApplicationContext());
        try {
            NameImages name_images_new = presult.getObjectsArrayNext(name_images, string);
            ArrayList<NameImage> new_name_images = new ArrayList<NameImage>(name_images.getImages());
            goAdapterNext(name_images_new.getImages());
            for(NameImage this_name_images_new : name_images_new.getImages()){
                new_name_images.add(this_name_images_new);
            }
            name_images.setImages(new_name_images);
            try{
                name_images.setNext(name_images_new.getNext());
            }catch(Exception e){
                name_images.setNext(null);
            }
            onDataLoaded();
        } catch (Exception e) {
            Log.d("backpaper2","1"+e.toString());
            displayError(e.toString());
        }
    }

    private void goAdapterNext(ArrayList<NameImage> name_images_list) {
        for(NameImage this_name_image : name_images_list){
//			System.out.println(this_name_image.getId());
            adapter.add(this_name_image);
        }
    }

    private void onDataLoaded(){
        loading_data = false;
        load_data = false;
        displayLoaders(false);
    }

    private void closeThisActivity(){
        try{
        getActivity().finish();
        }catch (Exception e){
            Log.d("backpaper2","another wierd null pointer error " +e.toString());
        }
    }

    private void onDataLoading(){
        displayLoaders(true);
    }
    // next result set functions ends here
    private Boolean loading_data = false;
    private Boolean load_data = false;
    private AbsListView.OnScrollListener mScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if(load_data && !loading_data){
                attachLoaderToGrid();
                loading_data = true;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            if(!loading_data){
                if(firstVisibleItem + visibleItemCount == totalItemCount){
                    load_data = true;
                }else{
//					load_data = false;
                }
            }
        }
    };

    protected void attachLoaderToGrid() {
        if(name_images.getNext() != null){
            displayLoaders(true);
            goOnFileNext(name_images.getNext(),name_images);
        }else{
            displayToast( "All data loaded.");
            Log.d("backpaper2","no next item");
        }
    }

    private AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parents, View arg1, int arg2,long arg3) {
            Intent intent = new Intent(parents.getContext(),ImageNavV2.class);
            NameImage selected_name_image = (NameImage)parents.getAdapter().getItem(arg2);
            Log.d("backpaper2","selected image id "+Integer.valueOf(selected_name_image.getId()).toString());
            GTracker.trackEvent(context, "NameDetailImages", "view_full_image", selected_name_image.getResourceUri(), new Long(selected_name_image.getId()));
            intent.putExtra(IntentConfigVariables.getNameImgNavMessageImages(),selected_name_image);
            intent.putExtra(IntentConfigVariables.getNameImgNavMessageName(), given_name);
            startActivity(intent);
        }
    };



    public static class MyDialogFragment extends DialogFragment {
        static MyDialogFragment newInstance() {
            MyDialogFragment f = new MyDialogFragment();
            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.resource_name_images_dialog_helper, container, false);
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            return v;
        }
    }

    private void showFirstTimeOptions(boolean b) {
        if(b){
            SharedPreferences settings = this.getActivity().getSharedPreferences(IntentConfigVariables.getFirstTimeOptions(), 0);
            Boolean firstTime = settings.getBoolean(IntentConfigVariables.FirstTimeOptions.nameDetailImagesHelpV1702, true);
            if(firstTime){

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = MyDialogFragment.newInstance();
                newFragment.show(ft, "helper_dialog");

                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(IntentConfigVariables.FirstTimeOptions.nameDetailImagesHelpV1702, false);
                editor.commit();
            }
        }
    }
}

