package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by saikrishna on 9/23/13.
 */
public class LikesParser {
    public static String getTotalCount(String urlData){
        try {
            JSONObject main_obj = new JSONObject(urlData);
            JSONObject metaObj = main_obj.getJSONObject("meta");
            return ""+metaObj.getInt("ranks_sum")+"("+metaObj.getInt("total_no_ranks")+")";
        }catch (Exception e){
            return "0";
        }
    }
    public static int getMyRank(String urlData){
        try {
            JSONObject main_obj = new JSONObject(urlData);
            JSONArray main_objects = main_obj.getJSONArray("objects");
            if(main_objects.length() >= 2){
                Log.d("backpaper2","somethig impossible happend length is > 2");
                return -1;
            }else if(main_objects.length() == 0){
                Log.d("bacpaper2"," user has not yet rated this one");
                return -2;
            }else{
                JSONObject thisObj = main_objects.getJSONObject(0);
                return thisObj.getInt("rank_given");
            }

        }catch (Exception e){
            Log.d("backpaper2", "error while trying to get own rank " + e.toString());
            return -1;
        }
    }
}
