package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.w4rlock.backpaper2.models.Comments;
import com.w4rlock.backpaper4.R;

public class MainListViewAdapter extends ArrayAdapter<Comments> {

    Context context;
    int layoutResourceId;
    ArrayList<Comments> data = null;
    public MainListViewAdapter(Context context,int layoutResourceId, ArrayList<Comments> data){
        super(context,layoutResourceId,data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        CommentHolder commentHolder = null;
        if(position == 0){

        }
        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layoutResourceId, parent,false);
            commentHolder = new CommentHolder();
            commentHolder.commentData = (TextView)row.findViewById(R.id.comment_data);
            commentHolder.commentUname = (TextView)row.findViewById(R.id.comment_uname);
            row.setTag(commentHolder);
        }else{
            commentHolder = (CommentHolder)row.getTag();
        }
        try{
            String this_comment_data = data.get(position).getCommentData();
            String this_comment_uname = "by "+data.get(position).getCommentUname() + " on " + data.get(position).getCommentTstamp().substring(0,10);
            Log.d("backpaper2-username-comment-data",this_comment_uname);
            commentHolder.commentUname.setText(this_comment_uname);
            commentHolder.commentData.setText(this_comment_data);
        }catch(Exception e){
            Log.d("backpaper2", "error while displaying image@ " + position + e);
        }
        Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
        row.startAnimation(animation);
        return row;
    }

    static class CommentHolder{
        TextView commentData;
        TextView commentUname;
    }
}
