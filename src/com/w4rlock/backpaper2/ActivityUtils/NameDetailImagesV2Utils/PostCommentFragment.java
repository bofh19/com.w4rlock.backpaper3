package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Comments;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.backpaper4.ImageNavV2;
import com.w4rlock.backpaper4.NameDetailImagesV2;
import com.w4rlock.backpaper4.R;
import com.w4rlock.cacheManager.getURLdata;

/**
 * Created by krishna on 19/7/13.
 */
public class PostCommentFragment extends Fragment {
    private static final String DEBUG_TAG = "backpaper2-comments-fragment";
    private Context context;
    private Toast mToast=null;
    private Name given_name=null;
    private NameImage given_name_image = null;
    private ViewGroup rootView=null;
    OnPostButtonClickedListner pButtonListner;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(
                R.layout.resource_name_detail_images_v2_comments_comment_fragment,container, false);
        context = this.getActivity().getApplicationContext();
        Intent intent = this.getActivity().getIntent();
        if(this.getActivity().getClass() == NameDetailImagesV2.class){
            given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameDetailIntentMessage());
        }else if(this.getActivity().getClass() == ImageNavV2.class){
            given_name_image = (NameImage) intent.getSerializableExtra(IntentConfigVariables.getNameImgNavMessageImages());
        }
        Button button = (Button) rootView.findViewById(R.id.test_comment_submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                goOnSubmitButton();
            }
        });
        Button button_edit = (Button) rootView.findViewById(R.id.edit_details);
        button_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                makeAllVisible();
            }
        });
        hideLayouts();
        GetCSRFToken();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            pButtonListner = (OnPostButtonClickedListner)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+" must implement post button listner");
        }
    }

    private void hideLayouts() {
        SharedPreferences settings =  context.getSharedPreferences(IntentConfigVariables.CommentsDetailsPref, 0);
        EditText uname_txt = (EditText)rootView.findViewById(R.id.editText_uname);
        String username_str = (settings.getString(IntentConfigVariables.CommentsDetailsUsernameValue,null));
        if(username_str != null){
            uname_txt.setText(username_str);
            View uname_view = rootView.findViewById(R.id.layout_uname);
            uname_view.setVisibility(View.GONE);
        }
        EditText email_txt = (EditText)rootView.findViewById(R.id.editText_email);
        String email_str = settings.getString(IntentConfigVariables.CommentsDetailsEmailValue,null);
        if(email_str != null){
            email_txt.setText(email_str);
            View email_uname = rootView.findViewById(R.id.layout_email);
            email_uname.setVisibility(View.GONE);
        }
    }


    private void removeCommentData() {
        EditText data = (EditText)this.getActivity().findViewById(R.id.editText_comment);
        data.setText("");
    }

    private void makeAllVisible() {
        View uname_view = rootView.findViewById(R.id.layout_uname);
        uname_view.setVisibility(View.VISIBLE);
        View email_uname = rootView.findViewById(R.id.layout_email);
        email_uname.setVisibility(View.VISIBLE);
    }

    private void goOnSubmitButton() {
        if(!NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            displayToast("Not able to Connect to Internet");
            return;
        }
        class CommentPost extends SCommentPost {
            @Override
            public void failed(String message){
                displayToast(message);
            }
            @Override
            public void postResult(Object result){
                try{
                    displayToast(result.toString());
                    removeCommentData();
                    SharedPreferences settings_comments =  context.getSharedPreferences(IntentConfigVariables.CommentsDetailsPref, 0);
                    SharedPreferences.Editor comments_detail_editor = settings_comments.edit();
                    comments_detail_editor.putLong(IntentConfigVariables.CommentsLastCommentEpoch,System.currentTimeMillis());
                    comments_detail_editor.commit();
                }catch(Exception e){
                    Log.d(DEBUG_TAG,"error while post result display toast "+e.toString());
                }
            }
        }
        EditText uname_txt = (EditText)this.getActivity().findViewById(R.id.editText_uname);
        EditText email = (EditText)this.getActivity().findViewById(R.id.editText_email);
        EditText data = (EditText)this.getActivity().findViewById(R.id.editText_comment);
        String comment_name = String.valueOf(uname_txt.getText());
        String comment_data = String.valueOf(data.getText());
        String comment_email = String.valueOf(email.getText());
        Log.d(DEBUG_TAG, comment_name);
        Log.d(DEBUG_TAG,comment_data);
        Log.d(DEBUG_TAG,comment_email);
        SharedPreferences settings_comments =  context.getSharedPreferences(IntentConfigVariables.CommentsDetailsPref, 0);
        SharedPreferences.Editor comments_detail_editor = settings_comments.edit();
        long timeDiff = System.currentTimeMillis() - settings_comments.getLong(IntentConfigVariables.CommentsLastCommentEpoch,0);
        if(timeDiff/1000 < 5){
            displayToast("Please Wait "+timeDiff/1000+" before commenting again");
            return;
        }
        if(comment_data.length() < 5){
            displayToast("Please enter 5 more characters in comment data");
            return;
        }
        if(comment_email.length() == 0){
            comment_email = "anonymous@anonymous.com";
            comments_detail_editor.putString(IntentConfigVariables.CommentsDetailsEmailValue,null);
        }else{
            comments_detail_editor.putString(IntentConfigVariables.CommentsDetailsEmailValue,comment_email);
        }
        String comment_extra_field = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if(comment_name.length() == 0){
            comment_name = "anonymous";
            comments_detail_editor.putString(IntentConfigVariables.CommentsDetailsUsernameValue,null);
        }else{
            if(comment_name.toLowerCase().contains("bofh") || comment_name.toLowerCase().contains("w4rlock") || comment_name.toLowerCase().contains("admin")){
                if(!comment_extra_field.equals("5d69af7a37d9a967")){
                    displayToast("Cannot Use bofh or w4rlock or admin. please change them.");
                    return;
                }
            }
            comments_detail_editor.putString(IntentConfigVariables.CommentsDetailsUsernameValue,comment_name);
        }
        comments_detail_editor.commit();
        String url = ServerVariables.getCommentsUrl();
        String domain = ServerVariables.getDomain();
        SharedPreferences settings =  context.getSharedPreferences(IntentConfigVariables.CsrfTokenPref, 0);
        String csrf_token = null;
        if(( csrf_token = settings.getString(IntentConfigVariables.CsrfTokenValue,null)) == null){
            displayToast("Some thing has Gone wrong. Sorry About that !!!!. Press back and come back here.");
            return;
        }

//        String comment_uid = "A_"+given_name.getId();
        String comment_uid = "U";
        if(this.getActivity().getClass() == NameDetailImagesV2.class){
            comment_uid = given_name.getCommentUid();
        }else if(this.getActivity().getClass() == ImageNavV2.class){
            comment_uid = given_name_image.getCommentUid();
        }
        removeCommentData();
        CommentPost new_comment = new CommentPost();
        new_comment.postData(url,domain,csrf_token,comment_uid,comment_data,comment_name,comment_email,comment_extra_field);
        ////
        Comments current_comment = new Comments();
        current_comment.setCommentId("-1");
        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:MM:ss");
        TimeZone tz = TimeZone.getTimeZone("UTC");
        dateFormatUTC.setTimeZone(tz);
        Date new_date = new Date();
        String tstamp = (dateFormatUTC.format(new_date)).toString();
        current_comment.setCommentTstamp(tstamp);
        current_comment.setCommentUname(comment_name);
        current_comment.setCommentData(comment_data);
        current_comment.setCommentEmail(comment_email);
        pButtonListner.onPostButtonClicked(current_comment);
        ////
        hideLayouts();
//        CommentsFragment.
    }

    private void GetCSRFToken() {
        class GetCsrfToken extends getURLdata {
            GetCsrfToken(){
                setReadTimeOut(15000);
                setConnTimeOut(15000);
            }
            @Override
            public void postresult(Object result2) {
                try{
                    Log.d("backpaper2-apost result csrf token", (String) result2);
                    String csrf_token = (String)result2;
                    SharedPreferences settings =  context.getSharedPreferences(IntentConfigVariables.CsrfTokenPref, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(IntentConfigVariables.CsrfTokenValue, csrf_token);
                    editor.commit();
                }catch(Exception e){
                    Log.d("backpaper2-apost-result-csrf-token","unable to get csrftoken "+e.toString());
                    displayToast("Unable to access Server. You May be Unable to Comment");
                }
            }
        }
        GetCsrfToken getCsrfToken = new GetCsrfToken();
        getCsrfToken.getData(ServerVariables.getCommentsUrl());
    }

    private void displayToast(String msg){
        try{
            mToast.setText(msg);
            mToast.show();
        }catch(Exception e){
            mToast = Toast.makeText(this.getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    public interface OnPostButtonClickedListner{
        public void onPostButtonClicked(Comments comment);
    }

    public void setGivenNameImage(NameImage current_name_image){
        given_name_image = current_name_image;
    }
}
