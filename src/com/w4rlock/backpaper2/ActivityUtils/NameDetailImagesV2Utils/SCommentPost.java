package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

import android.os.AsyncTask;
import android.util.Log;

import com.w4rlock.backpaper2.utils.HashingUtility;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by krishna on 17/7/13.
 * ASSUMES CONNECTION TO NETWORK EXISTS
 * input
 * post url
 * domain
 * csrf_token
 * comment_uid
 * comment_data
 * comment_name
 * comment_email
 * comment_extra_field
 *
 * posts data to server by adding an hmac secret_key
 * and gets result
 */
public class SCommentPost {
    public static final String DEBUG_TAG = "backpaper2-post-network";
    public String result = null;

    public void postData(String url,
                         String domain,
                         String csrf_token,
                         String comment_uid,
                         String comment_data,
                         String comment_name,
                         String comment_email,
                         String comment_extra_field){
        new DownloadWebPageText().execute(
                url,
                domain,
                csrf_token,
                comment_uid,
                comment_data,
                comment_name,
                comment_email,
                comment_extra_field
        );
    }

    public void postResult(Object result2) {
        result = (String) result2;
        Log.d(DEBUG_TAG, result);
    }

    public void failed(String message){
        Log.d(DEBUG_TAG,message);
    }

    private class DownloadWebPageText extends AsyncTask<Object, Object, Object> {

        @Override
        protected Object doInBackground(Object... params) {
            String url = (String) params[0];
            String domain = (String)params[1];
            String csrf_token = (String) params[2];
            String comment_uid = (String) params[3];
            String comment_data = (String) params[4];
            String comment_name = (String) params[5];
            String comment_email = (String) params[6];
            String comment_extra_field = (String) params[7];
            try {
                return downloadUrl(
                        url,
                        domain,
                        csrf_token,
                        comment_uid,
                        comment_data,
                        comment_name,
                        comment_email,
                        comment_extra_field);
            } catch (IOException e) {
                return "unable to get the data sry :(";
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            Log.d(DEBUG_TAG,"--------");
            Log.d(DEBUG_TAG,result.toString());
            if (result == null || result == " " || result == "") {
                result = "-1";
            } else {
                postResultInternal(result);
            }
        }

        private String downloadUrl(
                String url,
                String domain,
                String csrf_token,
                String comment_uid,
                String comment_data,
                String comment_name,
                String comment_email,
                String comment_extra_field) throws IOException {
            //InputStream is = null;
            Log.d(DEBUG_TAG,url);
            Log.d(DEBUG_TAG,domain);
            Log.d(DEBUG_TAG,csrf_token);
            Log.d(DEBUG_TAG,comment_uid);
            Log.d(DEBUG_TAG,comment_data);
            Log.d(DEBUG_TAG,comment_name);
            Log.d(DEBUG_TAG,comment_email);
            Log.d(DEBUG_TAG,comment_extra_field);
            String secret_key = "";

            String[] keys = HashKeys.getKeys();

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            final BasicCookieStore cookieStore =  new BasicCookieStore();
            BasicClientCookie csrf_cookie = new BasicClientCookie("csrftoken", csrf_token);
            csrf_cookie.setDomain(domain);
            cookieStore.addCookie(csrf_cookie);
            HttpContext localContext = new BasicHttpContext();
            localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
            post.setHeader("Referer", url);
            post.setHeader("X-CSRFToken", csrf_token);

            SimpleDateFormat dateFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:MM:ss");
            TimeZone tz = TimeZone.getTimeZone("UTC");
            dateFormatUTC.setTimeZone(tz);
            Date new_date = new Date();

            String tstamp = (dateFormatUTC.format(new_date)).toString();
            char second_char = tstamp.charAt(tstamp.length()-1);
            Integer second = Integer.valueOf(String.valueOf(second_char));
//	    System.out.println(tstamp);
//	    System.out.println(DEV_KEYS[second]);

            String hmac_message = tstamp+comment_data+comment_uid;
            String hmac_key = keys[second];

            try {
                secret_key = HashingUtility.HMAC_MD5_encode(hmac_key, hmac_message);
//                System.out.println(secret_key);
            } catch (Exception e) {
                failed("failed something has gone wrong.");
            }

            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("csrfmiddlewaretoken",csrf_token));
                nameValuePairs.add(new BasicNameValuePair("tstamp",tstamp));
                nameValuePairs.add(new BasicNameValuePair("comment_uid",comment_uid));
                nameValuePairs.add(new BasicNameValuePair("comment_data",comment_data));
                nameValuePairs.add(new BasicNameValuePair("comment_name",comment_name));
                nameValuePairs.add(new BasicNameValuePair("comment_email",comment_email));
                nameValuePairs.add(new BasicNameValuePair("extra_field",comment_extra_field));
                nameValuePairs.add(new BasicNameValuePair("secret_key",secret_key));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = client.execute(post,localContext);
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                String output = "";
                while ((line = rd.readLine()) != null) {
                    output = output+line;
                }
                return output;
            } catch (IOException e) {
                Log.d(DEBUG_TAG,e.toString());
                failed("Something has Gone Wrong. Network Error Probably");
                return null;
            }
        }
    }

    private void postResultInternal(Object result) {
        try{
            String message = result.toString();
            if(message.toLowerCase().contains("failed")){
                failed("Something Has Gone Wrong, Server Error Probably");
            }
            else{
                postResult(result);
            }
        }catch (Exception e){
            failed("Something Has Gone Wrong, Server Error Probably: Message: "+result.toString());
        }

    }

}
