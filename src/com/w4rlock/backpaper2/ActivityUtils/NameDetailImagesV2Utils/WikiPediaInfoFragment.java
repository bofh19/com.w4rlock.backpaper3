package com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.backpaper4.R;
import com.w4rlock.cacheManager.getURLdata;

/**
 * Created by skarumuru on 11/27/13.
 */
public class WikiPediaInfoFragment extends Fragment {
    private static String DEBUG_TAG = "backpaper2-webview-fragment";
    private ViewGroup rootView;
    private WebView wikipediaWebView;
    private Name given_name=null;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.resource_name_detail_images_v2_wikipedia_webview, container, false);
        wikipediaWebView = (WebView) rootView.findViewById(R.id.name_detail_images_v2_wikipedia_webview);
        Intent intent = this.getActivity().getIntent();
        given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameDetailIntentMessage());
        wikipediaWebView.setWebViewClient(new WikiPediaWebViewClient());
        if(NetworkChecker.getNetStatus(this.getActivity().getApplicationContext())){
            getWebViewData();
        }
        return rootView;
    }

    private void getWebViewData() {
        drawLoadingText();
        String name = given_name.getName();
        name = name.trim().replaceAll(" ","_");
        name = name.replaceAll("[0-9]","");
        name = name.replaceAll("\\(", "");
        name = name.replaceAll("\\)", "");
        String wikipediaUrl = ServerVariables.getWikiPediaFullUrl(name);
        Log.d(DEBUG_TAG, "wiki pedia url " + wikipediaUrl);
        class GetWikiPediaData extends getURLdata {
            public void postresult(Object result2){
                String data = (String)result2;
                try {
                    JSONObject mainObj = new JSONObject(data);
                    String htmlData = mainObj.getJSONObject("parse").getJSONObject("text").getString("*");
                    drawWidthData(htmlData);
                }catch (Exception e){
                    Log.d(DEBUG_TAG,"got error "+e.toString());
                    drawNoDataFound();
                }
            }

        }
        GetWikiPediaData getWikiPediaData = new GetWikiPediaData();
        getWikiPediaData.getData(wikipediaUrl);
    }
    private void drawLoadingText(){
        wikipediaWebView.loadData("Loading Data From WikiPedia Please give a second","text/text","UTF-8");
    }
    private void drawNoDataFound() {
        wikipediaWebView.loadData("No DATA FOUND","text/text","UTF-8");
    }

    private void drawWidthData(String htmlData) {
        wikipediaWebView.getSettings().setJavaScriptEnabled(true);
        wikipediaWebView.loadDataWithBaseURL("http://m.wikipedia.org/", htmlData, "text/html", "UTF-8", "");
    }

    private class WikiPediaWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
