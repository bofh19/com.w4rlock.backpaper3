package com.w4rlock.backpaper2.Logger;

/**
 * Created by w4rlock on 15/6/13.
 */

import android.content.Context;

import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import com.w4rlock.backpaper4.R;

public class GTracker {
   public static void trackEvent(Context context,String category,String action,String label,Long value){
       GoogleAnalytics gaInstance = GoogleAnalytics.getInstance(context);
       Tracker gaTracker = gaInstance.getTracker(context.getResources().getString(R.string.ga_trackingId));
       gaTracker.sendEvent(category,action,label,value);
       gaTracker.close();
   }
   public static void trackActivity(Context context,String activityName){
       GoogleAnalytics gaInstance = GoogleAnalytics.getInstance(context);
       Tracker gaTracker = gaInstance.getTracker(context.getResources().getString(R.string.ga_trackingId));
       gaTracker.sendView("com.w4rlock.backpaper."+activityName);
       gaTracker.close();
   }
}
