package com.w4rlock.backpaper2.Logger;

import android.util.Log;

/**
 * Created by krishna on 5/20/13.
 */
public class L {
    public static final String MAIN_APP_NAME = "backpaper2";
    public static void d(Class c,String message){
        Log.d(MAIN_APP_NAME + "-"+c.toString(),message);
    }

    public static void d(Class c,String m1,String m2){
        Log.d(MAIN_APP_NAME+"-"+c.toString()+"-"+m1,m2);
    }
}
