package com.w4rlock.backpaper2.Logger;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.w4rlock.backpaper2.utils.NetworkChecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by krishna on 5/29/13.
 */
public class PostDevAcc {
    public static final String DEBUG_TAG = "backpaper2-post-network-devAcc";
    public static final String TIMEOUT_TAG = DEBUG_TAG + "-timeout-network1234567987654";

    private Context context;
    SharedPreferences sharedPrefs;
    public PostDevAcc(Context context) {
        this.context = context;
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void postDevAccData(int img_id, int type_taken) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        class PostUrlData extends postDevAccDataService {
            @Override
            public void postResult(Object result2) {
                Log.d(DEBUG_TAG+"-result", result2.toString());
            }
        }
        if(sharedPrefs.getBoolean("stats_pref", true)){
            if(NetworkChecker.getNetStatus(context)){
                PostUrlData postData = new PostUrlData();
                postData.getData("http://backpaper.w4rlock.in/user/devAcc/?dev_id=" + deviceId + "&img_id=" + img_id + "&type_taken=" + type_taken);
            }
        }
    }


    class postDevAccDataService {
        public String result = null;

        public void getData(String url) {
            new DownloadWebPageText().execute(url);
        }

        public void postResult(Object result2) {
            result = (String) result2;
            Log.d(DEBUG_TAG, result);
        }

        private class DownloadWebPageText extends AsyncTask<Object, Object, Object> {

            @Override
            protected Object doInBackground(Object... params) {
                String url = (String) params[0];
                try {
                    return downloadUrl(url);
                } catch (IOException e) {
                    return "unable to get the data sry :(";
                }
            }

            @Override
            protected void onPostExecute(Object result) {
                try{
                    Log.d(DEBUG_TAG, "--------");
                    Log.d(DEBUG_TAG, result.toString());
                    if (result == null || result == " " || result == "") {
                        result = "-1";
                    } else {
                        postResult(result);
                    }
                }catch(Exception e){
                    Log.d(DEBUG_TAG,"crashed -  "+e.toString());
                }
            }

            private String downloadUrl(String myUrl)
                    throws IOException {
                //InputStream is = null;
                InputStream is = null;

                try {
                    Log.d("backpaper2-network", (myUrl));
                    URL url = new URL(myUrl);
                    HttpURLConnection conn = (HttpURLConnection) url
                            .openConnection();
                    conn.setReadTimeout(20000 /*10000 milliseconds */);
                    conn.setConnectTimeout(30000 /*15000 milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    // Starts the query
                    try {
                        conn.connect();
                    } catch (Exception e) {
                        Log.d("backpaper2-network", "Error while connection: " + e);
                        return TIMEOUT_TAG;
                    }
                    Log.d("backpaper2-network", "connection request above");
                    int response = conn.getResponseCode();
                    Log.d(DEBUG_TAG, " " + response);
                    try {
                        is = conn.getInputStream();
                    } catch (Exception e) {
                        Log.d("backpaper2-network", "error while getting inputstream in do background: " + e);
                        return null;
                    }
                    String contentAsString = readAll(is);
                    return contentAsString;

                } catch (Exception e) {
                    Log.d("backpaper2-network", e.toString());
                    Log.d("backpaper2-network", "x");
                    return null;
                } finally {
                    if (is != null) {
                        is.close();
                    } else {
                        return null;
                    }
                }
            }

            public String readAll(InputStream stream) throws IOException,
                    UnsupportedEncodingException {
                StringBuilder sb = new StringBuilder();
                String s;
                try {
                    Reader reader = new InputStreamReader(stream, "UTF-8");
                    BufferedReader buf = new BufferedReader(reader);
                    while (true) {
                        s = buf.readLine();
                        if (s == null || s.length() == 0)
                            break;
                        sb.append(s);
                    }
                    return sb.toString();
                } catch (Exception e) {
                    Log.d("backpaper2-post-network", "error on readall ie catching input stream " + e.toString());
                    return " ";
                }
            }
        }

    }
}

