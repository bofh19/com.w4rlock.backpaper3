package com.w4rlock.backpaper2.configs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class GetDisplayParams {
	public static String getDisplayAsQueryParams(Context context){
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		int width = (int)((100*displayMetrics.density)+0.5);
		int height = (int)((160*displayMetrics.density)+0.5);
		String params = "width="+Integer.valueOf(width)+"&height="+Integer.valueOf(height)+"&device_density="+displayMetrics.density;
		return params;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static String getDeviceDisplayAsQueryParams(Context context){
		int width;
		int height;
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		try{
			if(android.os.Build.VERSION.SDK_INT >= 13){
				Point size = new Point();
				display.getSize(size);
				width = size.x;
				height = size.y;
			}else{
				width = display.getWidth();
				height = display.getHeight();
			}
		}catch(Exception e){
			Log.d("backpaper2-DisplayParams",e.toString());
			return null;
		}
		String params = "device_width="+Integer.valueOf(width)+"&device_height="+Integer.valueOf(height);
		return params;
	}
	
	public static String getFullDisplayAsQueryParams(Context context){
		String params1 = getDisplayAsQueryParams(context);
		String params2 = getDeviceDisplayAsQueryParams(context);
		return params1+"&"+params2;
	}
	
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static int getDeviceWidth(Context context){
		int width;
//		int height;
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		try{
			if(android.os.Build.VERSION.SDK_INT >= 13){
				Point size = new Point();
				display.getSize(size);
				width = size.x;
//				height = size.y;
			}else{
				width = display.getWidth();
//				height = display.getHeight();
			}
		}catch(Exception e){
			Log.d("backpaper2-DisplayParams",e.toString());
			return -1;
		}
		return width;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static int getDeviceHeight(Context context){
//		int width;
		int height;
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		try{
			if(android.os.Build.VERSION.SDK_INT >= 13){
				Point size = new Point();
				display.getSize(size);
//				width = size.x;
				height = size.y;
			}else{
//				width = display.getWidth();
				height = display.getHeight();
			}
		}catch(Exception e){
			Log.d("backpaper2-DisplayParams",e.toString());
			return -1;
		}
		return height;
	}
}
