package com.w4rlock.backpaper2.configs;

public class IntentConfigVariables {
	private static final String NAME_DETAIL_INTENT_MESSAGE = "com.w4rlock.backpaper2.name_detail";
	private static final String NAME_IMG_NAV_MESSAGE_IMAGES = "com.w4rlock.backpaper2.image_nav.images";
	private static final String NAME_IMG_NAV_MESSAGE_NAME = "com.w4rlock.backpaper2.image_nav.name";


	
	/* LIVE WALLPAPER VARIABLES*/
    /* default wallpaper variables */
    public static final String lwpDefaultLocationPref = "com.w4rlock.backpaper2.lwp.default_wallpaper_image";
    public static final String getLwpDefaultLocationValue = "com.w4rlock.backpaper2.lwp.default_wallpaper_image_value";
    /* default wallpaper variables ends */
	public static final String LWP_IMGS_LOCATION_PREF = "com.w4rlock.backpaper2.lwp.img_location_prefs";
//	public static final String LWP_IMGS_COUNTER = "com.w4rlock.backpaper2.lwp.img_counter";
	public static final String LWP_IMGS_LOCATION_VAR = "com.w4rlock.backpaper2.lwp.img_location_var";
	public static final String LWP_IMGS_COUNTER_VAR = "com.w4rlock.backpaper2.lwp.img_counter_var";
	/* LIVE WALLPAPER LISTS VARIABLES - currently fixed 4 lists*/
	public static final String LWP_IMGS_LOCATION_VAR_L_0 = "com.w4rlock.backpaper2.lwp.img_location_var_l_0";
	public static final String LWP_IMGS_LOCATION_VAR_L_1 = "com.w4rlock.backpaper2.lwp.img_location_var";
	public static final String LWP_IMGS_LOCATION_VAR_L_2 = "com.w4rlock.backpaper2.lwp.img_location_var_l_2";
	public static final String LWP_IMGS_LOCATION_VAR_L_3 = "com.w4rlock.backpaper2.lwp.img_location_var_l_3";
	public static final String LWP_IMGS_LOCATION_VAR_L_4 = "com.w4rlock.backpaper2.lwp.img_location_var_l_4";
	/* LIVE WALLPAPER VARIABLES LIST ENDS HERE*/

    /* ADDS STATUS VARIABLE */
    public static final String ADD_MOB_STATUS_PREF = "com.w4rlock.backpaper2.addmob_status_pref";
    public static final String ADDMOB_STATUS_VAR = "com.w4rlock.backpaper2.addmob_status_var";
    /* ADDS STATUS VARIABLE  ENDS HERE*/

    /* CSRF TOKEN VARIABLE STARTS HERE */
    public static final String CsrfTokenPref = "com.w4rlock.backpaper2.csrf_token_pref";
    public static final String CsrfTokenValue = "com.w4rlock.backpaper2.csrf_token_value";
    /* CSRF TOKEN VARIABLES ENDS HERE */

    /* Comments username and email value starts here */
    public static final String CommentsDetailsPref = "come.w4rlock.backpaper2.comments.pref";
    public static final String CommentsDetailsUsernameValue = "com.w4rlock.backpaper2.comments.username.value";
    public static final String CommentsDetailsEmailValue = "com.w4rlock.backpaper2.comments.email.value";
    public static final String CommentsLastCommentEpoch = "com.w4rlock.backpaper2.comments.tstamp.epoch";

    public static final String LikesDetailsPref = "come.w4rlock.backpaper2.likes.pref";
    public static final String LikesLastCommentEpoch = "com.w4rlock.backpaper2.likes.tstamp.epoch";
    public static String LikesCurrentStatusVar(String commentId){
        return "com.w4rlock.backpaper2.likes.values"+commentId;
    }
    /* Comments username and email value ends here */
	
	private static final String FIRST_TIME_OPTIONS = "com.w4rlock.backpaper2.first_time_options";
	
	public static final class FirstTimeOptions{
		public static final String slidingMenu = "com.w4rlock.backpaper2.first_time_options.sliding_menu";
		public static final String lastCategorieUrl = "com.w4rlock.backpaper2.first_time.last_categorie_id";
        public static final String nameDetailImagesHelp = "com.w4rlock.backpaper2.first_time.name_detail_images_help";
        public static final String nameDetailImagesHelpV1702 = "com.w4rlock.backpaper2.first_time.name_detail_images_help.V1702";
        public static final String imageViewDetailHelp = "com.w4rlock.backpaper2.first_time.image_view_detail_help";
//        public static final String gcmRegisterId = "com.w4rlock.backpaper2.first_time.gcm_register_id";
    //    public static final String gcmRegisterStatus = "com.w4rlock.backpaper2.first_time.gcm_register_status";
	}
	public static String getNameImgNavMessageImages() {
		return NAME_IMG_NAV_MESSAGE_IMAGES;
	}

	public static String getNameImgNavMessageName() {
		return NAME_IMG_NAV_MESSAGE_NAME;
	}

	public static String getNameDetailIntentMessage() {
		return NAME_DETAIL_INTENT_MESSAGE;
	}

	public static String getFirstTimeOptions() {
		return FIRST_TIME_OPTIONS;
	}
}
