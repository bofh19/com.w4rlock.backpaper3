package com.w4rlock.backpaper2.configs;



public class ServerVariables {
    public static final String server_domain = "backpaper.w4rlock.in";
	public static final String server = "http://backpaper.w4rlock.in";
    //public static final String server_domain = "192.168.0.103";
    //public static final String server = "http://192.168.0.103:8000";
	public static final String api_ext = "/api/v1/";
	public static final String format = "format=json";
	public static final String name_images_ext = "name_images";
	public static final String name_ext = "name";
	public static final String categories = "categories";
	public static final String feedbackReport = "/feedback/report/?img_id=";
    public static final String infoUrl = "info";
    public static final String feedbackUser = "/feedback/user_feedback/";
    public static final String feedbackRequest = "/feedback/request_new/";
    public static final String name_images_popular_ext = "name_images_popular";
    public static final String names_popular_ext = "names_popular";
    public static final String s_comments_url = "/scomments/apost/";
    public static final String comments_ext = "comments";
    public static final String s_likes_url = "/scomments/alike/";
    public static final String s_likes_sum_url = "/scomments/get_likes_sum/";
    public static final String likes_ext = "likes";
    public static final int getConnTimeOut = 20000;
    public static final int getReadTimeOut = 30000;
    public static final int postTimeOut = 45000;
    public static final String startAppDEVID = "107718730";
    public static final String startAppAPPID = "207882783";
    public static String getDomain(){
        return server_domain;
    }
    public static String getServerUrl(){
		return server;
	}
	private static String getServerAPIUrl(){
		return server+api_ext;
	}
    public static String getInfoUrl(){
        return getServerAPIUrl()+infoUrl+"/?"+format;
    }
    public static String getNameUrlWithParamsNoLimit(String params){
        return getServerAPIUrl()+name_ext+"/?"+format+"&"+params+"&limit=0";
    }

    public static String getNameUrlNoLimitWithParamsCategorie(String params,String categorieId){
		return getNameUrlWithParamsNoLimit(params)+"&categorie__id="+categorieId;
	}
    public static String getNamePopularUrlNoLimitWithParams(String params){
        return getServerAPIUrl()+names_popular_ext+"/?"+format+"&"+params+"&limit=0";
    }
	public static String getCategoriesUrl(){
		return getServerAPIUrl()+categories+"/?"+format+"&limit=0";
	}
	
	public static String getImgUrl(String url){
		return getServerUrl()+url;
	}
    public static String getNameImagesUrlWithParams(String params){
        return getServerAPIUrl()+name_images_ext+"/?"+format+"&"+params;
    }
    public static String getNameImagesPopularUrlWithParams(String params){
        return getServerAPIUrl()+name_images_popular_ext+"/?"+format+"&"+params;
    }
	public static String getNameAllImageUrlWithParams(int id,String params){
		return getServerAPIUrl()+name_images_ext+"/?"+format+"&name="+Integer.valueOf(id).toString()+"&"+params;
	}
	
	public static String getNextUrl(String next_url){
		return getServerUrl()+next_url;
	}

	public static String getNameSearchUrlWithParamsNoLimit(String params,String query){
		return ServerVariables.getNameUrlWithParamsNoLimit(params)+"&name__icontains="+query;
	}

    public static String getFeedBackReportUrl(int image_id){
        return server+ServerVariables.feedbackReport+Integer.toString(image_id);
    }

    public static String getFeedbackRequestUrl(){
        return server+ServerVariables.feedbackRequest;
    }

    public static String getFeedbackUserUrl(){
        return server+ServerVariables.feedbackUser;
    }

    public static String getCommentsUrl(){
        return server+ServerVariables.s_comments_url;
    }

    public static String getCommentsList(String uniqueId){
        return getServerAPIUrl()+comments_ext+"/?"+format+"&unique_group_id="+uniqueId;
    }

    public static String getLikePostUrl(){
        return server+ServerVariables.s_likes_url;
    }

    public static String getLikesListUrl(String uniqueId){
        return server+ServerVariables.s_likes_sum_url+"?unique_group_id="+uniqueId;
    }

    public static String getLikesListOwnUrl(String uniqueId,String devId){
        return getServerAPIUrl()+likes_ext+"/?"+format+"&unique_group_id="+uniqueId+"&dev_id="+devId;
    }

    public static String getWikiPediaFullUrl(String main_string){
        return "http://en.wikipedia.org/w/api.php?action=parse&page="+main_string+"&format=json&prop=text&mobileformat&redirects=";
    }
}
