package com.w4rlock.backpaper2.gcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper4.NameDetailImages;
import com.w4rlock.backpaper4.R;

/**
 * Created by krishna on 5/21/13.
 */
public class GCMNotify {
    private static final int NotificationId = 81258115;
    public static void displayNotify(Context context, Bundle bundle) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        if(sPref.getBoolean("notify_pref",false)){
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(bundle.getString("title"))
                            .setContentText(bundle.getString("message"))
                            .setAutoCancel(true);
            try{
                NotificationCompat.InboxStyle inboxStyle =new NotificationCompat.InboxStyle();
                Log.d("backpaper2","here");
                String actors_str =bundle.getString("actors");
                Log.d("backpaper2","here");
                Log.d("backpaper-2-actors-list",actors_str);
                String[] events = actors_str.split(",");
                Log.d("backpaper2","here");
                inboxStyle.setBigContentTitle(bundle.getString("title"));
                Log.d("backpaper2","here");
                for (int i=0; i < events.length; i++) {
//                    Log.d("backpaper2-notification","3: "+events[i]);
                    inboxStyle.addLine(events[i]);
                }
                Log.d("backpaper2","here");
                mBuilder.setStyle(inboxStyle);
            }catch(Exception e){
                Log.d("backpaper2-notification","2: "+e.toString());
            }
//            try {
//                URL url = new URL(bundle.getString("test_url"));
//                Log.d("backpaper2-notification", "2: " + bundle.getString("test_url"));
//                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
//                bigPictureStyle.bigPicture(image);
//                mBuilder.setStyle(bigPictureStyle);
//            } catch (Exception e) {
//                Log.d("backpaper2-notification", "1: " + e.toString());
//            }


            Name recentName = new Name();
            recentName.setId(-1);
            recentName.setName("Recent Images");
                Intent resultIntent = new Intent(context, NameDetailImages.class);
                resultIntent.putExtra(IntentConfigVariables.getNameDetailIntentMessage(),recentName);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addParentStack(NameDetailImages.class);
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(NotificationId, mBuilder.build());
        }
    }
}
