package com.w4rlock.backpaper2.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by krishna on 5/21/13.
 */
public class GCMRegisterOnServer {

    public static void register(Context context){
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("backpaper2", deviceId);
        Log.d("backpaper2-build",android.os.Build.MANUFACTURER);

        if(GCMRegistrar.getRegistrationId(context).equals("")){
            Log.d("backpaper2-GCM", GCMServerSettings.senderId);
            GCMRegistrar.register(context, GCMServerSettings.senderId);
        }else{
            Log.d("backpaper2-GCM","GCM REGISTRATION ID: " + GCMRegistrar.getRegistrationId(context));
            SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
            Boolean status = sPref.getBoolean("notify_pref",false);
            //sending notification status daily once
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            Date date = new Date();
            String todayDateString = dateFormat.format(date);
            SharedPreferences settings = context.getSharedPreferences(IntentConfigVariables.getFirstTimeOptions(), 0);
            String todaySetValue = settings.getString(todayDateString,null);
            Log.d("backpaper2-GCM",todayDateString);
            if(todaySetValue == null){
                String regId = GCMRegistrar.getRegistrationId(context).toString();
                registerBackground(deviceId, regId, status);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(todayDateString, "sent");
                editor.commit();
            }
        }
    }

    public static void registerNotify(Context context){
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("backpaper2", deviceId);
        Log.d("backpaper2-build",android.os.Build.MANUFACTURER);
        if(GCMRegistrar.getRegistrationId(context).equals("")){
            Log.d("backpaper2-GCM", GCMServerSettings.senderId);
            GCMRegistrar.register(context, GCMServerSettings.senderId);
        }
        String regId = GCMRegistrar.getRegistrationId(context).toString();
        Log.d("backpaper2-GCM","GCM REGISTRATION ID: " + regId);
        registerBackground(deviceId, regId, true);
    }

    public static void unregisterNotify(Context context){
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("backpaper2", deviceId);
        Log.d("backpaper2-build",android.os.Build.MANUFACTURER);
        if(GCMRegistrar.getRegistrationId(context).equals("")){
            Log.d("backpaper2-GCM", GCMServerSettings.senderId);
            GCMRegistrar.register(context, GCMServerSettings.senderId);
        }
        String regId = GCMRegistrar.getRegistrationId(context).toString();
        Log.d("backpaper2-GCM","GCM REGISTRATION ID: " + GCMRegistrar.getRegistrationId(context));
        registerBackground(deviceId, regId, false);
    }

    public static void registerBackground(String dev_id,String reg_id,Boolean status) {
        class PostUrlData extends postNotificationData {
            @Override
            public void postResult(Object result2){
                Log.d("backpaper2-gcm-register",result2.toString());
            }
        }
        PostUrlData postData = new PostUrlData();
        postData.getData("http://backpaper.w4rlock.in/api/v1/deviceNotify/"+dev_id+"/",dev_id,reg_id,status);
    }
}
