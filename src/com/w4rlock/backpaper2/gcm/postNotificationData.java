package com.w4rlock.backpaper2.gcm;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

/**
 * Created by krishna on 5/21/13.
 */
public class postNotificationData {
    public static final String DEBUG_TAG = "backpaper2-post-network";
    public String result = null;

    public void getData(String url, String dev_id,String res_id,Boolean status) {
        String status_str;
        if(status){
            status_str = "true";
        }else{
            status_str = "false";
        }
        new DownloadWebPageText().execute(url, dev_id,res_id,status_str);
    }

    public void postResult(Object result2) {
        result = (String) result2;
        Log.d(DEBUG_TAG,result);
    }

    private class DownloadWebPageText extends AsyncTask<Object, Object, Object> {

        @Override
        protected Object doInBackground(Object... params) {
            String url = (String) params[0];
            String dev_id = (String) params[1];
            String res_id = (String)params[2];
            String status_str = (String)params[3];
            try {
                return downloadUrl(url, dev_id,res_id,status_str);
            } catch (IOException e) {
                return "unable to get the data sry :(";
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            Log.d(DEBUG_TAG,"--------");
            Log.d(DEBUG_TAG,result.toString());
            if (result == null || result == " " || result == "") {
                result = "-1";
            } else {
                postResult(result);
            }
        }

        private String downloadUrl(String myUrl, String dev_id,String res_id,String status_str)
                throws IOException {
            //InputStream is = null;
            Log.d(DEBUG_TAG,myUrl);
            Log.d(DEBUG_TAG,dev_id);
            Log.d(DEBUG_TAG,res_id);
            Log.d(DEBUG_TAG,status_str);
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPut httpPut = new HttpPut(myUrl);
                httpPut.setHeader("content-type", "application/json");

                String entity_str = "{\"dev_id\": \""+dev_id+"\", \"is_active\": "+status_str+", \"reg_id\": \""+res_id+"\"}";
                Log.d(DEBUG_TAG,entity_str);
                StringEntity entity = new StringEntity(entity_str);
                httpPut.setEntity(entity);
                HttpResponse response = httpclient.execute(httpPut);

                if(response!=null){

                    InputStream in = response.getEntity().getContent(); //Get the data in the entity

                    String content = readAll(in);
                    return content.toString();
                }else{
                    return null;
                }

            } catch (Exception e) {
                Log.d("backpaper2-post-network","e1 "+e.toString());
                return " ";
            }
        }

        public String readAll(InputStream stream) throws IOException,
                UnsupportedEncodingException {
            StringBuilder sb = new StringBuilder();
            String s;
            try {
                Reader reader = new InputStreamReader(stream, "UTF-8");
                BufferedReader buf = new BufferedReader(reader);
                while (true) {
                    s = buf.readLine();
                    if (s == null || s.length() == 0)
                        break;
                    sb.append(s);
                }
                return sb.toString();
            } catch (Exception e) {
                Log.d("backpaper2-post-network", "error on readall ie catching input stream "+e.toString());
                return " ";
            }
        }
    }

}
