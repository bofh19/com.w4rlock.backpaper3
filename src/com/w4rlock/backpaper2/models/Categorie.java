package com.w4rlock.backpaper2.models;

public class Categorie {
	private String categorie_name;
	private int id;
    private Boolean categorieAdult;
	public String getCategorie_name() {
		return categorie_name;
	}
	public void setCategorie_name(String categorie_name) {
		this.categorie_name = categorie_name;
	}

    public Boolean getCategorieAdult() {
        return categorieAdult;
    }

    public void setCategorieAdult(Boolean categorieAdult) {
        this.categorieAdult = categorieAdult;
    }

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
