package com.w4rlock.backpaper2.models;

/**
 * Created by Chitti on 19/7/13.
 */
public class Comments {
    private String commentId;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentData() {
        return commentData;
    }

    public void setCommentData(String commentData) {
        this.commentData = commentData;
    }

    public String getCommentTstamp() {
        return commentTstamp;
    }

    public void setCommentTstamp(String commentTstamp) {
        this.commentTstamp = commentTstamp;
    }

    public String getCommentUname() {
        return commentUname;
    }

    public void setCommentUname(String commentUname) {
        this.commentUname = commentUname;
    }

    public String getCommentEmail() {
        return commentEmail;
    }

    public void setCommentEmail(String commentEmail) {
        this.commentEmail = commentEmail;
    }

    private String commentData;
    private String commentTstamp;
    private String commentUname;
    private String commentEmail;
}
