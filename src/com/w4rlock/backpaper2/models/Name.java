package com.w4rlock.backpaper2.models;

import java.io.Serializable;
import java.util.Comparator;

public class Name implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2605567980073245833L;
	private String categorie;
	private int id;
	private String name;
	private String img_url;
	private int total_count;
    private int avg_of_ranks;
    private int sum_votes_count;
    private int total_votes_count;
    private int comments_count;
    private String adapter_display_name;
    private int likes_votes_count;

    public String getAdapter_display_name() {
        return adapter_display_name;
    }

    public void setAdapter_display_name(String adapter_display_name) {
        this.adapter_display_name = adapter_display_name;
    }

    public int getLikes_votes_count() {
        return likes_votes_count;
    }

    public void setLikes_votes_count(int likes_votes_count) {
        this.likes_votes_count = likes_votes_count;
    }

    public int getDislikes_votes_count() {
        return dislikes_votes_count;
    }

    public void setDislikes_votes_count(int dislikes_votes_count) {
        this.dislikes_votes_count = dislikes_votes_count;
    }

    private int dislikes_votes_count;

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public int getSum_votes_count() {
        return sum_votes_count;
    }

    public void setSum_votes_count(int sum_votes_count) {
        this.sum_votes_count = sum_votes_count;
    }

    public int getTotal_votes_count() {
        return total_votes_count;
    }

    public void setTotal_votes_count(int total_votes_count) {
        this.total_votes_count = total_votes_count;
    }

    public int getAvg_of_ranks() {
        return avg_of_ranks;
    }

    public void setAvg_of_ranks(int avg_of_ranks) {
        this.avg_of_ranks = avg_of_ranks;
    }

    public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
        this.adapter_display_name = name;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
    public String getCommentUid(){
        try{
            return "A_"+getId();
        }catch(Exception e){
            return "A_U";
        }
    }
	private String resource;


    public static class SortByAvgOfRanks implements Comparator<Name> {
        @Override
        public int compare(Name name, Name name2) {
            Integer n1 = Integer.valueOf(name.getAvg_of_ranks());
            Integer n2 = Integer.valueOf(name2.getAvg_of_ranks());
            return n2.compareTo(n1);
        }
    }

    public static class SortByName implements Comparator<Name>{

        @Override
        public int compare(Name name, Name name2) {
            String s1 = name.getName();
            String s2 = name2.getName();
            return s1.compareTo(s2);
        }
    }

    public static class SortByCommentsCount implements Comparator<Name>{
        @Override
        public int compare(Name name, Name name2) {
            Integer n1 = Integer.valueOf(name.getComments_count());
            Integer n2 = Integer.valueOf(name2.getComments_count());
            return n2.compareTo(n1);
        }
    }

    public static class SortByVotesCount implements Comparator<Name>{
        @Override
        public int compare(Name name, Name name2) {
            Integer n1 = Integer.valueOf(name.getTotal_votes_count());
            Integer n2 = Integer.valueOf(name2.getTotal_votes_count());
            return n2.compareTo(n1);
        }
    }

    public static class SortByLikesCount implements Comparator<Name>{
        @Override
        public int compare(Name name, Name name2) {
            Integer n1 = Integer.valueOf(name.getLikes_votes_count());
            Integer n2 = Integer.valueOf(name2.getLikes_votes_count());
            return n2.compareTo(n1);
        }
    }

    public static class SortByDislikesCount implements Comparator<Name>{
        @Override
        public int compare(Name name, Name name2) {
            Integer n1 = Integer.valueOf(name.getDislikes_votes_count());
            Integer n2 = Integer.valueOf(name2.getDislikes_votes_count());
            return n2.compareTo(n1);
        }
    }
}
