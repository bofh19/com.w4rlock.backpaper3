package com.w4rlock.backpaper2.models;

import java.io.Serializable;

public class NameImage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6160084639067785491L;
	
	private int id;
	private String date;
	private String imagefile;
	private String imagefile_small;
	private String name_uri;
	private String source;
	private String imagefileOriginal;
    private Boolean imageAdult;
    private String resourceUri;
    private int ranksUsed;

    public int getRanksUsed(){
        return ranksUsed;
    }

    public void setRanksUsed(int ranksUsed){
        this.ranksUsed = ranksUsed;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    private String imageName;
    public Boolean getImageAdult() {
        return imageAdult;
    }

    public void setImageAdult(Boolean imageAdult) {
        this.imageAdult = imageAdult;
    }

    public String getImagefileOriginal() {
		return imagefileOriginal;
	}
	public void setImagefileOriginal(String imagefileOriginal) {
		this.imagefileOriginal = imagefileOriginal;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getImagefile() {
		return imagefile;
	}
	public void setImagefile(String imagefile) {
		this.imagefile = imagefile;
	}
	public String getImagefile_small() {
		return imagefile_small;
	}
	public void setImagefile_small(String imagefile_small) {
		this.imagefile_small = imagefile_small;
	}
	public String getName_uri() {
		return name_uri;
	}
	public void setName_uri(String name_uri) {
		this.name_uri = name_uri;
	}
    public String getCommentUid(){
        try{
            return "I_"+getId();
        }catch (Exception e){
            return "I_U";
        }
    }
}
