package com.w4rlock.backpaper2.models;

import java.io.Serializable;
import java.util.ArrayList;

public class NameImages implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7201194952897643446L;
	private int total_count;
	private Name name;

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}
	private ArrayList<NameImage> images;
	private String next;
	public String getNext() {
		return next;
	}
	
	public ArrayList<NameImage> getImages() {
		return images;
	}

	public void setImages(ArrayList<NameImage> images) {
		this.images = images;
	}

	public void setNext(String next) {
		this.next = next;
	}
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
}
