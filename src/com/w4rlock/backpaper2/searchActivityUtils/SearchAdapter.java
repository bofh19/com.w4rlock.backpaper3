package com.w4rlock.backpaper2.searchActivityUtils;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper4.R;

public class SearchAdapter extends ArrayAdapter<Name> {
	Context context;
	int layoutResourceId;
	List<Name> data = null;
	public ImageLoader imageLoader;
	public DisplayImageOptions doptions;
	public SearchAdapter(Context context, int textViewResourceId,List<Name> objects) {
		super(context, textViewResourceId, objects);
		this.layoutResourceId = textViewResourceId;
		this.context = context;
		this.data = objects;
//		imageLoader = new ImageLoader(context);
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
		
		doptions = new DisplayImageOptions.Builder()
					.cacheOnDisc()
					.showStubImage(R.drawable.ic_stub)
					.showImageForEmptyUri(R.drawable.ic_empty)
					.showImageOnFail(R.drawable.ic_error)
					.cacheInMemory()
					.cacheOnDisc()
					.displayer(new RoundedBitmapDisplayer(10))
					.build();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ResultHolder holder = null;
		if(row == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
			
			row = inflater.inflate(layoutResourceId, parent,false);
			
			holder = new ResultHolder();
			holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
			holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
			holder.desc = (TextView)row.findViewById(R.id.result_desc);
			row.setTag(holder);
			
		}else{
			holder = (ResultHolder)row.getTag();
		}
		Name name = data.get(position);
		holder.txtTitle.setText(name.getName());
		holder.desc.setText("");
		try{
			String url = name.getImg_url();
//			imageLoader.DisplayImage(url, holder.imgIcon);
			imageLoader.displayImage(url, holder.imgIcon,doptions);
		}catch(Exception e){
			Log.d("backpaper2","error while displaying image "+e.toString());
		}
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
        row.startAnimation(animation);
		return row;
	}
	
	static class ResultHolder
	{
		ImageView imgIcon;
		TextView txtTitle;
		TextView desc;
	}
}
