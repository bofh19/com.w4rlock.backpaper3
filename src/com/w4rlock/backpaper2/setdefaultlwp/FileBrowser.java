package com.w4rlock.backpaper2.setdefaultlwp;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper4.R;

/**
 * Created by saikrishna on 9/20/13.
 */
public class FileBrowser extends Activity {
    public static final String DEBUG_TAG = "backpaper2-file-browser";
    private ArrayList<FileModel> fileObjects = new ArrayList<FileModel>();
    private FileListDisplayAdapter fileListDisplayAdapter;
    private ListView filesListView;
    private TextView currentPath;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_browser);
        fileListDisplayAdapter = new FileListDisplayAdapter(this.getApplicationContext(),R.layout.resource_file_browser_row,fileObjects);
        filesListView = (ListView)findViewById(R.id.files_list);
        filesListView.setAdapter(fileListDisplayAdapter);
        currentPath = (TextView)findViewById(R.id.current_file_path);
        initialPath();
        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    FileModel cFileModel = (FileModel)adapterView.getAdapter().getItem(i);
                    if(cFileModel.getFolder()){
                        Log.d(DEBUG_TAG,"abs path "+cFileModel.getAbsPath());
                        startFileDisplayActivity(cFileModel.getAbsPath());
                    }else if(cFileModel.getMimeType().toLowerCase().contains("image")){
                        SharedPreferences settings = getApplicationContext().getSharedPreferences(IntentConfigVariables.lwpDefaultLocationPref, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(IntentConfigVariables.getLwpDefaultLocationValue,cFileModel.getAbsPath());
                        editor.commit();
                        Toast.makeText(getApplicationContext(), "Default Image Changed to "+cFileModel.getName(), Toast.LENGTH_LONG).show();
                        finish();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Not a Folder or Image", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.d(DEBUG_TAG,"exception on item click listener 1 "+e.toString());
                }
            }
        });
    }

    private void initialPath(){
        fileObjects.clear();
        if(isSdPresent()){
            FileModel externalCard = new FileModel();
            externalCard.setName("External SD Card");
            externalCard.setDesc("External SD Card");
            externalCard.setFolder(true);
            File extPath = Environment.getExternalStorageDirectory();
            externalCard.setAbsPath(extPath.getAbsolutePath());
            fileObjects.add(externalCard);
        }
        FileModel internalCard = new FileModel();
        internalCard.setName("Internal SD Card");
        internalCard.setDesc("Internal SD Card");
        internalCard.setFolder(true);
        File intPath = Environment.getDataDirectory();
        internalCard.setAbsPath(intPath.getAbsolutePath());
        fileObjects.add(internalCard);

        FileModel rootFile = new FileModel();
        rootFile.setName("Root");
        rootFile.setDesc("Root");
        rootFile.setAbsPath("/storage");
        rootFile.setFolder(true);
        fileObjects.add(rootFile);
        fileListDisplayAdapter.notifyDataSetChanged();
    }

    private void startFileDisplayActivity(String path) {
        if(path == null){
            return;
        }
        fileObjects.clear();
        if(path != null){
            Log.d(DEBUG_TAG,"given path "+path);
            currentPath.setText(path);
        }
        if(!(new File(path).exists())){
            return;
        }
        else{
            try{
                File gPath = new File(path);
                FileModel parentFile = new FileModel();
                parentFile.setName("Back");
                parentFile.setDesc("back to " + gPath.getParentFile().getName());
                parentFile.setFolder(true);
                parentFile.setAbsPath(gPath.getParentFile().getAbsolutePath());
                fileObjects.add(parentFile);
                File[] allDirectorFiles = gPath.listFiles();
                Arrays.sort(allDirectorFiles, new Comparator<File>() {
                    public int compare(File f1, File f2) {
                        return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
                    }
                });
                for(File f:allDirectorFiles){
                   // Log.d(DEBUG_TAG,"file path "+f.getAbsolutePath());
                    FileModel cFileModel = new FileModel();
                    cFileModel.setName(f.getName());
                    String type = getMimeType(f.getAbsolutePath());
                    cFileModel.setDesc(type);
                    cFileModel.setMimeType(type);
                    cFileModel.setFolder(f.isDirectory());
                    cFileModel.setAbsPath(f.getAbsolutePath());
                    fileObjects.add(cFileModel);
                }
            }catch (Exception e){
                Log.d(DEBUG_TAG,"Error while getting processing filelist "+path.toString());
                currentPath.setText("Error Cant Index \""+path+"\" location, probably no permission");
                initialPath();
                return;
            }
        }
        fileListDisplayAdapter.notifyDataSetChanged();
    }

    public static boolean isSdPresent() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }
    public static String getMimeType(String url)
    {
        final String[] audioExt = {"mp3","mp4","ogg","wav","wmv"};
        final String[] imageExt = {"jpg","jpeg","png","gif"};
        final String[] docExt = {"pdf","doc","docx","xls","txt"};
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        if(type == null){
            try{
                String exten = url.substring(url.lastIndexOf('.')+1);
                for(String str:audioExt){
                    if(exten.toLowerCase().contains(str)){
                        return "audio/"+exten;
                    }
                }
                for(String str:imageExt){
                    if(exten.toLowerCase().contains(str)){
                        return "image/"+exten;
                    }
                }
                for(String str:docExt){
                    if(exten.toLowerCase().contains(str)){
                        return "text/"+exten;
                    }
                }
            }catch(Exception e){

            }
        }
        return type;
    }
}
