package com.w4rlock.backpaper2.setdefaultlwp;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.w4rlock.backpaper4.R;

/**
 * Created by saikrishna on 9/20/13.
 */
public class FileListDisplayAdapter extends ArrayAdapter<FileModel> {
    Context context;
    int layoutResourceId;
    ArrayList<FileModel> fileObjects;

    public ImageLoader imageLoader;
    public DisplayImageOptions doptions;
    public static final String DEBUG_TAG = "backpaper2-file-browser-adapter";
    public FileListDisplayAdapter(Context context, int layoutResourceId, ArrayList<FileModel> fileObjects) {
        super(context, layoutResourceId, fileObjects);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.fileObjects = fileObjects;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        doptions = new DisplayImageOptions.Builder()
                .cacheOnDisc()
                .showStubImage(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory()
                .cacheOnDisc()
                .displayer(new RoundedBitmapDisplayer(10))
                .build();
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        View row = convertView;
        FileViewHolder holder;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layoutResourceId,parent,false);
            holder = new FileViewHolder();
            holder.icon = (ImageView)row.findViewById(R.id.icon);
            holder.fnameTxt = (TextView)row.findViewById(R.id.f_name_main);
            holder.fdescTxt = (TextView)row.findViewById(R.id.f_name_desc);
            row.setTag(holder);
        }else{
            holder = (FileViewHolder)row.getTag();
        }
        FileModel currentFile = fileObjects.get(position);
        Log.d(DEBUG_TAG,currentFile.getName());
        holder.fnameTxt.setText(currentFile.getName());
        holder.fdescTxt.setText(currentFile.getDesc());
        try{
            String type = currentFile.getMimeType();
            if(type.contains("image")){
                String location = "file://"+currentFile.getAbsPath();
                Log.d(DEBUG_TAG,"image type found icon : "+location);
                imageLoader.displayImage(location,holder.icon);
            }else if(type.contains("text")){
                imageLoader.displayImage("drawable://"+R.drawable.notes_icon,holder.icon);
            }else if(type.contains("audio")){
                imageLoader.displayImage("drawable://"+R.drawable.itunes_icon,holder.icon);
            }else if(type.contains("application")){
                imageLoader.displayImage("drawable://"+R.drawable.folder_apps_icon,holder.icon);
            }else{
                imageLoader.displayImage("drawable://"+R.drawable.ic_stub,holder.icon);
            }
        }catch(Exception e){
            //Log.d(DEBUG_TAG,"no type");
            imageLoader.displayImage("drawable://"+R.drawable.ic_stub,holder.icon);
        }
        if(currentFile.getFolder()){
            imageLoader.displayImage("drawable://"+R.drawable.folder_icon_1,holder.icon);
        }
        Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
        row.startAnimation(animation);
        return row;
    }

    static class FileViewHolder{
        ImageView icon;
        TextView fnameTxt;
        TextView fdescTxt;
    }
}
