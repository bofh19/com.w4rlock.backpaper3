package com.w4rlock.backpaper2.setdefaultlwp;

/**
 * Created by saikrishna on 9/20/13.
 */
public class FileModel {
    private Boolean folder;
    private String name;
    private String absPath;
    private String Desc;
    private String mimeType;

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Boolean getFolder() {
        return folder;
    }
    public void setFolder(Boolean folder) {
        this.folder = folder;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbsPath() {
        return absPath;
    }

    public void setAbsPath(String absPath) {
        this.absPath = absPath;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }
}
