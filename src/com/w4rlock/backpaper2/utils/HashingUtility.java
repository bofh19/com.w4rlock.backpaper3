package com.w4rlock.backpaper2.utils;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by krishna on 17/7/13.
 */
public class HashingUtility {
    public static String HMAC_MD5_encode(String key, String message) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(
                key.getBytes(),
                "HmacMD5");
        Mac mac = Mac.getInstance("HmacMD5");
        mac.init(keySpec);
        byte[] rawHmac = mac.doFinal(message.getBytes());
        char[] chars = Hex.encodeHex(rawHmac);
        String return_string = "";
        for(char each_char:chars){
            return_string += String.valueOf(each_char);
        }
        return return_string;
    }
}
