package com.w4rlock.backpaper2.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkChecker {
	public static boolean getNetStatus(Context context){
		
		ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if(networkInfo != null && networkInfo.isConnected()){
      		Log.d("network", "loading data");
    		try{
    			Log.d("network","connecting");
    			return true;
    		}catch(Exception e){
    			return false;
    		}
    	}else {
    		return false;
    	}
	}
}
