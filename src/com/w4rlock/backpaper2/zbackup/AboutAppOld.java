package com.w4rlock.backpaper2.zbackup;

/**
 * Created by krishna on 6/3/13.
 */
public class AboutAppOld {
}



//public class AboutApp extends Activity {
//    CacheManager cm ;
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_about_app);
//        if (android.os.Build.VERSION.SDK_INT >= 13) {
//            try {
//                getActionBar().setDisplayHomeAsUpEnabled(true);
//            }catch (NullPointerException e){
//                Log.d("backpaper2","unable to set nav home up");
//            }
//        }
//        cm = new CacheManager(this.getApplicationContext());
//        setAppVersions();
//        setLWPData();
//        setCacheData();
//        executeMemory();
//    }
//
//    private void executeMemory() {
////
////        http://stackoverflow.com/questions/2298208/how-to-discover-memory-usage-of-my-application-in-android/2299813#2299813
////
//
//        final TextView textView5 = (TextView) findViewById(R.id.textView5);
//        final ProgressBar progressBar5 = (ProgressBar)findViewById(R.id.progressBar5);
//
//        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
//        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
//        activityManager.getMemoryInfo(memoryInfo);
//        String TAG = "backpaper2-memory";
//        Log.i(TAG, " memoryInfo.availMem " + memoryInfo.availMem + "\n" );
//        Log.i(TAG, " memoryInfo.lowMemory " + memoryInfo.lowMemory + "\n" );
//        Log.i(TAG, " memoryInfo.threshold " + memoryInfo.threshold + "\n" );
//
//        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
//
//        Map<Integer, String> pidMap = new TreeMap<Integer, String>();
//        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses)
//        {
//            if(runningAppProcessInfo.processName.equals("com.w4rlock.backpaper2")){
//                pidMap.put(runningAppProcessInfo.pid, runningAppProcessInfo.processName);
//            }
//        }
//
//        Collection<Integer> keys = pidMap.keySet();
//
//        for(int key : keys)
//        {
//            int pids[] = new int[1];
//            pids[0] = key;
//            android.os.Debug.MemoryInfo[] memoryInfoArray = activityManager.getProcessMemoryInfo(pids);
//            for(android.os.Debug.MemoryInfo pidMemoryInfo: memoryInfoArray)
//            {
////                Log.i(TAG, String.format("** MEMINFO in pid %d [%s] **\n",pids[0],pidMap.get(pids[0])));
////                Log.i(TAG, " pidMemoryInfo.getTotalPrivateDirty(): " + pidMemoryInfo.getTotalPrivateDirty() + "\n");
////                Log.i(TAG, " pidMemoryInfo.getTotalPss(): " + pidMemoryInfo.getTotalPss() + "\n");
////                Log.i(TAG, " pidMemoryInfo.getTotalSharedDirty(): " + pidMemoryInfo.getTotalSharedDirty() + "\n");
//                textView5.setText("** MEMINFO in pid "+pids[0]+"\n"+pidMap.get(pids[0])+" ** (kB)\n");
//                textView5.append("total private dirty memory usage - "+pidMemoryInfo.getTotalPrivateDirty() + "\n");
//                textView5.append("total PSS memory usage in - "+pidMemoryInfo.getTotalPss() + "\n");
//                textView5.append("total shared dirty memory usage - "+pidMemoryInfo.getTotalSharedDirty()+"\n");
//            }
//        }
//        progressBar5.setVisibility(View.GONE);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                finish();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    private void setLWPData(){
//        final TextView textView2 = (TextView) findViewById(R.id.textView2);
//        final ProgressBar progressBar2 = (ProgressBar)findViewById(R.id.progressBar2);
//        Set<String> imgs_locs;
//        SharedPreferences settings = getSharedPreferences(IntentConfigVariables.LWP_IMGS_LOCATION_PREF,0);
//        textView2.setText("No of Images In LWP Lists");
//        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_1,new TreeSet<String>()));
//        textView2.append("\nList 1 : "+imgs_locs.size());
//        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_2,new TreeSet<String>()));
//        textView2.append("\nList 2 : "+imgs_locs.size());
//        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_3,new TreeSet<String>()));
//        textView2.append("\nList 3 : "+imgs_locs.size());
//        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_4,new TreeSet<String>()));
//        textView2.append("\nList 4 : "+imgs_locs.size());
//        progressBar2.setVisibility(View.GONE);
//    }
//    private void setCacheData(){
//        final TextView textView3 = (TextView) findViewById(R.id.textView3);
//        final ProgressBar progressBar3 = (ProgressBar)findViewById(R.id.progressBar3);
//        textView3.setText("Cache Size : " + String.valueOf(cm.getSize() / 1000) + " KB");
//        progressBar3.setVisibility(View.GONE);
//        Button clearCache = (Button) findViewById(R.id.button_clear_cache);
//        clearCache.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                cm.clear();
//                setCacheData();
//            }
//        });
//    }
//    private void setAppVersions(){
//        final TextView textView1 = (TextView) findViewById(R.id.textView);
//        final TextView textView4 = (TextView) findViewById(R.id.textView4);
//        final ProgressBar progressBar4 = (ProgressBar)findViewById(R.id.progressBar4);
//        final ProgressBar progressBar1 = (ProgressBar)findViewById(R.id.progressBar);
//        textView4.setText("Additional Server Data ");
//        String versionName;
//        try {
//            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
//        } catch (PackageManager.NameNotFoundException e) {
//            versionName = "Unable to fetch Version";
//            Log.d("backpaper2", "unable to get version " + e.toString());
//        }
//        textView1.setText("App Version : "+versionName);
//
//        class getData extends getURLdata{
//            @Override
//            public void postresult(Object result2){
//                String res = (String)result2;
//                try {
//                    JSONObject main_obj = new JSONObject(res);
//                    JSONArray main_obj_array = main_obj.getJSONArray("objects");
//                    for (int i = 0; i < main_obj_array.length(); i++) {
//                        JSONObject this_obj = main_obj_array.getJSONObject(i);
//                        textView1.append("\nCategories Count: "+this_obj.getString("categories_count"));
//                        textView1.append("\nNames Count: "+this_obj.getString("names_count"));
//                        textView1.append("\nImages Count: "+this_obj.getString("images_count"));
//                        Iterator<?> keys = this_obj.keys();
//                        while(keys.hasNext()){
//                            String key = (String)keys.next();
//                            if(!(key.equals("categories_count") || key.equals("names_count") || key.equals("images_count") || key.equals("resource_uri") || key.equals("uuid")  )){
//                                textView4.append("\n"+key+" : "+this_obj.getString(key));
//                            }
//                        }
//                    }
//                }catch(Exception e){
//                    dataError();
//                }
//
//                hideBars();
//            }
//
//            @Override
//            public void onTimeOut(Object result2){
//                Log.d("backpaper2-network","TIME - OUT ");
//                dataError();
//            }
//            @Override
//            public void onError(Object result2){
//                Log.d("backpaper2-network","Error");
//                dataError();
//            }
//            public void hideBars(){
//                progressBar1.setVisibility(View.GONE);
//                progressBar4.setVisibility(View.GONE);
//            }
//            public void dataError(){
//                textView1.append("\nCategories Count: "+"unable to get data");
//                textView1.append("\nCategories Count: "+"unable to get data");
//                textView1.append("\nCategories Count: "+"unable to get data");
//                textView4.append("Unable to get Data");
//                hideBars();
//            }
//        }
//        getData showVersions = new getData();
//        showVersions.getData(ServerVariables.getInfoUrl());
//    }
//}