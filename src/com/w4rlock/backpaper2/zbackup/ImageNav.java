//package com.w4rlock.backpaper;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.View.OnTouchListener;
//import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.GridImageAdapter2;
//import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.parseResultNameImages;
//import com.w4rlock.backpaper2.configs.GetDisplayParams;
//import com.w4rlock.backpaper2.configs.IntentConfigVariables;
//import com.w4rlock.backpaper2.configs.ServerVariables;
//import com.w4rlock.backpaper2.models.Name;
//import com.w4rlock.backpaper2.models.NameImage;
//import com.w4rlock.backpaper2.models.NameImages;
//import com.w4rlock.backpaper2.utils.ImageLoader;
//import com.w4rlock.cacheManager.UrlCacheManager;
//import com.widget.HorizontalScrollView;
//import com.widget.ImageView;
//import com.widget.LinearLayout;
//import com.widget.ListView;
//
//public class ImageNav extends Activity {
//	public static final ServerVariables server_variable = new ServerVariables();
//	ImageLoader imageLoader;
////	private NameImage given_name_image = null;
//	private LinearLayout myGallery;
//	private Name given_name = null;
//	private ListView listview;
//	private GridImageAdapter2 adapter;
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_image_nav);
//		Intent intent = getIntent();
//		given_name = (Name)intent.getSerializableExtra(IntentConfigVariables.getNameImgNavMessageName());
//		imageLoader = new ImageLoader(this.getApplicationContext());
//		myGallery = (LinearLayout) findViewById(R.id.mygallery);
//		
//		listview = (ListView)findViewById(R.id.activity_image_nav_listview1);
//		
//		displayPhotos(given_name);
//		final HorizontalScrollView horizontal_scroll_view = (HorizontalScrollView)findViewById(R.id.horizontal_scroll_view);
//		myGallery.setOnTouchListener( new OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View view, MotionEvent arg1) {
//				 int scrollX = horizontal_scroll_view.getScrollX();
//			        int scrollY = horizontal_scroll_view.getScrollY();
//			        System.out.println("scrollll");
//System.out.println(scrollX);
//System.out.println(scrollY);
//				return false;
//			}
//		});
//		
//	}
//	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_image_nav, menu);
//		return true;
//	}
//	
//	private void displayPhotos(Name given_name) {
//		class getPhotoData extends UrlCacheManager{
//			public getPhotoData(Context context) {
//				super(context);
//			}
//			@Override
//			public void postresult(Object result2){
//				parseResultNameImages presult = new parseResultNameImages();
//				try {
//					NameImages name_images = presult.getObjectsArray(result2);
//					displayImages(name_images);
//					goAdapterTest(name_images);
//				} catch (Exception e) {
//					Log.d("backpaper2","1"+e.toString());
//				}
//			}
//		}
//		getPhotoData getphotodata = new getPhotoData(this.getApplicationContext());
//		getphotodata.getData(server_variable.getNameAllImageUrlWithParams((given_name.getId()), GetDisplayParams.getDisplayAsQueryParams(this)));
//	}
//	
//	private void displayImages(NameImages name_images) {
//		for (NameImage this_name_image : name_images.getImages()){
//			myGallery.addView(getPhoto(this_name_image.getImagefile_small()));
//		}
//	}
//	
//	private View getPhoto(String img_url) {
//		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View layout = inflater.inflate(R.layout.resource_back_nav_image_1, null);
//		ImageView imageView = (ImageView)layout.findViewById(R.id.resource_nav_image_1_image_view);
//		imageLoader.DisplayImage(img_url,imageView);
//		return layout;
//	}
//	
//	private void goAdapterTest(NameImages name_images){
//		System.out.println("test");
//		adapter = new GridImageAdapter2(this, R.layout.resource_image_nav_adapter_1, name_images.getImages());
//		System.out.println("test");
//		listview.setAdapter(adapter);
//		System.out.println("test");
//	}
//}
