package com.w4rlock.backpaper4;



/**
 * Created by krishna on 5/28/13.
 */

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.MenuItem;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.utils.AppRater;
import com.w4rlock.cacheManager.CacheManager;
import com.w4rlock.cacheManager.getURLdata;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

@SuppressLint("ValidFragment")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AboutApp extends PreferenceActivity {
    private Context context;
    PreferenceScreen root;
    CacheManager cm ;
    private Context con;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
        context = getApplicationContext();
        con = this;
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        cm = new CacheManager(this.getApplicationContext());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class MyPreferenceFragment extends PreferenceFragment{
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.about_app);
            root = (PreferenceScreen) findPreference(getResources().getString(R.string.aboutAppPreferenceScreen));
            AddRateDialog();
            setAppVersions();
            setCacheData();
            setLWPData();
            executeMemory();
            getListView().setBackgroundColor(Color.rgb(4, 26, 55));
        }


    }

    private void AddRateDialog() {
        final PreferenceCategory main_pref6 = new PreferenceCategory(context);
        main_pref6.setTitle("Rate Us");
        root.addPreference(main_pref6);

        final Preference ratePref = new Preference(context);
        ratePref.setTitle("Rate App Now");
        ratePref.setSummary("If you like using the app please rate us now");
        ratePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                SharedPreferences prefs = context.getSharedPreferences("rate_app", 0);
                SharedPreferences.Editor editor = prefs.edit();
                AppRater.showRateDialog(con,editor);
                return true;
            }
        });
        main_pref6.addPreference(ratePref);

    }

    private void executeMemory() {

//
//        http://stackoverflow.com/questions/2298208/how-to-discover-memory-usage-of-my-application-in-android/2299813#2299813
//
        final PreferenceCategory main_pref5 = new PreferenceCategory(context);
        main_pref5.setTitle("App Memory Usage");
        root.addPreference(main_pref5);

        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        String TAG = "backpaper2-memory";
        Log.i(TAG, " memoryInfo.availMem " + memoryInfo.availMem + "\n" );
        Log.i(TAG, " memoryInfo.lowMemory " + memoryInfo.lowMemory + "\n" );
        Log.i(TAG, " memoryInfo.threshold " + memoryInfo.threshold + "\n" );

        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();

        Map<Integer, String> pidMap = new TreeMap<Integer, String>();
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses)
        {
            if(runningAppProcessInfo.processName.equals("com.w4rlock.backpaper4")){
                pidMap.put(runningAppProcessInfo.pid, runningAppProcessInfo.processName);
            }
        }

        Collection<Integer> keys = pidMap.keySet();

        for(int key : keys)
        {
            int pids[] = new int[1];
            pids[0] = key;
            android.os.Debug.MemoryInfo[] memoryInfoArray = activityManager.getProcessMemoryInfo(pids);
            for(android.os.Debug.MemoryInfo pidMemoryInfo: memoryInfoArray)
            {
                Preference memoryPref1 = new Preference(context);
                memoryPref1.setTitle("** MEMINFO in pid  "+pids[0] +"** (kB)");
                memoryPref1.setSummary(pidMap.get(pids[0]));
                main_pref5.addPreference(memoryPref1);

                Preference memoryPref2 = new Preference(context);
                memoryPref2.setTitle("Total Private Dirty Memory Usage");
                memoryPref2.setSummary(String.valueOf(pidMemoryInfo.getTotalPrivateDirty()));
                main_pref5.addPreference(memoryPref2);

                Preference memoryPref3 = new Preference(context);
                memoryPref3.setTitle("Total PSS Memory Usage");
                memoryPref3.setSummary(String.valueOf(pidMemoryInfo.getTotalPss()));
                main_pref5.addPreference(memoryPref3);

                Preference memoryPref4 = new Preference(context);
                memoryPref4.setTitle("Total Shared Dirty Memory Usage");
                memoryPref4.setSummary(String.valueOf(pidMemoryInfo.getTotalSharedDirty()));
                main_pref5.addPreference(memoryPref4);
            }
        }
    }

    private void setCacheData() {
        final PreferenceCategory main_pref4 = new PreferenceCategory(context);
        main_pref4.setTitle("Cache Data");
        root.addPreference(main_pref4);

        final Preference cachePref1 = new Preference(context);
        cachePref1.setTitle("Cache Size");
        cachePref1.setSummary(String.valueOf(cm.getSize() / 1000) + " KB");
        main_pref4.addPreference(cachePref1);

        Preference cachePref2 = new Preference(context);
        cachePref2.setTitle("Clear Cache");
        cachePref2.setSummary("Clear All Cache Data");
        cachePref2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                cm.clear();
                cachePref1.setSummary(String.valueOf(cm.getSize() / 1000) + " KB");
                return true;
            }
        });
        main_pref4.addPreference(cachePref2);
    }

    private void setLWPData() {
        final PreferenceCategory main_pref3 = new PreferenceCategory(context);
        main_pref3.setTitle("Live Wallpaper Data");
        root.addPreference(main_pref3);

        Set<String> imgs_locs;
        SharedPreferences settings = getSharedPreferences(IntentConfigVariables.LWP_IMGS_LOCATION_PREF,0);

        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_1,new TreeSet<String>()));
        Preference LWPPref1 = new Preference(context);
        LWPPref1.setTitle("List 1");
        LWPPref1.setSummary(String.valueOf(imgs_locs.size())+" images");
        main_pref3.addPreference(LWPPref1);

        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_2,new TreeSet<String>()));
        Preference LWPPref2 = new Preference(context);
        LWPPref2.setTitle("List 2");
        LWPPref2.setSummary(String.valueOf(imgs_locs.size())+" images");
        main_pref3.addPreference(LWPPref2);

        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_3,new TreeSet<String>()));
        Preference LWPPref3 = new Preference(context);
        LWPPref3.setTitle("List 3");
        LWPPref3.setSummary(String.valueOf(imgs_locs.size())+" images");
        main_pref3.addPreference(LWPPref3);

        imgs_locs = new TreeSet<String>(settings.getStringSet(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_4,new TreeSet<String>()));
        Preference LWPPref4 = new Preference(context);
        LWPPref4.setTitle("List 4");
        LWPPref4.setSummary(String.valueOf(imgs_locs.size())+" images");
        main_pref3.addPreference(LWPPref4);
    }

    private void setAppVersions() {
        final PreferenceCategory main_pref2 = new PreferenceCategory(context);
        final PreferenceCategory main_pref = new PreferenceCategory(context);
        main_pref2.setTitle("About App");
        root.addPreference(main_pref2);
        main_pref.setTitle("Server Data");
        root.addPreference(main_pref);
        Preference appVersion = new Preference(context);
        String versionName;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = "Unable to fetch Version";
            Log.d("backpaper2", "unable to get version " + e.toString());
        }
        appVersion.setTitle("App Version");
        appVersion.setSummary(versionName);
        try {
            main_pref2.addPreference(appVersion);
        }catch (Exception e){
            Log.d("backpaper2-error",e.toString());
        }

        class getData extends getURLdata {
            @Override
            public void postresult(Object result2){
                String res = (String)result2;
                try {
                    JSONObject main_obj = new JSONObject(res);
                    JSONArray main_obj_array = main_obj.getJSONArray("objects");
                    for (int i = 0; i < main_obj_array.length(); i++) {
                        JSONObject this_obj = main_obj_array.getJSONObject(i);
                        Preference serverPref1 = new Preference(context);
                        serverPref1.setTitle("Categories Count");
                        serverPref1.setSummary(this_obj.getString("categories_count"));
                        main_pref.addPreference(serverPref1);

                        Preference serverPref2 = new Preference(context);
                        serverPref2.setTitle("Names Count");
                        serverPref2.setSummary(this_obj.getString("names_count"));
                        main_pref.addPreference(serverPref2);

                        Preference serverPref3 = new Preference(context);
                        serverPref3.setTitle("Images Count");
                        serverPref3.setSummary(this_obj.getString("images_count"));
                        main_pref.addPreference(serverPref3);

                        Iterator<?> keys = this_obj.keys();
                        while(keys.hasNext()){
                            String key = (String)keys.next();
                            if(!(key.equals("categories_count") || key.equals("names_count") || key.equals("images_count") || key.equals("resource_uri") || key.equals("uuid")  )){
                                Preference serverPref4 = new Preference(context);
                                serverPref4.setTitle(key);
                                serverPref4.setSummary(this_obj.getString(key));
                                main_pref.addPreference(serverPref4);
                            }
                        }
                    }
                }catch(Exception e){
                    dataError();
                }
            }

            @Override
            public void onTimeOut(Object result2){
                Log.d("backpaper2-network","TIME - OUT ");
                dataError();
            }
            @Override
            public void onError(Object result2){
                Log.d("backpaper2-network","Error");
                dataError();
            }
            public void dataError(){
                Preference serverPref4 = new Preference(context);
                serverPref4.setTitle("Server Info");
                serverPref4.setSummary("Unable To Get Data");
                main_pref.addPreference(serverPref4);
            }
        }
        getData showVersions = new getData();
        showVersions.getData(ServerVariables.getInfoUrl());
    }
}
