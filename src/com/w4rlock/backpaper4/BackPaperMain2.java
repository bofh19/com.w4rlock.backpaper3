package com.w4rlock.backpaper4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import com.searchboxsdk.android.StartAppSearch;
import com.startapp.android.publish.StartAppAd;
import com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils.CategoriesParser;
import com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils.GridImageAdapter;
import com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils.SlideMenuAdapter;
import com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils.parseResultNames;
import com.w4rlock.backpaper2.Logger.GTracker;
import com.w4rlock.backpaper2.configs.GetDisplayParams;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.gcm.GCMRegisterOnServer;
import com.w4rlock.backpaper2.models.Categorie;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.utils.AppRater;
import com.w4rlock.backpaper2.utils.Connectivity;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.cacheManager.CacheManager;
import com.w4rlock.cacheManager.getURLdata;

//import com.apperhand.device.android.AndroidSDKProvider;
//import com.startapp.android.publish.StartAppAd;


@SuppressLint("NewApi")
public class BackPaperMain2 extends ActionBarActivity implements ActionBar.OnNavigationListener{
    public static final ServerVariables server_variable = new ServerVariables();
    private GridImageAdapter adapter;
    private GridView gridView;
    public CacheManager cm;
    public String url;
    private Toast mToast = null;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    ArrayList<Categorie> categories_list = new ArrayList<Categorie>();
    private Context context;

    private GoogleAnalytics gaInstance;
    private Tracker gaTracker;

    Name[] name_list_array = {};

    private boolean auto_updated = false;

//    private StartAppAd startAppAd = null; //startapp

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back_paper_main);
        context = this.getApplicationContext();
        cm = new CacheManager(this);
        gaInstance = GoogleAnalytics.getInstance(this);
        gaTracker = gaInstance.getTracker(getResources().getString(R.string.ga_trackingId));
        auto_updated = false;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mTitle = mDrawerTitle = getTitle();
        setDrawerItems();
        GCMRegisterOnServer.register(this.context);
        Log.d("backpaper2-build-version",String.valueOf(android.os.Build.VERSION.SDK_INT));

        if (android.os.Build.VERSION.SDK_INT >= 13) {
            try {
                getActionBar().setDisplayHomeAsUpEnabled(true);
                getActionBar().setHomeButtonEnabled(true);
            }catch (NullPointerException e){
                Log.d("backpaper2","unable to set nav home up");
            }
        }
//        AndroidSDKProvider.setTestMode(true);//startapp
//        AndroidSDKProvider.initSDK(this); //startapp
//        if(startAppAd != null) { //startapp
//            startAppAd.show();//startapp
//            startAppAd = new StartAppAd(this); startAppAd.load();//startapp
//        }//startapp
        AppRater.app_launched(this);
        declareAddsStatus();
        displayAdds();

        if(Build.VERSION.SDK_INT >= 14){
            final ActionBar actionBar = getActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            actionBar.setListNavigationCallbacks(new ArrayAdapter<String>(actionBar.getThemedContext(),
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                    new String[]{
                            "Sort by Clicks",
                            "Sort by Name",
                            "Sort by Comments",
                            "Sort by Total Votes",
                            "Sort by Likes Count",
                            "Sort by Dislikes Count"
                            // getString(R.string.main_nav_menu_file_browser),
                            //getString(R.string.main_nav_menu_list_of_images),
                    }), this);
        }

    }

    @Override
    public boolean onNavigationItemSelected(int position, long id) {
        if(name_list_array.length == 0)
            return true;
        ArrayList<Name> name_list = new ArrayList<Name>(Arrays.asList(name_list_array));
        goAdapter(name_list);
        return true;
    }

    private ArrayList<Name> getSortedList(int position,ArrayList<Name> names_array){

        if(position == 1){
            Collections.sort(names_array,new Name.SortByName());
            for(Name current_name:names_array){
                current_name.setAdapter_display_name(current_name.getName());
            }
        }else if(position == 0){
            Collections.sort(names_array,new Name.SortByAvgOfRanks());
            for(Name current_name:names_array){
                current_name.setAdapter_display_name(current_name.getName()+" ("+current_name.getAvg_of_ranks()+")");
            }
        }else if(position == 2){
            Collections.sort(names_array,new Name.SortByCommentsCount());
            for(Name current_name:names_array){
                current_name.setAdapter_display_name(current_name.getName()+" ("+current_name.getComments_count()+")");
            }
        }else if(position == 3){
            Collections.sort(names_array,new Name.SortByVotesCount());
            for(Name current_name:names_array){
                current_name.setAdapter_display_name(current_name.getName()+" ("+current_name.getTotal_votes_count()+")");
            }
        }else if(position == 4){
            Collections.sort(names_array,new Name.SortByLikesCount());
            for(Name current_name:names_array){
                current_name.setAdapter_display_name(current_name.getName()+" ("+current_name.getLikes_votes_count()+")");
            }
        }else if(position == 5){
            Collections.sort(names_array,new Name.SortByDislikesCount());
            for(Name current_name:names_array){
                current_name.setAdapter_display_name(current_name.getName()+" ("+current_name.getDislikes_votes_count()+")");
            }
        }
        else {
            return names_array;
        }
        Log.d("backpaper2","sorted on position "+position);
        return names_array;
    }
    private ArrayList<Name> getSortedList(int position,Name[] names_array){
        ArrayList<Name> name_list = new ArrayList<Name>();
        try{
            name_list = new ArrayList<Name>(Arrays.asList(names_array));
            if(name_list.size() == 0)
                return name_list;
            else{
                return getSortedList(position,name_list);
            }
        }catch (Exception e){
            Log.d("backpaper2","wtf is happening "+e);
            return name_list;
        }
    }

    private void setDrawerItems() {
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            public void onDrawerClosed(View view) {
                if (android.os.Build.VERSION.SDK_INT >= 13) {
                    getActionBar().setTitle(mTitle);
                }
            }

            public void onDrawerOpened(View drawerView) {
                if (android.os.Build.VERSION.SDK_INT >= 13) {
                    getActionBar().setTitle(mDrawerTitle);
                }
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        String cat_url = server_variable.getCategoriesUrl();
        if(cm.checkFileFromUrl(cat_url)){
            displayToast("Displaying Cached Data. Click Refresh in Menu Button To Load Latest Data");
            goOnFileDrawer(cat_url);
        }else{
            goOnNetworkDrawer(cat_url);
        }
//        mDrawerList.setAdapter(new ArrayAdapter<String>(this,R.layout.drawer_list_item, mPlanetTitles));
//        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
    }

    private boolean goOnNetworkDrawer(final String cat_url) {
        displayLoaders(true);
        if(!NetworkChecker.getNetStatus(this)){
            this.displayError("No Network or Cache File");
            return false;
        }
        class getUrlDataExtended extends getURLdata{
            getUrlDataExtended(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2) {
                try {
                    cm.addtoCacheFile((String)result2, cat_url);
                } catch (IOException e) {
                    Log.d("backpaper2-drawer","unable to write to file "+e.toString());
                }
                parseResultDrawer(result2);
            }
        }
        getUrlDataExtended url_data = new getUrlDataExtended();
        url_data.getData(cat_url);
        return false;
    }

    private void parseResultDrawer(Object result2) {
        this.displayLoaders(false);
        CategoriesParser pResult = new CategoriesParser();
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        try {
            categories_list = pResult.getObjectsArray(result2);
            if(!sPref.getBoolean("category_adult",false)){
                ArrayList<Categorie> new_categorie = new ArrayList<Categorie>();
                for(Categorie this_cat : categories_list){
                    if(!this_cat.getCategorieAdult()){
                        new_categorie.add(this_cat);
                    }
                }
                categories_list = new_categorie;
            };
            Categorie dummyCatUpdate = new Categorie();
            dummyCatUpdate.setCategorie_name("Refresh Categories List");
            dummyCatUpdate.setId(-2);
            categories_list.add(0, dummyCatUpdate);

            Categorie dummyCatRecent = new Categorie();
            dummyCatRecent.setCategorie_name("Recently Added");
            dummyCatRecent.setId(-1);
            categories_list.add(1,dummyCatRecent);

            Categorie popularImages = new Categorie();
            popularImages.setCategorie_name("Popular Images");
            popularImages.setId(-3);
            categories_list.add(2, popularImages);

            Categorie popularActress = new Categorie();
            popularActress.setCategorie_name("Popular Actress");
            popularActress.setId(-4);
            categories_list.add(3, popularActress);

            SlideMenuAdapter adapter = new SlideMenuAdapter(this,R.layout.drawer_list_item,categories_list);
            mDrawerList.setAdapter(adapter);
            showFirstTimeOptions(true);
        } catch (Exception e) {
            Log.d("backpaper2-drawer","error in parse result "+e.toString());
        }

        mDrawerList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Categorie this_cat = categories_list.get(position);
                if (position == 0) {
                    goOnNetworkDrawer(server_variable.getCategoriesUrl());
                }else if(this_cat.getId() == -1){
                    Name recentName = new Name();
                    recentName.setId(-1);
                    recentName.setName("Recent Images");
                    Intent recentIntent = new Intent(context, NameDetailImages.class);
                    recentIntent.putExtra(IntentConfigVariables.getNameDetailIntentMessage(), recentName);
                    startActivity(recentIntent);
                }
                else if (this_cat.getId() == -3){
                    Name recentName = new Name();
                    recentName.setId(-3);
                    recentName.setName("Popular Images");
                    Intent recentIntent = new Intent(context, NameDetailImages.class);
                    recentIntent.putExtra(IntentConfigVariables.getNameDetailIntentMessage(),recentName);
                    startActivity(recentIntent);
                }
                else if(this_cat.getId() == -4){
                    auto_updated = false;
                    String server_url = server_variable.getNamePopularUrlNoLimitWithParams
                            (GetDisplayParams.getFullDisplayAsQueryParams(context));
                    url = server_url;
                    Boolean cacheFile = cm.checkFileFromUrl(server_url);
                    Log.d("backpaper2-drawer", cacheFile.toString());
                    if (cm.checkFileFromUrl(server_url)) {
                        goOnFile(server_url);
                        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
                        if(sharedPrefs.getBoolean("auto_update",true) && Connectivity.isConnectedWifi(context)){
                            displayToast("Updating Data");
                        }else{
                            displayToast("Displaying Cached Data. Click Refresh in Menu Button To Load Latest Data");
                        }
                    } else {
                        displayLoaders(true);
                        goOnNetwork(server_url);
                    }
                    mDrawerLayout.closeDrawer(mDrawerList);
                    SharedPreferences settings = getSharedPreferences(IntentConfigVariables.getFirstTimeOptions(), 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(IntentConfigVariables.FirstTimeOptions.lastCategorieUrl, server_url);
                    editor.commit();
                }
                else {
                    auto_updated = false;
                    String server_url = server_variable.getNameUrlNoLimitWithParamsCategorie
                            (GetDisplayParams.getFullDisplayAsQueryParams(context), (Integer.valueOf(this_cat.getId())).toString());
                    url = server_url;
                    Boolean cacheFile = cm.checkFileFromUrl(server_url);
                    Log.d("backpaper2-drawer", cacheFile.toString());
                    if (cm.checkFileFromUrl(server_url)) {
                        goOnFile(server_url);
                        displayToast("Displaying Cached Data. Click Refresh in Menu Button To Load Latest Data");
                    } else {
                        displayLoaders(true);
                        goOnNetwork(server_url);
                    }
                    mDrawerLayout.closeDrawer(mDrawerList);
                    SharedPreferences settings = getSharedPreferences(IntentConfigVariables.getFirstTimeOptions(), 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(IntentConfigVariables.FirstTimeOptions.lastCategorieUrl, server_url);
                    editor.commit();
                }
            }
        });
    }

    private void showFirstTimeOptions(boolean b) {
        if(b){
            SharedPreferences settings = getSharedPreferences(IntentConfigVariables.getFirstTimeOptions(), 0);
            Boolean firstTime = settings.getBoolean(IntentConfigVariables.FirstTimeOptions.slidingMenu, true);
            if(firstTime){
                mDrawerLayout.openDrawer(mDrawerList);

                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(IntentConfigVariables.FirstTimeOptions.slidingMenu, false);
                editor.commit();
            }
        }
    }

    private boolean goOnFileDrawer(String cat_url) {
        try{
            String result2 = cm.readFromUrl(cat_url);
            parseResultDrawer(result2);
            return true;
        }catch(Exception e){
            Log.d("backpaper2-drawer","error while reading file "+e.toString());
            goOnNetwork(cat_url);
        }
        return false;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public void onPause(){
        super.onPause();
        //startapp
        try{
            startAppAd.onPause();
        }catch (Exception e){
            Log.d("backpaper2-startapp","failed on startapp "+e.toString());
        }
        //startapp
    }

    @Override
    public void onStart() {
        super.onStart();
        GTracker.trackActivity(context,"BackPaperMain");
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy(){

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        //startapp
        try{
            startAppAd.onBackPressed();
        }catch (Exception e){
            Log.d("backpaper2-startapp","failed on startapp "+e.toString());
            displayAdds();
        }
        //startapp
        super.onBackPressed();
    }

    @Override
    public void onResume(){
        super.onPause();
        if(url == null){
            try{
                SharedPreferences settings = this.getSharedPreferences
                        (IntentConfigVariables.getFirstTimeOptions(), 0);
                url = settings.getString(IntentConfigVariables.FirstTimeOptions.lastCategorieUrl, null);
                Log.d("backpaper2-main-onresume",url);
                Boolean cachefile = cm.checkFileFromUrl(url);
                Log.d("backpaper2", cachefile.toString());
                if (cm.checkFileFromUrl(url)) {
                    goOnFile(url);
                } else {
                    goOnNetwork(url);
                }
            }catch(Exception e){
                Log.d("backpaper2-on-resume"," errror "+e.toString());
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }
        //startapp
        try{
            startAppAd.onResume();
        }catch (Exception e){
            Log.d("backpaper2-startapp","failed on startapp "+e.toString());
            displayAdds();
        }
        //startapp
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_back_paper_main, menu);
        return true;
    }


    @SuppressLint("NewApi")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.refresh_data:
                reloadDataFromNetwork();
                return true;
            case R.id.menu_clear_cache:
                cm.clear();
                return true;
            case R.id.menu_lwp_list:
                Intent intent2 = new Intent(this, LwpListEdit.class);
                startActivity(intent2);
                return true;
            case R.id.grid_default_search:
                if(Build.VERSION.SDK_INT <13){
                    displayToast("Not Available Below Android 3.2 or HONEYCOMB Working on bringing it to More Versions");
                    return true;
                }
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_settings:
//            if(Build.VERSION.SDK_INT <13){
//                displayToast("Not Available Below Android 3.2 or HONEYCOMB Working on bringing it to More Versions");
//                return true;
//            }
                Intent intent_settings = new Intent(this, QuickPrefsActivity.class);
                startActivity(intent_settings);
                return true;
            case R.id.menu_about_app:
                if(Build.VERSION.SDK_INT <13){
                    displayToast("Not Available Below Android 3.2 or HONEYCOMB Working on bringing it to More Versions");
                    return true;
                }
                Intent intent_about_app = new Intent(this, AboutApp.class);
                startActivity(intent_about_app);
                return true;
            case R.id.menu_set_wallpaper:
                int REQUEST_CODE = 1;
                Toast toast = Toast.makeText(this, "Choose \"Heroine Wallpapers\" in the list ", Toast.LENGTH_LONG);
                toast.show();
                Intent intent_set_wallpaper = new Intent();
                intent_set_wallpaper.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
                startActivityForResult(intent_set_wallpaper, REQUEST_CODE);
                return true;
            case R.id.menu_give_feedback:
                String feedbackUserUrl = ServerVariables.getFeedbackUserUrl();
                Intent ifu = new Intent(Intent.ACTION_VIEW);
                ifu.setData(Uri.parse(feedbackUserUrl));
                startActivity(ifu);
                return true;
            case R.id.menu_request_new:
                String feedbackRequestUrl = ServerVariables.getFeedbackRequestUrl();
                Intent ifr = new Intent(Intent.ACTION_VIEW);
                ifr.setData(Uri.parse(feedbackRequestUrl));
                startActivity(ifr);
                return true;
            case android.R.id.home:
                mDrawerLayout.openDrawer(mDrawerList);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void reloadDataFromNetwork() {
        displayLoaders(true);
        goOnNetwork(url);
    }

    public void displayLoaders(Boolean bool) {
        if (bool) {
            TextView loadingText = (TextView) findViewById(R.id.loading);
            View loadingBar = findViewById(R.id.progress_large);
            loadingBar.setVisibility(View.VISIBLE);
            loadingText.setVisibility(View.VISIBLE);
        } else {
            TextView loadingText = (TextView) findViewById(R.id.loading);
            View loadingBar = findViewById(R.id.progress_large);
            loadingBar.setVisibility(View.GONE);
            loadingText.setVisibility(View.GONE);
        }
    }

    public void goOnFile(String server_url) {
        try {
            String result2 = cm.readFromUrl(server_url);
            parseResult(result2);
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            if(!auto_updated && Connectivity.isConnectedWifi(context) && sharedPrefs.getBoolean("auto_download",true) ){
                reloadDataFromNetwork();
                auto_updated = true;
            }
        } catch (Exception e) {
            Log.d("backpaper2", "error while reading file " + e.toString());
            goOnNetwork(server_url);
        }
    }

    public Boolean goOnNetwork(final String url) {
        if (!NetworkChecker.getNetStatus(this)) {
            displayError("No Network or Cache File");
            return false;
        }
        class getUrlDataExt extends getURLdata {
            @Override
            public void postresult(Object result2) {
                try {
                    cm.addtoCacheFile((String) result2, url);
                } catch (IOException e) {
                    Log.d("backpaper2",
                            "unable to write to file " + e.toString());
                }
                parseResult(result2);
            }
        }
        getUrlDataExt url_data = new getUrlDataExt();
        url_data.getData(url);
        return true;
    }

    public void displayError(String string) {
        TextView loadingText = (TextView) findViewById(R.id.loading);
        View loadingBar = findViewById(R.id.progress_large);
        loadingBar.setVisibility(View.GONE);
        loadingText.setText(string);
        loadingText.setVisibility(View.VISIBLE);
    }

    public void parseResult(Object result2) {
        parseResultNames pResult = new parseResultNames();
        try {
            ArrayList<Name> all_names = pResult.getObjectsArray(result2);
            goAdapter(all_names);
        } catch (Exception e) {
            Log.d("backpaper2", e.toString());
        }
    }

    private void displayToast(String msg) {
        try {
            mToast.setText(msg);
            mToast.show();
        } catch (Exception e) {
            mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    private void goAdapter(ArrayList<Name> all_names) {
        int position = getActionBar().getSelectedNavigationIndex();
        ArrayList<Name> names_list = getSortedList(position,all_names);
        name_list_array = names_list.toArray(new Name[names_list.size()]);
        try {
            adapter = new GridImageAdapter(this,
                    R.layout.resource_back_paper_main_adapter_1,
                    name_list_array);
            gridView = (GridView) findViewById(R.id.back_paper_main_gridview1);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(mMessageClickedHandler);
            gridView.setFastScrollEnabled(true);
            if (android.os.Build.VERSION.SDK_INT >= 13) {
                gridView.setFastScrollAlwaysVisible(true);
            }
        } catch (Exception e) {
            Log.d("backpaper2", e.toString());
        }
        try {
            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

            View loadingBar =  findViewById(R.id.progress_large);
            loadingBar.setVisibility(View.GONE);

        } catch (Exception e) {
            Log.d("backpaper2", e.toString());
        }
    }
    private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
                                long arg3) {
//			Intent intent = new Intent(parents.getContext(),NameDetailImages.class);

            //startapp show add
            if(getRandomBoolean()){
                try{
                startAppAd.showAd();
                startAppAd.loadAd();
                }catch (Exception e){
                    Log.d("backpaper2-startapp","start app failed "+e.toString());
                    displayAdds();
                }
            }
            //startapp show add
            Intent intent = new Intent(parents.getContext(),NameDetailImagesV2.class);
            Name selected_name = (Name) parents.getAdapter().getItem(arg2);
            GTracker.trackEvent(context,"BackPaperMain","grid_image_clicked",selected_name.getName(),new Long
                    (selected_name.getId()));
            intent.putExtra(IntentConfigVariables.getNameDetailIntentMessage(),selected_name);
            startActivity(intent);
        }
    };


    private void declareAddsStatus(){
        class getData extends getURLdata {
            @Override
            public void postresult(Object result2){
                String res = (String)result2;
                try {
                    JSONObject main_obj = new JSONObject(res);
                    JSONArray main_obj_array = main_obj.getJSONArray("objects");
                    for (int i = 0; i < main_obj_array.length(); i++) {

                        JSONObject this_obj = main_obj_array.getJSONObject(i);
                        SharedPreferences settings = getSharedPreferences(IntentConfigVariables.ADD_MOB_STATUS_PREF, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putBoolean(IntentConfigVariables.ADDMOB_STATUS_VAR, this_obj.getBoolean("addEnabled"));
//                        editor.putBoolean(IntentConfigVariables.ADDMOB_STATUS_VAR, true);
                        editor.commit();
                    }
                }catch(Exception e){
                    dataError();
                }
            }

            @Override
            public void onTimeOut(Object result2){
                Log.d("backpaper2-network","TIME - OUT ");
                dataError();
            }
            @Override
            public void onError(Object result2){
                Log.d("backpaper2-network","Error");
                dataError();
            }
            public void dataError(){

            }
        }

        getData getAddStatus = new getData();
        getAddStatus.getData(ServerVariables.getInfoUrl());
    }

    private StartAppAd startAppAd;
    private void displayAdds(){
        SharedPreferences settings = getSharedPreferences(IntentConfigVariables.ADD_MOB_STATUS_PREF, 0);
        if(settings.getBoolean(IntentConfigVariables.ADDMOB_STATUS_VAR,false)){
            try{
            StartAppAd.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);
            StartAppSearch.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);
            StartAppSearch.showSearchBox(this);
            startAppAd = new StartAppAd(this);
            startAppAd.showAd(); // show the ad
            startAppAd.loadAd(); // load the next ad
            }catch (Exception e){
                Log.d("backpaper2-startapp","Failed to load startapp "+e.toString());
            }
//            AdView adView = new AdView(this, AdSize.BANNER,"4648b5bd0ef8426a");
//            LinearLayout main_layout = (LinearLayout)findViewById(R.id.id_main_layout);
//            AdRequest adRequest = new AdRequest();
////            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
////            adRequest.addTestDevice("410029197ddfc00");
//            main_layout.addView(adView);
//            adView.loadAd(adRequest);
        }
    }

    public boolean getRandomBoolean() {
        return true;
//        Random random = new Random();
//        return random.nextBoolean();
    }
}
