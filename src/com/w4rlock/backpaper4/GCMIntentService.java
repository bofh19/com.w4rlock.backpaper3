package com.w4rlock.backpaper4;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.w4rlock.backpaper2.gcm.GCMNotify;
import com.w4rlock.backpaper2.gcm.GCMRegisterOnServer;

import static com.w4rlock.backpaper2.gcm.GCMServerSettings.senderId;

/**
 * Created by krishna on 5/17/13.
 */
public class GCMIntentService extends GCMBaseIntentService{

    public GCMIntentService(){
        super(senderId);
    }


    @Override
    protected void onError(Context arg0, String arg1) {
        Log.d("backpaper2-GCM", arg1);

    }

    @Override
    protected void onMessage(Context arg0, Intent arg1) {
        final Bundle bundle = arg1.getExtras();
        GCMNotify.displayNotify(arg0, bundle);
        Log.d("backpaper2-GCM","Message: "+bundle);
    }

    @Override
    protected void onRegistered(Context arg0, String arg1) {
        Log.d("backpaper2-GCM-register",arg1);
        GCMRegisterOnServer.register(arg0);
    }

    @Override
    protected void onUnregistered(Context arg0, String arg1) {
        Log.d("backpaper2-GCM-unregister",arg1);

    }

}