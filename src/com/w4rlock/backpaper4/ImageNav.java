package com.w4rlock.backpaper4;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.w4rlock.backpaper2.ActivityUtils.ImageNavUtils.ListViewImageAdapter;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.parseResultNameImages;
import com.w4rlock.backpaper2.Logger.GTracker;
import com.w4rlock.backpaper2.Logger.PostDevAcc;
import com.w4rlock.backpaper2.configs.GetDisplayParams;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper2.models.NameImages;
import com.w4rlock.backpaper2.utils.ImageLoader;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.backpaper2.utils.Utils;
import com.w4rlock.cacheManager.CacheManager;
import com.w4rlock.cacheManager.UrlCacheManager;
import com.w4rlock.cacheManager.getURLdata;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

@SuppressLint({ "NewApi", "ValidFragment" })
public class ImageNav extends ActionBarActivity {
	public static final ServerVariables server_variable = new ServerVariables();
	ImageLoader imageLoader;
	private NameImage given_name_image = null;
	// private LinearLayout myGallery;
	private Name given_name = null;
	private ListView listview;
	private ListViewImageAdapter adapter;
	private NameImages name_images = null;
	private Toast mToast = null;
	private TouchImageView img_large = null;
	private NameImage selected_image = null;
	private CacheManager cm;
	static DisplayMetrics displaymetrics;
	private Context context;
	private long enqueue;
	private DownloadManager dm;
	BroadcastReceiver receiver;
	// lwp set variables //
	String location_pref = IntentConfigVariables.LWP_IMGS_LOCATION_PREF;
	String location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_nav);
		cm = new CacheManager(this.getApplicationContext());
        context = this.getApplicationContext();
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            try {
                getActionBar().setDisplayHomeAsUpEnabled(true);
            }catch (NullPointerException e){
                Log.d("backpaper2","unable to set nav home up");
            }
        }else{
        	try {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }catch (NullPointerException e){
                Log.d("backpaper2","unable to set nav home up");
            }
        }
		Intent intent = getIntent();
		given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameImgNavMessageName());
		listview = (ListView) findViewById(R.id.activity_image_nav_listview1);
		img_large = (TouchImageView) findViewById(R.id.activity_image_nav_imageview1);
//		zoom = (ZoomControls) findViewById(R.id.zoomControls1);
		imageLoader = new ImageLoader(this);
		try {
			given_name_image = (NameImage) intent.getSerializableExtra(IntentConfigVariables.getNameImgNavMessageImages());
			selected_image = given_name_image;
			imageLoader.DisplayImage(given_name_image.getImagefile(), img_large);
		} catch (Exception e) {
			Log.d("backpaper2","Error at displaying first image " + e.toString());
		}
		displayListView(given_name);
		addListenerOnButton();

		setTitle(selected_image.getImageName());
		displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		if(android.os.Build.VERSION.SDK_INT >= 9){
		        receiver = new BroadcastReceiver() {
	            @Override
	            public void onReceive(Context context, Intent intent) {
	                String action = intent.getAction();
	                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
	               //     long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
	                    Query query = new Query();
	                    query.setFilterById(enqueue);
	                    Cursor c = dm.query(query);
	                    if (c.moveToFirst()) {
	                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
	                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
	                            String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
	                            displayToast("Sucessfully Downloaded Image Saved at "+Uri.parse(uriString).toString());
                                Intent image_intent = new Intent();
                                image_intent.setAction(Intent.ACTION_VIEW);
                                image_intent.setDataAndType(Uri.parse(uriString), "image/*");
                                startActivity(image_intent);
	                        	Log.d("backpaper2-download-service",Uri.parse(uriString).toString());
	                        }
	                    }
	                }
	            }
	        };
	        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
		}
	}

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onPause(){
        super.onPause();
        try{
            unregisterReceiver(receiver);
        }catch (Exception e){
            Log.d("backpaper2-image-nav",e.toString());
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try{
            unregisterReceiver(receiver);
        }catch (Exception e){
            Log.d("backpaper2-image-nav",e.toString());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
//        EasyTracker.getInstance().activityStart(this); // Add this method.
        GTracker.trackActivity(context,"ImageNav");
    }

    @Override
    public void onStop() {
        super.onStop();
//        EasyTracker.getInstance().activityStop(this); // Add this method.
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_image_nav, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
	private File copyImgFile(File img_cache_file) throws Exception {
        img_cache_file = new File(cm.getFileNameFromUrl(selected_image.getImagefile()));
        try {
            String out_file_name = selected_image.getImageName()+ "_"+ Integer.valueOf(selected_image.getId()).toString() + ".jpg";
            File dir_file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+ "/WallpaperDownloads");
            if (!dir_file.exists()) {
                dir_file.mkdirs();
            }
            File img_out_file = new File(dir_file, out_file_name);
            if (!img_out_file.exists()) {
                img_out_file.createNewFile();
            } else {
                img_out_file.delete();
                img_out_file.createNewFile();
            }
            Utils.copyFile(img_cache_file, img_out_file);
            return img_out_file;
        }catch (Exception e){
            throw e;
        }
    }
	public void addListenerOnButton() {

		// ///////////////// BUTTON LISTENERS /////////

		Button button = (Button) findViewById(R.id.set_wallpaper);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (cm.checkFileFromUrl(selected_image.getImagefile())) {
					File img_cache_file = new File(cm.getFileNameFromUrl(selected_image.getImagefile()));
					try {
						File img_out_file = copyImgFile(img_cache_file);
						Bitmap wallpaperImage = BitmapFactory.decodeFile(img_out_file.getAbsolutePath());
						WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
						try {
							Bitmap blank = BitmapHelper.createNewBitmap(wallpaperManager.getDesiredMinimumWidth(),wallpaperManager.getDesiredMinimumHeight());
							Bitmap overlay = BitmapHelper.overlayIntoCentre(blank, wallpaperImage);
							wallpaperManager.setBitmap(overlay);
							
							displayToast("Saved image to file "+ img_out_file.getAbsolutePath()+ " will be Saved as Wrong Image if done before it is displayed To Prevent Spamming of requests to Server");
						} catch (Exception e) {
							Log.d("backpaper2-imageNav-wallpapersetting",e.toString());
							displayToast("Please Wait Untill Image is Loaded. This is to Prevent spamming of requests to server");
						}
					} catch (Exception e) {
						Log.d("backpaper2","unable to copy file " + e.toString());
					}
				} else {
					displayToast("Please Wait Untill Image is Loaded. This is to Prevent spamming of requests to server");
				}
                PostDevAcc pData = new PostDevAcc(context);
                pData.postDevAccData(selected_image.getId(),4);
                GTracker.trackEvent(context,"ImageNav","set_wallpaper","4"+" "+selected_image.getResourceUri(),new Long(selected_image.getId()));
			}

		});
		Button button_download = (Button) findViewById(R.id.download_image);
		button_download.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(android.os.Build.VERSION.SDK_INT >= 9){
					displayToast("Starting Download");
					dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    Log.d("backpaper2-dm",selected_image.getImagefileOriginal());
			        Request request = new Request(Uri.parse(selected_image.getImagefileOriginal()));
			        String out_file_name = selected_image.getImageName()+ "_full_"+ Integer.valueOf(selected_image.getId()).toString() + ".jpg";
			        request.setDestinationInExternalPublicDir((Environment.DIRECTORY_DOWNLOADS).toString(),out_file_name);
			        enqueue = dm.enqueue(request);
				}else{
//					displayToast("Requires API Level 9 or GINGERBREAD");
                    downloadOriginalImageApi8(selected_image);
				}
                PostDevAcc pData = new PostDevAcc(context);
                pData.postDevAccData(selected_image.getId(),3);
                GTracker.trackEvent(context,"ImageNav","download_image","3"+" "+selected_image.getResourceUri(),new Long(selected_image.getId()));
			}
		});

		Button button_share = (Button) findViewById(R.id.share_button);
		button_share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (cm.checkFileFromUrl(selected_image.getImagefile())) {
					File img_cache_file = new File(cm.getFileNameFromUrl(selected_image.getImagefile()));
					try {
                        File img_out_file = copyImgFile(img_cache_file);
						displayToast("Saved image to file "+ img_out_file.getAbsolutePath()+ " will be Saved as Wrong Image if done before it is displayed To Prevent Spamming of requests to Server");
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        sharingIntent.setType("image/*");
                        Uri uri = Uri.fromFile(img_out_file);
                        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        startActivity(Intent.createChooser(sharingIntent,"Share Image via"));
					} catch (Exception e) {
						Log.d("backpaper2",
								"unable to copy file " + e.toString());
					}
				} else {
					displayToast("Please Wait Untill Image is Loaded. This is to Prevent spamming of requests to server");
				}
                PostDevAcc pData = new PostDevAcc(context);
                pData.postDevAccData(selected_image.getId(),2);
                GTracker.trackEvent(context,"ImageNav","share_button","2"+" "+selected_image.getResourceUri(),new Long(selected_image.getId()));

			}
		});

		Button button_lwp = (Button) findViewById(R.id.add_to_live_wallpaper);
		button_lwp.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// displayToast("Comming Soon");
				if (cm.checkFileFromUrl(selected_image.getImagefile())) {
					class SelectListDialogFragmentExtended extends SelectListDialogFragment{
						@Override
						 protected void setList(int i) {
							Log.d("backpaper2-add-to-lwp","list number selected "+i);
					        	switch(i){
					        	case 1:
					        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_1;
					        		addToLWPList();
					        		return;
					        	case 2:
					        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_2;
					        		addToLWPList();
					        		return;
					        	case 3:
					        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_3;
					        		addToLWPList();
					        		return;
					        	case 4:
					        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_4;
					        		addToLWPList();
					        		return;
					        	default:
					        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR;
					        		addToLWPList();
					        		return;
					        	}
							}

						private void addToLWPList() {
							File img_cache_file = new File(cm.getFileNameFromUrl(selected_image.getImagefile()));
							try {
                                File img_out_file = copyImgFile(img_cache_file);
								displayToast("Saved image to file "+ img_out_file.getAbsolutePath()+ " will be Saved as Wrong Image if done before it is displayed To Prevent Spamming of requests to Server");
								SharedPreferences settings = getSharedPreferences(location_pref, 0);
								if (android.os.Build.VERSION.SDK_INT >= 13) {
									Set<String> imgs_locs = new TreeSet<String>(settings.getStringSet(location_pref_var,new TreeSet<String>()));
									imgs_locs.add(img_out_file.getAbsolutePath());
									SharedPreferences.Editor editor = settings.edit();
									editor.putStringSet(location_pref_var,imgs_locs);
									editor.commit();
								}else{
                                    Set<String> imgs_locs = new TreeSet<String>(Utils.getStringArrayPref(context,location_pref_var));
                                    imgs_locs.add(img_out_file.getAbsolutePath());
                                    Utils.setStringArrayPref(context,location_pref_var,new ArrayList<String>(imgs_locs));
//                                    Log.d("backpaper2",imgs_locs.toString());
                                }
							} catch (Exception e) {
								Log.d("backpaper2","unable to copy file " + e.toString());
							}
                            PostDevAcc pData = new PostDevAcc(context);
                            pData.postDevAccData(selected_image.getId(),1);
                            GTracker.trackEvent(context,"ImageNav","add_to_live_wallpaper","1"+" "+selected_image.getResourceUri(),new Long(selected_image.getId()));
						}
					}
//                    if(android.os.Build.VERSION.SDK_INT >= 11){
//					    android.support.v4.app.FragmentManager fm = getFragmentManager();
//                        SelectListDialogFragmentExtended sdf = new SelectListDialogFragmentExtended();
//                        sdf.show(fm,"Pick a List");
//                    }else{
                        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                        SelectListDialogFragmentExtended sdf = new SelectListDialogFragmentExtended();
                        sdf.show(fm,"Pick a List");
//                    }
				} else {
					displayToast("Please Wait Untill Image is Loaded. This is to Prevent spamming of requests to server");
				}
			}
		});

        Button image_report_button = (Button) findViewById(R.id.image_report);
        image_report_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String feedbackUrl = ServerVariables.getFeedBackReportUrl(selected_image.getId());
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(feedbackUrl));
                startActivity(i);
            }
        });
	}

    private void downloadOriginalImageApi8(final NameImage selected_image) {
        if(!NetworkChecker.getNetStatus(context)){
            displayToast("No Network");
            return;
        }
        class downloadImageData extends getURLdata{
            downloadImageData(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2) {
                try {
                    String out_file_name = selected_image.getImageName()+ "_"+ Integer.valueOf(selected_image.getId()).toString() + ".jpg";
                    File dir_file = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)));
                    if (!dir_file.exists()) {
                        dir_file.mkdirs();
                    }
                    File img_out_file = new File(dir_file, out_file_name);
                    if (!img_out_file.exists()) {
                        img_out_file.createNewFile();
                    } else {
                        img_out_file.delete();
                        img_out_file.createNewFile();
                    }
                    PrintWriter out = null;
                    out = new PrintWriter(new BufferedWriter(new FileWriter(img_out_file.getAbsolutePath(), false)));
                    if (out != null)
                        out.println((String)result2);
                    if (out !=null)
                        out.close();
                    displayToast("Sucessfully Saved File at "+img_out_file.getAbsolutePath());
                    try{
                        Intent image_intent = new Intent();
                        image_intent.setAction(Intent.ACTION_VIEW);
                        image_intent.setDataAndType(Uri.parse(img_out_file.getAbsolutePath()), "image/*");
                        startActivity(image_intent);
                    }catch (Exception e){
                        Log.d("backpaper2-download-api8","start gallery intent "+e.toString());
                    }
                } catch (Exception e) {
                    Log.d("backpaper2-download-api8","unable to write to file "+e.toString());
                    displayToast("Error while Downloading/Saving file "+e.toString());
                }
            }
        }
        downloadImageData url_data = new downloadImageData();
        displayToast("Starting Download...");
        url_data.getData(selected_image.getImagefileOriginal());
    }

    public static class BitmapHelper {

		public static Bitmap overlayIntoCentre(Bitmap bmp1, Bitmap bmp2) {
			try {
				Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),
						bmp1.getHeight(), bmp1.getConfig());
				Canvas canvas = new Canvas(bmOverlay);
				canvas.drawBitmap(bmp1, new Matrix(), null);// draw background
															// bitmap

				// overlay the second in the centre of the first
				// (may experience issues if the first bitmap is smaller than
				// the second, but then why would
				canvas.drawBitmap(bmp2,(bmp1.getWidth() / 2) - (bmp2.getWidth() / 2), 0, null);// you want to overlay a bigger one over a smaller one?!)
				return bmOverlay;
			} catch (Exception e) {
				Log.d("backpaper2-imageNav", e.toString());
				return null;
			}
		}

		public static Bitmap createNewBitmap(int width, int height) {
			// create a blanks bitmap of the desired width/height
			return Bitmap.createBitmap(width, height, Config.ARGB_8888);
		}
	}

	private void displayListView(Name given_name) {
		class getPhotoData extends UrlCacheManager {
			public getPhotoData(Context context) {
				super(context);
                setReadTimeOut(60000);
                setConnTimeOut(60000);
			}

			@Override
			public void postresult(Object result2) {
				parseResultNameImages presult = new parseResultNameImages(context);
				try {
					name_images = presult.getObjectsArray(result2);
					goAdapter(name_images);
				} catch (Exception e) {
					Log.d("backpaper2", "1" + e.toString());
				}
			}
		}
		getPhotoData getphotodata = new getPhotoData(
				this.getApplicationContext());
		getphotodata.getData(server_variable.getNameAllImageUrlWithParams(
				(given_name.getId()),
				GetDisplayParams.getFullDisplayAsQueryParams(this)));
	}

	private void goAdapter(NameImages name_images) {
		adapter = new ListViewImageAdapter(this,R.layout.resource_image_nav_adapter_1, name_images.getImages());
		listview.setAdapter(adapter);
		listview.setOnScrollListener(mScrollListener);
		listview.setOnItemClickListener(mMessageClickedHandler);
		displayLoaders(false);
        setScrollPosOnList(selected_image);
	}

	private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
			NameImage selected_name_image = (NameImage) parents.getAdapter()
					.getItem(arg2);
			Log.d("backpaper2",
					"selected image id "
							+ Integer.valueOf(selected_name_image.getId())
									.toString());
			Log.d("backpaper2",
					"selected image url " + selected_name_image.getImagefile());
            GTracker.trackEvent(context,"ImageNav","view_full_image",selected_name_image.getResourceUri() ,new Long(selected_name_image.getId()));
            GTracker.trackEvent(context,"ImageNav","view_full_image_pos",String.valueOf(arg2) ,new Long(selected_name_image.getId()));
			selected_image = selected_name_image;
			imageLoader.DisplayImage(selected_name_image.getImagefile(),img_large);
            setScrollPosOnList(selected_image);
		}
	};

    private void setScrollPosOnList(NameImage selected_image) {
        int i=0;
        if(selected_image == null)
            return;
        while(adapter.getItem(i)!=null){
            NameImage cur_image = adapter.getItem(i);
            if(cur_image.getId() == selected_image.getId()){
                listview.setSelection(i);
                break;
            }
            i++;
        }
    }

    private void displayLoaders(boolean b) {
		if (b) {
			try {
				TextView loading = (TextView) findViewById(R.id.loading);
				loading.setVisibility(View.VISIBLE);
				View loadingbar = (View) findViewById(R.id.progress_large);
				loadingbar.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				Log.d("backpaper2", e.toString());
			}
		} else {
			try {
				TextView loading = (TextView) findViewById(R.id.loading);
				loading.setVisibility(View.GONE);
				View loadingbar =  findViewById(R.id.progress_large);
				loadingbar.setVisibility(View.GONE);
			} catch (Exception e) {
				Log.d("backpaper2", e.toString());
			}
		}
	}

	// / next set in listview
	public void parseresultNext(String string) {
		parseResultNameImages presult = new parseResultNameImages(context);
		try {
			NameImages name_images_new = presult.getObjectsArrayNext(
					name_images, string);
			ArrayList<NameImage> new_name_images = new ArrayList<NameImage>(
					name_images.getImages());
			goAdapterNext(name_images_new.getImages());
			for (NameImage this_name_images_new : name_images_new.getImages()) {
				new_name_images.add(this_name_images_new);
			}
			name_images.setImages(new_name_images);
			try {
				name_images.setNext(name_images_new.getNext());
			} catch (Exception e) {
				name_images.setNext(null);
			}
			onDataLoaded();
		} catch (Exception e) {
			Log.d("backpaper2", "1" + e.toString());
		}
	}

	private void goAdapterNext(ArrayList<NameImage> name_images_list) {
		for (NameImage this_name_image : name_images_list) {
			// System.out.println(this_name_image.getId());
			adapter.add(this_name_image);
		}
	}

	private void onDataLoaded() {
		loading_data = false;
		load_data = false;
		displayLoaders(false);
	}

	// next result set functions ends here
	private Boolean loading_data = false;
	private Boolean load_data = false;
	private OnScrollListener mScrollListener = new OnScrollListener() {
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			if (load_data && !loading_data) {
				attachLoaderToGrid();
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			if (!loading_data) {
				if (firstVisibleItem + visibleItemCount == totalItemCount) {
					load_data = true;
				} else {
					// load_data = false;
				}
			}
		}
	};

	protected void attachLoaderToGrid() {
        loading_data = true;
		if (name_images.getNext() != null) {
			displayLoaders(true);
			displayListViewNext(name_images.getNext());
		} else {
			displayToast("All data loaded.");
			Log.d("backpaper2", "no next item");
		}
	}

	private void displayListViewNext(String next) {
		// System.out.println("next url " + next);
		displayLoaders(true);
		class getPhotoData extends UrlCacheManager {
			public getPhotoData(Context contextx) {
				super(contextx);
                setReadTimeOut(60000);
                setConnTimeOut(60000);
			}

			@Override
			public void postresult(Object result2) {
				parseresultNext((String) result2);
			}
		}
		getPhotoData getphotodata = new getPhotoData(this);
		getphotodata.getData(next);
	}

	// // next set in listview ends here

	private void displayToast(String msg) {
		try {
			mToast.setText(msg);
			mToast.show();
		} catch (Exception e) {
			mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
			mToast.show();
		}
	}

	
	// DIALOG FOR LWP LIST SELECTIN ITEMS
	public static class SelectListDialogFragment extends android.support.v4.app.DialogFragment {
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setTitle("Pick a List");
	        String[] items = {"List 1","List 2","List 3","List 4"};
	        builder.setItems(items, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	            	   setList(which+1);
	               }
	        });
	        return builder.create();
	    }
	    protected void setList(int i) {
        	
		}
	}
}


