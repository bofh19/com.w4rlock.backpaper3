package com.w4rlock.backpaper4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.searchboxsdk.android.StartAppSearch;
import com.startapp.android.publish.StartAppAd;
import com.w4rlock.backpaper2.ActivityUtils.ImageNavV2Utils.FragmentPagerAdapter;
import com.w4rlock.backpaper2.ActivityUtils.ImageNavV2Utils.ImageViewFragment;
import com.w4rlock.backpaper2.ActivityUtils.ImageNavV2Utils.VerticalViewPager;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.CommentsFragment;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.LikesParser;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.PostCommentFragment;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.SLikePost;
import com.w4rlock.backpaper2.Logger.GTracker;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Comments;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper2.utils.ImageLoader;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.cacheManager.getURLdata;

@SuppressLint("NewApi")
public class ImageNavV2 extends ActionBarActivity implements PostCommentFragment.OnPostButtonClickedListner,ImageViewFragment.OnImageChangedListener {
    public static final ServerVariables server_variable = new ServerVariables();
    ImageLoader imageLoader;
    private NameImage given_name_image = null;
    private static final int NUM_PAGES = 2;
    // private LinearLayout myGallery;
    private Name given_name = null;
    static DisplayMetrics displaymetrics;
    private Context context;
//    private ViewPager mPager;
    private VerticalViewPager mPager;
//    DirectionalViewPager mPager;
    private PagerAdapter mPagerAdapter;
    public CommentsFragment comments_fragment = null;
    private Toast mToast=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_nav_v2_f1);
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            try {
                getActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (NullPointerException e) {
                Log.d("backpaper2", "unable to set nav home up");
            }
        }else{
        	try {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }catch (NullPointerException e){
                Log.d("backpaper2","unable to set nav home up");
            }
        }
        context = this.getApplicationContext();
        Intent intent = getIntent();
        given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameImgNavMessageName());
        try {
            given_name_image = (NameImage) intent.getSerializableExtra(IntentConfigVariables.getNameImgNavMessageImages());
        } catch (Exception e) {
            Log.d("backpaper2", "Error at displaying first image " + e.toString());
        }
        setTitle(given_name_image.getImageName());
        displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

       mPager = (VerticalViewPager) findViewById(R.id.image_nav_v2_vertical_pager);
        mPager.verticalSwipeEnabled = false;
//        mPager = (DirectionalViewPager)findViewById(R.id.image_nav_v2_pager);
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        this.setTitle(given_name.getName());
        setCommentClickAction();
        displayAdds();
    }

    private void setCommentClickAction(){
        LinearLayout comment_layout = (LinearLayout)findViewById(R.id.main_layout_2);
        comment_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(1);
                setLayoutsVisibility(1);
            }
        });
        TextView commentsTextView = (TextView)findViewById(R.id.image_nav_v2_show_comments);
        commentsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(1);
                setLayoutsVisibility(1);
            }
        });
        LinearLayout images_layout = (LinearLayout)findViewById(R.id.main_layout_1);
        images_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(0);
                setLayoutsVisibility(0);
            }
        });
        TextView imagesTextView = (TextView)findViewById(R.id.image_nav_v2_show_images);
        imagesTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(0);
                setLayoutsVisibility(0);
            }
        });
        mPager.setOnPageChangeListener(new VerticalViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setLayoutsVisibility(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setLikeButtonActions();
    }

    private void setLayoutsVisibility(int current_layout){
        LinearLayout images_layout = (LinearLayout)findViewById(R.id.main_layout_1);
        LinearLayout comment_layout = (LinearLayout)findViewById(R.id.main_layout_2);
        if(current_layout==0){
            images_layout.setVisibility(View.GONE);
            comment_layout.setVisibility(View.VISIBLE);
        }else if(current_layout==1){
            comment_layout.setVisibility(View.GONE);
            images_layout.setVisibility(View.VISIBLE);
        }
    }

    private void setLikeButtonActions(){
        View likeButtonLayout = findViewById(R.id.like_button_layout);
        likeButtonLayout.bringToFront();
        Button plusOneButton = (Button) findViewById(R.id.name_detail_plusone_button);
        plusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLikeRank(1);
            }
        });
        Button zeroButton = (Button) findViewById(R.id.name_detail_zero_button);
        zeroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLikeRank(0);
            }
        });

        Button minusOneButton = (Button) findViewById(R.id.name_detail_minusone_button);
        minusOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLikeRank(-1);
            }
        });
        setInitialCurrentRanks();
        setInitialButtonRankView();
    }

    private void postLikeRank(final int rank) {
        if(!NetworkChecker.getNetStatus(getApplicationContext())){
            displayToast("Not able to Connect to Internet");
            return;
        }
        class LikePost extends SLikePost {
            @Override
            public void failed(String message){
                displayToast(message);
            }
            @Override
            public void postResult(Object result){
                try{
                    displayToast(result.toString()+" On "+rank);
                    SharedPreferences settings_likes =  context.getSharedPreferences(IntentConfigVariables.LikesDetailsPref, 0);
                    SharedPreferences.Editor settings_likes_editor = settings_likes.edit();
                    settings_likes_editor.putLong(IntentConfigVariables.LikesLastCommentEpoch,System.currentTimeMillis());
                    settings_likes_editor.putInt(IntentConfigVariables.LikesCurrentStatusVar(given_name_image.getCommentUid()),rank);
                    settings_likes_editor.commit();
                    setButtonClickedViews(rank,true);
                    setInitialCurrentRanks();
                }catch(Exception e){
                    Log.d(DEBUG_TAG,"error while post result display toast "+e.toString());
                }
            }
        }
        String url = ServerVariables.getLikePostUrl();
        String rank_given = String.valueOf(rank);
        SharedPreferences settings_likes =  context.getSharedPreferences(IntentConfigVariables.LikesDetailsPref, 0);
        long timeDiff = System.currentTimeMillis() - settings_likes.getLong(IntentConfigVariables.LikesLastCommentEpoch,0);
        if(timeDiff/1000 < 5){
            displayToast("Please Wait "+timeDiff/1000+" before rating again");
            return;
        }
        String dev_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String domain = ServerVariables.getDomain();
        SharedPreferences settings =  context.getSharedPreferences(IntentConfigVariables.CsrfTokenPref, 0);
        String csrf_token = null;
        if(( csrf_token = settings.getString(IntentConfigVariables.CsrfTokenValue,null)) == null){
            displayToast("Some thing has Gone wrong. Sorry About that !!!!. Press back and come back here.");
            return;
        }
        LikePost new_like = new LikePost();
        String rank_uid = given_name_image.getCommentUid();
        displayToast("Submitting Rating");
        new_like.postData(url, domain, csrf_token, rank_given, rank_uid, dev_id);
    }

    private void setInitialButtonRankView(){
        SharedPreferences settings_likes = context.getSharedPreferences(IntentConfigVariables.LikesDetailsPref,0);
        if(settings_likes.getInt(IntentConfigVariables.LikesCurrentStatusVar(given_name_image.getCommentUid()),-2)==-2){
            setButtonClickedViews(-2,false);
            return;
        };
        if(!NetworkChecker.getNetStatus(getApplicationContext())){
            displayToast("Not able to Connect to Internet");
            return;
        }
        class getUserRank extends getURLdata {
            getUserRank(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2){
                try {
                    int currentRank = LikesParser.getMyRank((String) result2);
                    if(currentRank!=-1)
                        setButtonClickedViews(currentRank,true);
                    else if(currentRank == -2)
                        displayToast("Not Rated. To Rate Select on the top right corner");
                    else
                        setButtonClickedViews(currentRank,false);
                } catch (Exception e) {
                    Log.d("backpaper2","unable to write to cache file "+e.toString());
                }
            }
            @Override
            public void onError(Object result){
                displayToast( "Unable to get Users Current Rank");
            }
            @Override
            public void onTimeOut(Object result){
                displayError("Unable to get Users Current Rank");
            }
        }
        getUserRank gettingUserRank = new getUserRank();
        String dev_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String rank_uid = given_name_image.getCommentUid();
        String current_url = ServerVariables.getLikesListOwnUrl(rank_uid,dev_id);
        Log.d("backpaper2-likes-own-url",current_url);
        gettingUserRank.getData(current_url);
    }

    private  void setInitialCurrentRanks(){
        final TextView ranksText = (TextView)findViewById(R.id.name_detail_current_rank);
        ranksText.setText("loading..");
        if(!NetworkChecker.getNetStatus(getApplicationContext())){
            ranksText.setText("XX");
            return;
        }
        class getRanksSum extends getURLdata{
            getRanksSum(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
            @Override
            public void postresult(Object result2){
                try {
                    ranksText.setText(LikesParser.getTotalCount((String)result2));
                } catch (Exception e) {
                    Log.d("backpaper2","unable to write to cache file "+e.toString());
                    ranksText.setText("XX");
                }
            }
            @Override
            public void onError(Object result){
                displayToast( "Unable to get Users Current Rank");
            }
            @Override
            public void onTimeOut(Object result){
                displayError("Unable to get Users Current Rank");
            }
        }
        getRanksSum gettingRanksSum = new getRanksSum();
        String rank_uid = given_name_image.getCommentUid();
        String current_url = ServerVariables.getLikesListUrl(rank_uid);
        Log.d("backpaper2-likes-list-url",current_url);
        gettingRanksSum.getData(current_url);
    }

    private void setButtonClickedViews(int rank, boolean b) {
        Button plusOneButton = (Button) findViewById(R.id.name_detail_plusone_button);
        Button zeroButton = (Button) findViewById(R.id.name_detail_zero_button);
        Button minusOneButton = (Button) findViewById(R.id.name_detail_minusone_button);
        // minusOneButton.setBackgroundResource(R.drawable.arrow_down_icon);
        // plusOneButton.setBackgroundResource(R.drawable.arrow_up_icon);
        minusOneButton.setBackgroundColor(Color.TRANSPARENT);
        plusOneButton.setBackgroundColor(Color.TRANSPARENT);
        if(b){
            if(rank == 1){
                plusOneButton.setBackgroundColor(getResources().getColor(R.color.black_transparent));
                // plusOneButton.setBackgroundResource(R.drawable.arrow_up_icon_black);
            }else if(rank == -1){
                minusOneButton.setBackgroundColor(getResources().getColor(R.color.black_transparent));
                // minusOneButton.setBackgroundResource(R.drawable.arrow_down_icon_black);
            }
        }
    }



    @Override
    public void onPostButtonClicked(Comments comment) {
        comments_fragment.addNewComment(comment);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
//        EasyTracker.getInstance().activityStart(this); // Add this method.
        GTracker.trackActivity(context, "ImageNav");
    }

    @Override
    public void onStop() {
        super.onStop();
//        EasyTracker.getInstance().activityStop(this); // Add this method.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_image_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onImageChanged(NameImage current_name_image) {
        comments_fragment.redrawComments(current_name_image);
        given_name_image = current_name_image;
        setInitialCurrentRanks();
        setInitialButtonRankView();
    }


    private void displayToast(String msg){
        try{
            mToast.setText(msg);
            mToast.show();
        }catch(Exception e){
            try{
                mToast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
                mToast.show();
            }catch (Exception ex){
                Log.d("backpaper2",ex.toString());
            }
        }
    }

    private void displayError(String string) {
//        TextView loading_text = (TextView)this.getActivity().findViewById(R.id.loading);
//        loading_text.setText(string);
//        loading_text.setVisibility(View.VISIBLE);
        displayToast(string);
    }

//    private class PagerAdapter extends FragmentStatePagerAdapter {
    private class PagerAdapter extends FragmentPagerAdapter {
        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new ImageViewFragment();
            } else {
                comments_fragment = new CommentsFragment();
                return comments_fragment;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

    }



    private void displayAdds(){
        SharedPreferences settings = getSharedPreferences(IntentConfigVariables.ADD_MOB_STATUS_PREF, 0);
        if(settings.getBoolean(IntentConfigVariables.ADDMOB_STATUS_VAR,false)){
            try{
            StartAppAd.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);
            StartAppSearch.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);
            StartAppSearch.showSearchBox(this);
            }catch (Exception e){
                Log.d("backpaper2-startapp","failed to load startapp");
            }
//            AdView adView = new AdView(this, AdSize.BANNER,"4648b5bd0ef8426a");
//            LinearLayout main_layout = (LinearLayout)findViewById(R.id.id_main_layout);
//            AdRequest adRequest = new AdRequest();
////            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
////            adRequest.addTestDevice("410029197ddfc00");
//            main_layout.addView(adView);
//            adView.loadAd(adRequest);
        }
    }
}


