package com.w4rlock.backpaper4;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.w4rlock.backpaper2.ActivityUtils.LwpListEditUtils.ImgLocHolder;
import com.w4rlock.backpaper2.ActivityUtils.LwpListEditUtils.LwpGridImageAdapter;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.utils.Utils;

@SuppressLint("NewApi")
public class LwpListEdit extends ActionBarActivity {

    private LwpGridImageAdapter adapter;
    private GridView gridView;
    ArrayList<ImgLocHolder> img_loc_holder = new ArrayList<ImgLocHolder>();
    private TextView images_count;
    private TextView images_selected_count;
    private int selected_images_count;
    String location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lwp_list_edit);
        setTitle("LWP Images List");
        images_count = (TextView) findViewById(R.id.lwp_list_total_images);
        images_selected_count = (TextView) findViewById(R.id.lwp_list_selected_images);
        context = this.getApplicationContext();
        setGoAdapter(1);
        addListenerOnButton();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_lwp_list_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clean_up:
               startCleanUp();
               return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startCleanUp() {
        ArrayList<String> all_images_loc = new ArrayList<String>();
        ArrayList<String> loc_prefs = new ArrayList<String>();
        loc_prefs.add(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_1);
        loc_prefs.add(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_2);
        loc_prefs.add(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_3);
        loc_prefs.add(IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_4);
        SharedPreferences settings = getSharedPreferences(IntentConfigVariables.LWP_IMGS_LOCATION_PREF, 0);
        Set<String> imgs_locs;
        for (String each_loc_pref : loc_prefs){
            if(android.os.Build.VERSION.SDK_INT >= 11){
                imgs_locs = new TreeSet<String>(settings.getStringSet(each_loc_pref, new TreeSet<String>()));
                all_images_loc.addAll(imgs_locs);
            }else{
                imgs_locs = new TreeSet<String>(Utils.getStringArrayPref(context,each_loc_pref));
                all_images_loc.addAll(imgs_locs);
            }
        }

        File dir_file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+ "/WallpaperDownloads");
        if(!dir_file.exists())
            return;
        File[] all_direcotry_files = dir_file.listFiles();
        ArrayList<String> all_dir_files = new ArrayList<String>();
        for (File each_image:all_direcotry_files){
            all_dir_files.add(each_image.getAbsolutePath());
        }

        for(String each_img_file:all_images_loc){
            try{
                all_dir_files.remove(each_img_file);
            }catch(Exception e){
                Log.d("backpaper2-lwp-edit","Error while trying to keep an object that does not exist "+e.toString());
            }
        }
        int deleteCounter = 0;
//        System.out.println(all_dir_files);
        for(String eachFileLeft:all_dir_files){
            try {
                File f = new File(eachFileLeft);
                if(!f.getName().equals(".nomedia")){
                    if(f.delete()){
                        deleteCounter = deleteCounter + 1;
                    }else throw new NullPointerException("not able to delete");
                }
            }catch (Exception e){
                Log.d("backpaper2-lwp-edit","Error while trying to delete an object that just existed "+e.toString());
            }
        }
        Toast mToast = Toast.makeText(this.getApplicationContext(), "Deleted "+deleteCounter+" images that are being unused" , Toast.LENGTH_LONG );
        mToast.show();
    }

    private void setGoAdapter(int i) {
        location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR;
        switch (i) {
            case 1:
                location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_1;
                break;
            case 2:
                location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_2;
                break;
            case 3:
                location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_3;
                break;
            case 4:
                location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_4;
                break;
            default:
                location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR;
                break;
        }
//		System.out.println(i);
        ArrayList<ImgLocHolder> this_img_loc_holder;
        if(android.os.Build.VERSION.SDK_INT >= 11){
            SharedPreferences settings = getSharedPreferences(IntentConfigVariables.LWP_IMGS_LOCATION_PREF, 0);
            Set<String> imgs_locs = new TreeSet<String>(settings.getStringSet(location_pref_var, new TreeSet<String>()));
            ArrayList<String> all_imgs_locs = new ArrayList<String>();
            all_imgs_locs.addAll(imgs_locs);
            this_img_loc_holder = setImagesHolding(imgs_locs);
        }else{
            Set<String> imgs_locs = new TreeSet<String>(Utils.getStringArrayPref(context,location_pref_var));
            Log.d("backpaper2-lwp",imgs_locs.toString());
            ArrayList<String> all_imgs_locs = new ArrayList<String>();
            all_imgs_locs.addAll(imgs_locs);
            this_img_loc_holder = setImagesHolding(imgs_locs);
        }
        selected_images_count = 0;
        images_selected_count.setText("Selected Images : " + selected_images_count);
        goAdapter(this_img_loc_holder);
    }

    private void goAdapter(ArrayList<ImgLocHolder> this_img_loc_holder) {

        try {
            adapter = new LwpGridImageAdapter(this.getApplicationContext(), R.layout.resource_lwp_list_adapter_1, img_loc_holder);
            gridView = (GridView) findViewById(R.id.lwp_list_main_gridview1);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(mMessageClickedHandler);
            gridView.setFastScrollEnabled(true);
            if (android.os.Build.VERSION.SDK_INT >= 13) {
                gridView.setFastScrollAlwaysVisible(true);
            }
            if (img_loc_holder.size() > 50) {
                images_count.setText("Total Images : " + img_loc_holder.size() + " too many images app may crash");
            } else {
                images_count.setText("Total Images : " + img_loc_holder.size());
            }
        } catch (Exception e) {
            Log.d("backpaper2", e.toString());
        }
    }

    private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> parents, View arg1, int arg2, long arg3) {
            if (!img_loc_holder.get(arg2).getState()) {
                img_loc_holder.get(arg2).setState(true);
                selected_images_count = selected_images_count + 1;
                images_selected_count.setText("Selected Images : " + selected_images_count);
            } else {
                img_loc_holder.get(arg2).setState(false);
                selected_images_count = selected_images_count - 1;
                images_selected_count.setText("Selected Images : " + selected_images_count);
            }
            adapter.notifyDataSetChanged();
        }
    };

    private ArrayList<ImgLocHolder> setImagesHolding(Set<String> imgs_locs) {
        img_loc_holder.clear();
        try {
            for (String img_loc : imgs_locs) {
                Log.d("backpaper2-lwplistedit",img_loc);
                ImgLocHolder this_img_loc_holder = new ImgLocHolder();
                this_img_loc_holder.setImgLoc(img_loc);
                this_img_loc_holder.setState(false);
                img_loc_holder.add(this_img_loc_holder);
            }
        } catch (Exception e) {
            Log.d("backpaper2", "error while setting imgaesholding " + e.toString());
        }
        return img_loc_holder;
    }

    @SuppressLint("ValidFragment")
	public void addListenerOnButton() {
        Button button = (Button) findViewById(R.id.lwp_list_clear_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                class SelectListDialogFragmentExtended extends SelectListDialogFragment {
                    @Override
                    protected void setList(int i) {
                        selected_images_count = 0;
                        images_selected_count.setText("Selected Images : " + selected_images_count);
                        Set<String> new_imgs_locs = new TreeSet<String>();
                        ArrayList<ImgLocHolder> new_img_loc_holder = new ArrayList<ImgLocHolder>();
                        for (ImgLocHolder this_img_loc_holder : img_loc_holder) {
                            if (!this_img_loc_holder.getState()) {
                                new_imgs_locs.add(this_img_loc_holder.getImgLoc());
                                new_img_loc_holder.add(this_img_loc_holder);
                            } else {
                                if (i == 1) {
                                    Log.d("backpaper2-lwp-clear", this_img_loc_holder.getImgLoc());
                                    try {
                                        File f = new File(this_img_loc_holder.getImgLoc());
                                        boolean fStatus = f.delete();
                                        Log.d("backpaper2-lwp-clear", String.valueOf(fStatus));
                                    } catch (Exception e) {
                                        Log.d("backpaper2-Lwp-clear", this_img_loc_holder.getImgLoc() + " - " + e.toString());
                                    }
                                }
                            }
                        }
                        img_loc_holder = new_img_loc_holder;
                        if(android.os.Build.VERSION.SDK_INT >= 11){
                            SharedPreferences settings = getSharedPreferences(IntentConfigVariables.LWP_IMGS_LOCATION_PREF, 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putStringSet(location_pref_var, new_imgs_locs);
                            editor.commit();
                        }else{
                            Utils.setStringArrayPref(context,location_pref_var,new ArrayList<String>(new_imgs_locs));
                        }
                        adapter.clear();
                        goAdapter(img_loc_holder);
                    }
                }
                android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                SelectListDialogFragmentExtended sdf = new SelectListDialogFragmentExtended();
                sdf.show(fm, "Delete Files Also ?");
            }
        });

        Button button5 = (Button) findViewById(R.id.lwp_list_clear_all_button);
        button5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                class SelectListDialogFragmentExtended extends SelectListDialogFragment {
                    @Override
                    protected void setList(int i) {
                        selected_images_count = 0;
                        images_selected_count.setText("Selected Images : " + selected_images_count);
                        Set<String> new_imgs_locs = new TreeSet<String>();
                        ArrayList<ImgLocHolder> new_img_loc_holder = new ArrayList<ImgLocHolder>();
                        if(i==1){
                            for (ImgLocHolder this_img_loc_holder : img_loc_holder) {
                                try {
                                    File f = new File(this_img_loc_holder.getImgLoc());
                                    boolean fStatus = f.delete();
                                    Log.d("backpaper2-lwp-clear", String.valueOf(fStatus));
                                } catch (Exception e) {
                                    Log.d("backpaper2-Lwp-clear", this_img_loc_holder.getImgLoc() + " - " + e.toString());
                                }
                            }
                        }
                        img_loc_holder = new_img_loc_holder;
                        if(android.os.Build.VERSION.SDK_INT >= 11){
                            SharedPreferences settings = getSharedPreferences(IntentConfigVariables.LWP_IMGS_LOCATION_PREF, 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putStringSet(location_pref_var, new_imgs_locs);
                            editor.commit();
                        }else{
                            Utils.setStringArrayPref(context,location_pref_var,new ArrayList<String>(new_imgs_locs));
                        }
                        adapter.clear();
                        goAdapter(img_loc_holder);
                    }
                }
                android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                SelectListDialogFragmentExtended sdf = new SelectListDialogFragmentExtended();
                sdf.show(fm, "Delete Files Also ? (be careful if same image is on multiple lists it clashes ");
            }
        });


        Button button1 = (Button) findViewById(R.id.lwp_list_button1);
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setGoAdapter(1);
            }
        });

        Button button2 = (Button) findViewById(R.id.lwp_list_button2);
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setGoAdapter(2);
            }
        });

        Button button3 = (Button) findViewById(R.id.lwp_list_button3);
        button3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setGoAdapter(3);
            }
        });

        Button button4 = (Button) findViewById(R.id.lwp_list_button4);
        button4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setGoAdapter(4);
            }
        });
    }

    // DIALOG FOR LWP LIST SELECTIN ITEMS
    public static class SelectListDialogFragment extends android.support.v4.app.DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Delete Files Also ?");
            String[] items = {"Yes", "No"};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    setList(which + 1);
                }
            });
            return builder.create();
        }

        protected void setList(int i) {

        }
    }
}


