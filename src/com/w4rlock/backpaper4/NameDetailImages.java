package com.w4rlock.backpaper4;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.searchboxsdk.android.StartAppSearch;
import com.startapp.android.publish.StartAppAd;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.GridImageAdapter2;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesUtils.parseResultNameImages;
import com.w4rlock.backpaper2.Logger.GTracker;
import com.w4rlock.backpaper2.configs.GetDisplayParams;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.models.NameImage;
import com.w4rlock.backpaper2.models.NameImages;
import com.w4rlock.backpaper2.utils.Connectivity;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.cacheManager.CacheManager;
import com.w4rlock.cacheManager.getURLdata;

import java.io.IOException;
import java.util.ArrayList;

public class NameDetailImages extends ActionBarActivity {
	public static final ServerVariables server_variable = new ServerVariables();
	private static String url;
	private CacheManager cm;
	private GridImageAdapter2 adapter=null;
	private GridView gridView;
	private Boolean refresh = false;
	private Toast mToast=null;
//	private ArrayList<String> image_urls=null;
	private NameImages name_images = null;
	private Name given_name=null;

    private Context context;

    private Boolean auto_updated=false;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_name_detail_images);
		if (android.os.Build.VERSION.SDK_INT >= 13) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}else{
        	try {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }catch (NullPointerException e){
                Log.d("backpaper2","unable to set nav home up");
            }
        }
		cm = new CacheManager(this);
        context = this.getApplicationContext();
		Intent intent = getIntent();
		given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameDetailIntentMessage());
        String server_url;
        if(given_name.getId() == -1){  // if recent images initiated through notifications panel
            server_url = server_variable.getNameImagesUrlWithParams(GetDisplayParams.getFullDisplayAsQueryParams(this));
            url = new String(server_url);
            refresh = true;
            goOnNetwork(server_url);
        }else if(given_name.getId() == -3){
            server_url = server_variable.getNameImagesPopularUrlWithParams(GetDisplayParams.getFullDisplayAsQueryParams(this));
            url = new String(server_url);
            refresh = true;
            goOnNetwork(server_url);
        }
        else{
            server_url = server_variable.getNameAllImageUrlWithParams((given_name.getId()), GetDisplayParams.getFullDisplayAsQueryParams(this));
            url = new String(server_url);
            if(cm.checkFileFromUrl(server_url)){
                goOnFile(server_url);
                displayToast("Displaying Cached Data. Click Refresh in Menu Button To Load Latest Data");
            }else{
                goOnNetwork(server_url);
            }
        }
		Log.d("backpaper2",given_name.getName());
		if(android.os.Build.VERSION.SDK_INT >= 13){
			getActionBar().setTitle(given_name.getName());
		}else{
			getSupportActionBar().setTitle(given_name.getName());
		}
        displayAdds();
	}
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onStart() {
        super.onStart();
//        EasyTracker.getInstance().activityStart(this); // Add this method.
        GTracker.trackActivity(context,"NameDetailImages");
    }

    @Override
    public void onStop() {
        super.onStop();
//        EasyTracker.getInstance().activityStop(this); // Add this method.
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_name_detail_images, menu);
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh_data:
			loading_data = false;
			load_data = false;
			refresh = true;
			reloadDataFromNetwork();
			return true;
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void reloadDataFromNetwork() {
        View loadingBar = (View)findViewById(R.id.progress_large);
		if ( loadingBar.getVisibility() == View.VISIBLE){
			displayToast("Please Wait While Data is Loading");
		}else{
			goOnNetwork(url);
		}
	}
	
	private void goOnFile(String server_url) {
		try{
			String result2 = cm.readFromUrl(server_url);
			parseresult(result2);
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            if(!auto_updated && Connectivity.isConnectedWifi(context) && sharedPrefs.getBoolean("auto_download",true) ){
                reloadDataFromNetwork();
                auto_updated = true;
                refresh = true;
            }
		}catch(Exception e){
			Log.d("backpaper2","error while reading file "+e.toString());
			goOnNetwork(server_url);
		}
	}

	private Boolean goOnNetwork(final String url) {
		onDataLoading();
		if(!NetworkChecker.getNetStatus(this)){
			displayError("No Network Or Cache");
			return false;
		}
		class GetData extends getURLdata{
            GetData(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
			@Override
			public void postresult(Object result2){
				try {
					cm.addtoCacheFile((String)result2, url);
				} catch (IOException e) {
					Log.d("backpaper2","unable to write to cache file "+e.toString());
				}
				parseresult(((String)result2));
			}
			@Override
			public void onError(Object result){
				Log.d("backpaper2","Error - goOnNetwork");
				displayToast( "Error while loading data");
                displayError("Error while Loading Data .. Try Reloading Data");
			}
			@Override
			public void onTimeOut(Object result){
				displayToast("Time Out..");
                displayError("Time Out.. Try Reloading data");
			}
		}
		GetData getdata = new GetData();
		getdata.getData(url);
		return true;
	}
	
	private void displayError(String string) {
		TextView loading_text = (TextView)findViewById(R.id.loading);
		loading_text.setText(string);
		loading_text.setVisibility(View.VISIBLE);
        hideLoadingBar();
	}
	

	private void parseresult(String result2) {
		parseResultNameImages presult = new parseResultNameImages(this.getApplicationContext());
		try {
			name_images = presult.getObjectsArray(result2);
			goAdapter(name_images.getImages());
		} catch (Exception e) {
			Log.d("backpaper2","1"+e.toString());
            displayError(e.toString());
		}
	}

    private void goAdapter(ArrayList<NameImage> name_images) {
	//	String[] name_images_array = name_images.toArray(new String[name_images.size()]);		
    	try{
    		adapter = new GridImageAdapter2(this, R.layout.resource_name_detail_images_adapter,name_images);
    		gridView = (GridView)findViewById(R.id.name_images_gridview1);    	
    		gridView.setAdapter(adapter);
    		gridView.setOnItemClickListener(mMessageClickedHandler);
			gridView.setOnScrollListener(mScrollListener);
    	}catch (Exception e){
    		Log.d("backpaper2","2"+e.toString());
    	}
    	displayLoaders(false);
	}
	
	private void hideLoadingBar(){
        View loadingbar = (View)findViewById(R.id.progress_large);
        loadingbar.setVisibility(View.GONE);
    }
	private void displayLoaders(boolean b) {
		if(b){
			try{
	    		TextView loading = (TextView)findViewById(R.id.loading);
	        	loading.setVisibility(View.VISIBLE);
	        	
	    		View loadingbar = (View)findViewById(R.id.progress_large);
	    		loadingbar.setVisibility(View.VISIBLE);
	    		
	    	}catch(Exception e){
	    		Log.d("backpaper2",e.toString());
	    	}
		}else{
			try{
	    		TextView loading = (TextView)findViewById(R.id.loading);
	        	loading.setVisibility(View.GONE);
	        	
	    		View loadingbar = (View)findViewById(R.id.progress_large);
	    		loadingbar.setVisibility(View.GONE);
	    		
	    	}catch(Exception e){
	    		Log.d("backpaper2",e.toString());
	    	}
		}
	}
	
	private void displayToast(String msg){
		try{
			mToast.setText(msg);
			mToast.show();
		}catch(Exception e){
			mToast = Toast.makeText( this  , msg , Toast.LENGTH_SHORT );
			mToast.show();
		}
	}

	/// loading extra data func
	
////next result set
	
	private void goOnFileNext(String server_url,NameImages name_images) {
		Log.d("backpaper2","refresh variable value - "+refresh.toString());
		try{
			if(refresh){
				goOnNetworkNext(server_url, name_images);
			}else{
				String result2 = cm.readFromUrl(server_url);
				parseresultNext(result2, name_images);
			}
		}catch(Exception e){
			Log.d("backpaper2","error while reading file "+e.toString());
			goOnNetworkNext(server_url, name_images);
		}
	}
	
	private Boolean goOnNetworkNext(final String url,final NameImages name_images) {
		onDataLoading();
		if(!NetworkChecker.getNetStatus(this)){
			displayError("No Network Or Cache");
			onDataLoaded();
			return false;
		}
		class GetData extends getURLdata{
            GetData(){
                setReadTimeOut(60000);
                setConnTimeOut(60000);
            }
			@Override
			public void postresult(Object result2){
				try {
					cm.addtoCacheFile((String)result2, url);
				} catch (IOException e) {
					Log.d("backpaper2","unable to write to cache file "+e.toString());
				}
				parseresultNext(((String)result2),name_images);
			}
			@Override
			public void onError(Object result){
				displayToast("Error while loading data");
                displayError("Error while Loading Data");
			}
			@Override
			public void onTimeOut(Object result){
				displayToast("Time Out..");
                displayError("Network Time Out...Try Reloading ");
			}
		}
		GetData getdata = new GetData();
		getdata.getData(url);
		return true;
	}
	public void parseresultNext(String string,NameImages name_images) {
		parseResultNameImages presult = new parseResultNameImages(this.getApplicationContext());
		try {
			NameImages name_images_new = presult.getObjectsArrayNext(name_images, string); 
			ArrayList<NameImage> new_name_images = new ArrayList<NameImage>(name_images.getImages());
			goAdapterNext(name_images_new.getImages());
			for(NameImage this_name_images_new : name_images_new.getImages()){
				new_name_images.add(this_name_images_new);
			}
			name_images.setImages(new_name_images);
			try{
				name_images.setNext(name_images_new.getNext());
			}catch(Exception e){
				name_images.setNext(null);
			}
			onDataLoaded();
		} catch (Exception e) {
			Log.d("backpaper2","1"+e.toString());
            displayError(e.toString());
		}
	}
	
	private void goAdapterNext(ArrayList<NameImage> name_images_list) {
		for(NameImage this_name_image : name_images_list){
//			System.out.println(this_name_image.getId());
			adapter.add(this_name_image);
		}
	}
	
	private void onDataLoaded(){
		loading_data = false;
		load_data = false;
		displayLoaders(false);
	}
	
	private void onDataLoading(){
		displayLoaders(true);
	}
	// next result set functions ends here
	private Boolean loading_data = false;
	private Boolean load_data = false;
	private OnScrollListener mScrollListener = new OnScrollListener() {
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			if(load_data && !loading_data){
				attachLoaderToGrid();
				loading_data = true;
			}
		}
		
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			if(!loading_data){
				if(firstVisibleItem + visibleItemCount == totalItemCount){
					load_data = true;
				}else{
//					load_data = false;
				}
			}
		}
	};
	
	protected void attachLoaderToGrid() {
		if(name_images.getNext() != null){
			displayLoaders(true);
			goOnFileNext(name_images.getNext(),name_images);
		}else{
			displayToast( "All data loaded.");
			Log.d("backpaper2","no next item");
		}
	}
	
    private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,long arg3) {
			Intent intent = new Intent(parents.getContext(),ImageNav.class);
			NameImage selected_name_image = (NameImage)parents.getAdapter().getItem(arg2);
			Log.d("backpaper2","selected image id "+Integer.valueOf(selected_name_image.getId()).toString());
            GTracker.trackEvent(context,"NameDetailImages","view_full_image",selected_name_image.getResourceUri(),new Long(selected_name_image.getId()));
			intent.putExtra(IntentConfigVariables.getNameImgNavMessageImages(),selected_name_image);
			intent.putExtra(IntentConfigVariables.getNameImgNavMessageName(), given_name);
			startActivity(intent);
		}
    };

    private void displayAdds(){
        SharedPreferences settings = getSharedPreferences(IntentConfigVariables.ADD_MOB_STATUS_PREF, 0);
        if(settings.getBoolean(IntentConfigVariables.ADDMOB_STATUS_VAR,false)){
            try{
            StartAppAd.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);
            StartAppSearch.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);

            StartAppSearch.showSearchBox(this);
            }catch (Exception e){
                Log.d("backpaper2-startapp","failed to load startapp"+e.toString());
            }
//            AdView adView = new AdView(this, AdSize.BANNER,"4648b5bd0ef8426a");
//            LinearLayout main_layout = (LinearLayout)findViewById(R.id.main_layout_2);
//            AdRequest adRequest = new AdRequest();
////            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
////            adRequest.addTestDevice("410029197ddfc00");
//            main_layout.addView(adView);
//            adView.loadAd(adRequest);

        }
    }
}
