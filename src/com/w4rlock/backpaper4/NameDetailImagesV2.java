package com.w4rlock.backpaper4;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;


import com.searchboxsdk.android.StartAppSearch;
import com.startapp.android.publish.StartAppAd;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.CommentsFragment;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.ImagesFragment;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.PostCommentFragment;
import com.w4rlock.backpaper2.ActivityUtils.NameDetailImagesV2Utils.WikiPediaInfoFragment;
import com.w4rlock.backpaper2.Logger.GTracker;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Comments;
import com.w4rlock.backpaper2.models.Name;

/**
 * Created by krishna on 18/7/13.
 */



public class NameDetailImagesV2 extends ActionBarActivity implements PostCommentFragment.OnPostButtonClickedListner {
    private Name given_name=null;
    private static final int NUM_PAGES = 3;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    public CommentsFragment comments_fragment = null;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_detail_images_v2);
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Intent intent = getIntent();
        given_name = (Name) intent.getSerializableExtra(IntentConfigVariables.getNameDetailIntentMessage());
        mPager = (ViewPager)findViewById(R.id.name_detail_images_v2_pager);
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        
        if(android.os.Build.VERSION.SDK_INT >= 13){
			getActionBar().setTitle(given_name.getName());
		}else{
			getSupportActionBar().setTitle(given_name.getName());
		}
        displayAdds();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onStart() {
        super.onStart();
//        EasyTracker.getInstance().activityStart(this); // Add this method.
        GTracker.trackActivity(this.getApplicationContext(), "NameDetailImages");
    }

    @Override
    public void onStop() {
        super.onStop();
//        EasyTracker.getInstance().activityStop(this); // Add this method.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_name_detail_images_v2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.detail_images:
                mPager.setCurrentItem(0);
                return true;
            case R.id.comments:
                mPager.setCurrentItem(1);
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.wikipedia:
                mPager.setCurrentItem(2);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostButtonClicked(Comments comment) {
        comments_fragment.addNewComment(comment);
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return new ImagesFragment();
            }if(position==1){
                comments_fragment = new CommentsFragment();
                return comments_fragment;
            }else{
                WikiPediaInfoFragment wikiPediaInfoFragment = new WikiPediaInfoFragment();
                return wikiPediaInfoFragment;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

    }

    private void displayAdds(){
        SharedPreferences settings = this.getSharedPreferences(IntentConfigVariables.ADD_MOB_STATUS_PREF, 0);
        if(settings.getBoolean(IntentConfigVariables.ADDMOB_STATUS_VAR,false)){
            try{
            StartAppAd.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);
            StartAppSearch.init(this, ServerVariables.startAppDEVID, ServerVariables.startAppAPPID);
            StartAppSearch.showSearchBox(this);
            }catch (Exception e){
                Log.d("backpaper2-startapp","failed to load startapp"+e.toString());
            }
//            AdView adView = new AdView(this, AdSize.BANNER,"4648b5bd0ef8426a");
//            LinearLayout main_layout = (LinearLayout)findViewById(R.id.main_layout_2);
//            AdRequest adRequest = new AdRequest();
////            adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
////            adRequest.addTestDevice("410029197ddfc00");
//            main_layout.addView(adView);
//            adView.loadAd(adRequest);
        }
    }
}
