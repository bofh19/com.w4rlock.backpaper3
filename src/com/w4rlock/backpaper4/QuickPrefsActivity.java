package com.w4rlock.backpaper4;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.gcm.GCMRegisterOnServer;
import com.w4rlock.backpaper2.setdefaultlwp.FileBrowser;

import java.io.File;


public class QuickPrefsActivity extends PreferenceActivity {
    private Context context;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }else{
            addPreferencesFromResource(R.xml.pref);
            Log.d("backpaper2","1");
            CheckBoxPreference notify_pref = (CheckBoxPreference) findPreference("notify_pref");
            notify_pref.setOnPreferenceChangeListener(
                    new Preference.OnPreferenceChangeListener() {
                        @Override
                        public boolean onPreferenceChange(Preference preference, Object newValue) {
                            Boolean is_active = (Boolean) newValue;
                            if (is_active) {
                                GCMRegisterOnServer.registerNotify(context);
                            } else {
                                GCMRegisterOnServer.unregisterNotify(context);
                            }
                            return true;
                        }
                    }
            );
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public class MyPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref);

            final CheckBoxPreference checkboxPref = (CheckBoxPreference) getPreferenceManager().findPreference("notify_pref");
            final Preference selectDefaultWallpaperPref = (Preference)getPreferenceManager().findPreference("select_default_wallpaper");
            selectDefaultWallpaperPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent_settings = new Intent(preference.getContext(), FileBrowser.class);
                    startActivity(intent_settings);
                    return true;
                }
            });
            final Preference resetDefaultWallpaperPref = (Preference)getPreferenceManager().findPreference("reset_default_wallpaper");
            resetDefaultWallpaperPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    SharedPreferences settings = getApplicationContext().getSharedPreferences(IntentConfigVariables.lwpDefaultLocationPref, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(IntentConfigVariables.getLwpDefaultLocationValue,null);
                    editor.commit();
                    Toast.makeText(preference.getContext(), "Reset to Default Android Bag Wallpaper", Toast.LENGTH_LONG).show();
                    return true;
                }
            });
            checkboxPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Log.d("backpaper2-preference-activity", "Pref " + preference.getKey() + " changed to " + newValue.toString());
                    runCheckBoxPreference(newValue);
                    return true;
                }
            });

            final CheckBoxPreference galleryImageHidePref = (CheckBoxPreference)getPreferenceManager().findPreference("gallery_images_hide");
            galleryImageHidePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Boolean newVal = (Boolean)newValue;
                    File dir_file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+ "/WallpaperDownloads");
                    try{
                        if(!dir_file.exists())
                            dir_file.mkdir();
                        File noMediaFile = new File(dir_file,".nomedia");
                        if(newVal){
                            if(!noMediaFile.exists())
                                noMediaFile.createNewFile();
                            Toast.makeText(context,"Images are now hidden open gallery and wait for few seconds so gallery cache is refreshed",Toast.LENGTH_SHORT);
                        }else{
                            if(noMediaFile.exists())
                                noMediaFile.delete();
                        }
                    }
                    catch(Exception e){
                        Toast.makeText(context,"Something has crashed unable to perform action requested",Toast.LENGTH_SHORT);
                    }
                    return true;
                }
            });
        }

        private void runCheckBoxPreference(Object newValue) {
            Boolean is_active = (Boolean) newValue;
            if (is_active) {
                GCMRegisterOnServer.registerNotify(context);
            } else {
                GCMRegisterOnServer.unregisterNotify(context);
            }
        }
    }

}

