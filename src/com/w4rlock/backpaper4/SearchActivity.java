package com.w4rlock.backpaper4;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.w4rlock.backpaper2.ActivityUtils.BackPaperMainUtils.parseResultNames;
import com.w4rlock.backpaper2.Logger.GTracker;
import com.w4rlock.backpaper2.configs.GetDisplayParams;
import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.configs.ServerVariables;
import com.w4rlock.backpaper2.models.Name;
import com.w4rlock.backpaper2.searchActivityUtils.SearchAdapter;
import com.w4rlock.backpaper2.utils.NetworkChecker;
import com.w4rlock.cacheManager.getURLdata;

public class SearchActivity extends ActionBarActivity {
	public Toast loadingtoast;
	private Toast mToast = null;
	private SearchAdapter adapter;
	ListView ls;
	final ArrayList<Name> all_names = new ArrayList<Name>();

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_activity_list_view);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View view = mInflater.inflate(R.xml.search_loading_dialog, null);
		loadingtoast = new Toast(this);
		loadingtoast.setView(view);
		loadingtoast.setDuration(Toast.LENGTH_LONG);

		ls = (ListView) findViewById(R.id.search_activity_list_view);
		ls.setBackgroundResource(R.color.back_main);
		ls.setDividerHeight(5);
		ls.setPadding(5, 5, 5, 5);

		adapter = new SearchAdapter(this, R.xml.search_result_adapter,
				all_names);
		ls.setAdapter(adapter);
		if (android.os.Build.VERSION.SDK_INT >= 13) {
			try {
				getActionBar().setDisplayHomeAsUpEnabled(true);
			} catch (NullPointerException e) {
				Log.d("backpaper2", "unable to set nav home up");
			}
		} else {
			try {
				getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			} catch (NullPointerException e) {
				Log.d("backpaper2", "unable to set nav home up");
			}
		}
		ls.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Name selected_name = (Name) arg0.getAdapter().getItem(position);
				Intent intent = new Intent(arg0.getContext(),
						NameDetailImagesV2.class);
				intent.putExtra(
						IntentConfigVariables.getNameDetailIntentMessage(),
						selected_name);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	public void onStart() {
		super.onStart();
		GTracker.trackActivity(getApplicationContext(),"NameDetailImages");
	}

	@Override
	public void onStop() {
		super.onStop();
		
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.search_activity_compact, menu);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		MenuItem searchItem = menu.findItem(R.id.grid_default_search);
		final android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) MenuItemCompat
				.getActionView(searchItem);
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(false); // Do not iconify the widget;
													// expand it by default
		searchView
				.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {

					@Override
					public boolean onQueryTextSubmit(String query) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(
								searchView.getWindowToken(), 0);
						doSearch(query);
						return true;
					}

					@Override
					public boolean onQueryTextChange(String arg0) {
						return false;
					}
				});
		searchView.requestFocus();
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	private Boolean doSearch(String queryStr) {

		if (!NetworkChecker.getNetStatus(this.getApplicationContext())) {
			displayToast("Error No Network");
			return false;
		}
		loadingtoast.show();
		String server_url = ServerVariables.getNameSearchUrlWithParamsNoLimit(
				GetDisplayParams.getFullDisplayAsQueryParams(this
						.getApplicationContext()), queryStr);
		class getData extends getURLdata {
			@Override
			public void postresult(Object result2) {
				parseresults(result2);
			}
		}
		getData getdata = new getData();
		getdata.getData(server_url);
		return true;
	}

	public void parseresults(Object result2) {
		parseResultNames presult = new parseResultNames();
		try {
			ArrayList<Name> all_names = presult.getObjectsArray(result2);
			goAdapter(all_names);
		} catch (Exception e) {
			Log.d("backpaper2", e.toString());
		}
	}

	private void goAdapter(ArrayList<Name> all_names) {
		try {
			this.all_names.clear();
			adapter.notifyDataSetChanged();
			this.all_names.addAll(all_names);
			adapter.notifyDataSetChanged();
			loadingtoast.cancel();
		} catch (Exception e) {
			Log.d("backpaper2", "Error goadapter " + e.toString());
		}
	}

	private void displayToast(String msg) {
		try {
			mToast.setText(msg);
			mToast.show();
		} catch (Exception e) {
			mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
			mToast.show();
		}
	}
}
