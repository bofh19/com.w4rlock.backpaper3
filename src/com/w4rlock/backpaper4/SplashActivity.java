package com.w4rlock.backpaper4;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.GoogleAnalytics;
import com.w4rlock.backpaper2.gcm.GCMRegisterOnServer;
import com.w4rlock.backpaper2.utils.AppRater;
import com.w4rlock.cacheManager.CacheManager;

/**
 * Created by saikrishna on 11/17/13.
 */
public class SplashActivity extends Activity {
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = getApplicationContext();
        if(Build.VERSION.SDK_INT >= 14){
            Intent backPaper2 = new Intent(getApplicationContext(), BackPaperMain2.class);
            startActivity(backPaper2);
        }else{
            Intent backPaper = new Intent(getApplicationContext(), BackPaperMain.class);
            startActivity(backPaper);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_activity);
        TextView txt_above = (TextView)findViewById(R.id.spalsh_activity_version_14_and_above);
        TextView txt_below = (TextView)findViewById(R.id.spalsh_activity_version_14_below);
        txt_above.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT >= 14){
                    Intent backPaper2 = new Intent(getApplicationContext(), BackPaperMain2.class);
                    startActivity(backPaper2);
                }else{
                    Toast.makeText(context,"Your Version is below ICS, App Might Crash if you try to do that",Toast.LENGTH_LONG);
                    Intent backPaper = new Intent(getApplicationContext(), BackPaperMain.class);
                    startActivity(backPaper);
                }
            }
        });
        txt_below.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT >= 14){
                    Intent backPaper2 = new Intent(getApplicationContext(), BackPaperMain2.class);
                    startActivity(backPaper2);
                }else{
                    Intent backPaper = new Intent(getApplicationContext(), BackPaperMain.class);
                    startActivity(backPaper);
                }
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();

    }

    @Override
    public void onResume(){
        super.onResume();
    }
}



