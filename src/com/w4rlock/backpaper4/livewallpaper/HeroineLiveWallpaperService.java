package com.w4rlock.backpaper4.livewallpaper;

import java.io.File;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import com.w4rlock.backpaper2.configs.IntentConfigVariables;
import com.w4rlock.backpaper2.livewallpaper.utils.ScalingUtilities;
import com.w4rlock.backpaper2.livewallpaper.utils.ScalingUtilities.ScalingLogic;
import com.w4rlock.backpaper2.utils.Utils;
import com.w4rlock.backpaper4.R;


@SuppressLint("NewApi")
public class HeroineLiveWallpaperService extends WallpaperService {
	public static final String PREFS_NAME = "com.w4rlock.backpaper2.livewallpaper.sprefs";
    public static final String DEBUG_TAG = "backpaper2-wallpaper-server-touch";
	SharedPreferences sharedPrefs;
    private Context context;
	@Override
	public Engine onCreateEngine() {
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		context = this.getApplicationContext();
		return new HeroineWallpaperEngine();
	}

	private class HeroineWallpaperEngine extends Engine {
		private boolean mVisible = false;
		private final Handler mHandler = new Handler();
		private final Runnable mUpdateDisplay = new Runnable() {
            @Override
			public void run() {
                setTouchEventsEnabled(true);
                if(android.os.Build.VERSION.SDK_INT >= 15){
				    setOffsetNotificationsEnabled(true);
                }
				draw();
			}
		};

		@Override
		public void onVisibilityChanged(boolean visible) {
			mVisible = visible;
			if (visible) {
				if(sharedPrefs.getBoolean("enable_auto_updates", true)){
				draw();
				}
			} else {
				mHandler.removeCallbacks(mUpdateDisplay);
			}
		}

		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
				int width, int height) {
			draw();
		}

		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder) {
			super.onSurfaceDestroyed(holder);
			mVisible = false;
			mHandler.removeCallbacks(mUpdateDisplay);
		}

		@Override
		public void onDestroy() {
			super.onDestroy();
			mVisible = false;
			mHandler.removeCallbacks(mUpdateDisplay);
		}
//		------
		
		static final String logTag = "backpaper2-ActivitySwipeDetector";
		private static final int MIN_DISTANCE_LONG = 200;
		private static final int MIN_DISTANCE_SHORT = 25;
		private float downX, downY, upX, upY;
		
		DisplayMetrics dm = getResources().getDisplayMetrics();
		int REL_SWIPE_MIN_DISTANCE_LONG = (int)(MIN_DISTANCE_LONG * dm.densityDpi / 160.0f);
		int REL_SWIPE_MIN_DISTANCE_SHORT = (int)(MIN_DISTANCE_SHORT * dm.densityDpi / 160.0f);
	
		private int MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_SHORT;
//		---------


        @Override
        public void setTouchEventsEnabled(boolean enabled) {
            super.setTouchEventsEnabled(enabled);
        }

        @Override
		public void onTouchEvent(MotionEvent event){
			Log.d("backpaper2-wallpaper-server-touch","x: "+event.getX()+" y: "+event.getY());
			switch(event.getAction()){
	        case MotionEvent.ACTION_DOWN: {
	            downX = event.getX();
	            downY = event.getY();
	            return;
	        }
	        case MotionEvent.ACTION_UP: {
	         
	        	upX = event.getX();
	            upY = event.getY();

	            float deltaX = downX - upX;
	            float deltaY = downY - upY;

	            // swipe horizontal?
	            if(Math.abs(deltaX) > MIN_DISTANCE){
	                // left or right
	                if(deltaX < 0) { 
	                	onLeftToRightSwipe(); 
//	                	return;
	                	}
	                if(deltaX > 0) { 
	                	onRightToLeftSwipe(); 
//	                	return; 
	                	}
	            }
	            else {
	                    Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
//	                    return; // We don't consume the event
	            }

	            // swipe vertical?
	            if(onBottomTop){
	            	MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_LONG;
	            }else{
	            	MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_SHORT;
	            }
	            if(Math.abs(deltaY) > MIN_DISTANCE){
	                // top or down
	                if(deltaY < 0) {  
	                	onTopToBottomSwipe(deltaY); 
	                	return; 
	                	}
	                if(deltaY > 0) {  
	                	onBottomToTopSwipe(deltaY); 
	                	return; }
	            }
	            else {
	                    Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
	                    return; // We don't consume the event
	            }

	            return ;
	        }
	    
	    }
		}
		
		private void onLeftToRightSwipe() {
			Log.d("backpaper2-lwp","on left to right");
//			mHandler.removeCallbacks(mUpdateDisplay);
//			draw();
			
		}
			
		private void onRightToLeftSwipe() {
			Log.d("backpaper2-lwp","on right to left");
//			mHandler.removeCallbacks(mUpdateDisplay);
//			draw();
		}
		
//		System.out.println(sharedPrefs.getBoolean("swipe_change", true));
//		System.out.println(sharedPrefs.getBoolean("secret_swipe", true));
//		System.out.println(Integer.parseInt(sharedPrefs.getString("updates_interval", "10"))*1000);

		private static final int SWIPE_TOP_TO_BOTTOM = 01100010;
		private static final int SWIPE_BOTTOM_TO_TOP = 01100001;
		private Boolean onBottomTop = false;
		private int swipeDirection = 0;
		private void onBottomToTopSwipe(float deltaY) {
			swipeDirection = SWIPE_BOTTOM_TO_TOP;
			if(sharedPrefs.getBoolean("swipe_change", true)){
				if(sharedPrefs.getBoolean("secret_swipe", true)){
					Log.d("backpaper2-lwp","on bottom to top");
					if(deltaY >= REL_SWIPE_MIN_DISTANCE_LONG){
						Log.d("backpaper2-lwp","on bottom to top LONG");
						mHandler.removeCallbacks(mUpdateDisplay);
						onBottomTop = true;
						draw();
						mHandler.removeCallbacks(mUpdateDisplay);
					}else{
						onBottomTop = false;
						draw();	
					}
				}else{
					onBottomTop = false;
					draw();	
				}
			}
		}

		private void onTopToBottomSwipe(float deltaY) {
			swipeDirection = SWIPE_TOP_TO_BOTTOM;
			if(sharedPrefs.getBoolean("swipe_change", true)){
				Log.d("backpaper2-lwp","on top to bottom");
				onBottomTop = false;
				draw();
			}
		}
		
		
//		@Override
//		public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset){
//			super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep,xPixelOffset, yPixelOffset);
//			Log.d("backpaper2-wallpaper-server-touch","xOffset "+ xOffset+" yOffset "+yOffset+" xOffsetStep "+xOffsetStep+" yOffsetStep "+yOffsetStep+" xPixelOffset "+" xPixelOffset "+xPixelOffset+" xPixelOffset "+yPixelOffset);
//			mHandler.removeCallbacks(mUpdateDisplay);
//			draw();
//		}
		
		private String getList(int i) {
			String location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR;
	        	switch(i){
	        	case 1:
	        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_1;
	        		return location_pref_var;
	        	case 2:
	        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_2;
	        		return location_pref_var;
	        	case 3:
	        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_3;
	        		return location_pref_var;
	        	case 4:
	        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR_L_4;
	        		return location_pref_var;
	        	default:
	        		location_pref_var = IntentConfigVariables.LWP_IMGS_LOCATION_VAR;
	        		return location_pref_var;
	        	}
			}
		
		private  int mDstHeight;
		private  int mDstWidth;

		@SuppressLint("NewApi")
		private void draw() {
//			if(android.os.Build.VERSION.SDK_INT>=13){
				SurfaceHolder holder = getSurfaceHolder();
				Canvas c = null;
				Random rand = new Random();
                Set<String> imgs_locs;
                String location_pref_var = getList(Integer.parseInt(sharedPrefs.getString("lwp_images_list", "1")));
                if(android.os.Build.VERSION.SDK_INT>=13){
                    SharedPreferences settings = getSharedPreferences(IntentConfigVariables.LWP_IMGS_LOCATION_PREF,0);
                    imgs_locs = new TreeSet<String>(settings.getStringSet(location_pref_var,new HashSet<String>()));
                }else{
                    imgs_locs = new TreeSet<String>(Utils.getStringArrayPref(context,location_pref_var));
                }
                if(imgs_locs.size() == 0){
                    try {
                        c = holder.lockCanvas();
                        if (c != null) {
                            c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                            Paint paint = new Paint();
                            paint.setColor(Color.WHITE); // Text Color
                            paint.setStrokeWidth(12); // Text Size
                            paint.setTextSize(48);
                            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
                            int text_left_pos = (int)(10 * dm.densityDpi / 160.0f);
                            int text_top_pos = (int)(60 * dm.densityDpi / 160.0f);
                            int text_top_pos_count = (int)(34 * dm.densityDpi / 160.0f);
                            c.drawText("List is EMPTY", text_left_pos, text_top_pos, paint);
                            c.drawText("please Click on ", text_left_pos, text_top_pos+text_top_pos_count, paint);
                            c.drawText("\"Add to LWP\" ", text_left_pos, text_top_pos+text_top_pos_count*2, paint);
                            c.drawText("Button on an image", text_left_pos, text_top_pos+text_top_pos_count*3, paint);
                            c.drawText("to add them", text_left_pos, text_top_pos+text_top_pos_count*4, paint);
                        }
                    }catch (Exception e){
                        Log.d("backpaper2-lwp","error at lwp "+e.toString());
                    }
                    finally {
                        if (c != null)
                            holder.unlockCanvasAndPost(c);
                    }
                    int timeOut = 10000;
                    mHandler.postDelayed(mUpdateDisplay, timeOut);
                    return;
                }
				String[] imgs_locs_array = imgs_locs.toArray(new String[imgs_locs.size()]);
				String update_type = sharedPrefs.getString("updates_type", "Random");
				String dimg;
				if(imgs_locs_array.length == 0)
					return;
				if(update_type.equals("Random")){
					int min = 0;
					int max = imgs_locs_array.length - 1;
					int randomnum = rand.nextInt(max - min + 1) + min;
					dimg = imgs_locs_array[randomnum];
				}else{
					SharedPreferences settings2 = getSharedPreferences(PREFS_NAME, 0);
					int counter = settings2.getInt("counter", 0);
					
					if(swipeDirection == SWIPE_BOTTOM_TO_TOP){
					//display previous image
						counter = counter - 1;
						if(counter<0)
							counter = imgs_locs_array.length-1;
						if(counter>=imgs_locs_array.length)
							counter = 0;
					}else{
						//display next image
						counter = counter + 1;
						if(counter>=imgs_locs_array.length)
							counter=0;
						if(counter<0)
							counter = imgs_locs_array.length-1;
					}
					try{
						dimg = imgs_locs_array[counter];
					}catch(Exception e){
						Log.d("backpaper2-lwp",e.toString());
						dimg = imgs_locs_array[0];
					}
//					System.out.println(counter);
					SharedPreferences.Editor editor = settings2.edit();
					editor.putInt("counter", counter);
					editor.commit();
				
				}
				try {
					c = holder.lockCanvas();
					if (c != null) {
						mDstHeight = c.getHeight();
						mDstWidth = c.getWidth();
						c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
						Paint p = new Paint();
						File f = new File(dimg);
						if(f.exists()){
							BitmapFactory.Options options = new BitmapFactory.Options();
							options.inPreferredConfig = Bitmap.Config.ARGB_8888;
							Bitmap b;
							if(onBottomTop){
                                SharedPreferences settings = getApplicationContext().getSharedPreferences(IntentConfigVariables.lwpDefaultLocationPref, 0);
                                String defaultWall = settings.getString(IntentConfigVariables.getLwpDefaultLocationValue,null);
                                try{
                                    if(defaultWall == null){
                                        throw (new Exception());
                                    }
                                    Bitmap unscaledBitmap = ScalingUtilities.decodeResourceFromImageFile(defaultWall,mDstWidth, mDstHeight, ScalingLogic.CROP);
                                    if(unscaledBitmap == null){
                                        Log.d(DEBUG_TAG,"throwing exception bcz got null in return");
                                        throw (new Exception());
                                    }
                                    b = ScalingUtilities.createScaledBitmap(unscaledBitmap, mDstWidth,mDstHeight, ScalingLogic.CROP);
                                    unscaledBitmap.recycle();
                                }catch (Exception e){
                                    Bitmap unscaledBitmap = ScalingUtilities.decodeResource(getResources(), R.drawable.rsz_hd_wallpapers_1,mDstWidth, mDstHeight, ScalingLogic.CROP);
                                    b = ScalingUtilities.createScaledBitmap(unscaledBitmap, mDstWidth,mDstHeight, ScalingLogic.CROP);
                                    unscaledBitmap.recycle();
                                }
							}else{
								Bitmap bx = BitmapFactory.decodeFile(dimg, options);
								b = ScalingUtilities.createScaledBitmap(bx, mDstWidth,mDstHeight, ScalingLogic.CROP);
								bx.recycle();
							}
							p.setColor(Color.RED);
							c.drawBitmap(b, 0, 0, null);
							if(sharedPrefs.getBoolean("display_image_name", false) && !onBottomTop){
								Paint paint = new Paint();
								paint.setColor(Color.WHITE); // Text Color
								paint.setStrokeWidth(12); // Text Size
								paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
								int text_left_pos = (int)(10 * dm.densityDpi / 160.0f);
								int text_top_pos = (int)(40 * dm.densityDpi / 160.0f);
								c.drawText(f.getName(), text_left_pos, text_top_pos, paint);
							}
						}else{
							Log.d("backpaper2-lwp","file does not exist");
						}
					}
				}catch(Exception e){
					Log.d("backpaper2-lwp","error at lwp "+e.toString());
				}
				finally {
					if (c != null)
						holder.unlockCanvasAndPost(c);
				}
				mHandler.removeCallbacks(mUpdateDisplay);
				if (mVisible && !onBottomTop && sharedPrefs.getBoolean("enable_auto_updates", true)) {
					int timeOut = 10000;
					try{
						 timeOut = Integer.parseInt(sharedPrefs.getString("updates_interval", "10"))*1000;
					}catch(Exception e){
						timeOut = 10000;
					}
					mHandler.postDelayed(mUpdateDisplay, timeOut);
				}
//			}
//            else{
//                SurfaceHolder holder = getSurfaceHolder();
//                Canvas c = null;
//                try {
//                    c = holder.lockCanvas();
//                    if (c != null) {
//                        Paint paint = new Paint();
//                        paint.setColor(Color.WHITE); // Text Color
//                        paint.setStrokeWidth(12); // Text Size
//                        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
//                        int text_left_pos = (int)(10 * dm.densityDpi / 160.0f);
//                        int text_top_pos = (int)(40 * dm.densityDpi / 160.0f);
//                        c.drawText("Currently Not Available on Android 3 or Below. Working on bringing it to More Versions", text_left_pos, text_top_pos, paint);
//                    }
//                }catch (Exception e){
//                    Log.d("backpaper2-lwp","error at lwp "+e.toString());
//                }
//                finally {
//                    if (c != null)
//                        holder.unlockCanvasAndPost(c);
//                }
//            }
		}

		// ////////////////////////////////////////////////
		// /////////////////////////////////////////

	}

}